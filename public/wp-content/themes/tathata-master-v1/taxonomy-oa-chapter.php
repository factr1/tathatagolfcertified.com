<?php
$term    = $wp_query->queried_object;
$taxname = $term->name;

get_header();
setPageBanner( 'Certified Program Video Library' );
?>

<?php // Custom Breadcrumbs ?>
    <section id="breadcrumbs">
        <div class="hidden-xs">
            <div class="container">
                <div class="row">

                    <div class="col-sm-8">
                        <div class="flex -flex-start">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/course/certified-movement-specialist-program/' ) ); ?>">Home</a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/certified-program-video-library/' ) ); ?>">Certified Program Video Library</a>
                                </li>

                                <?php if ( ! empty( $post->post_parent ) ) {
                                    $parentTitle = get_the_title( $post->post_parent ); ?>
                                    <li>
                                        <a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo $parentTitle; ?></a>
                                    </li>
                                <?php } ?>

                                <li><?php echo $taxname; ?></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="flex -flex-end">
                            <?php echo do_shortcode( '[followUs]' ); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="visible-xs">
            <div class="container">
                <div class="flex">
                    <div>
                        <ul class="breadcrumb">
                            <li>
                                <a href="<?php echo esc_url( home_url( '/course/certified-movement-specialist-program/' ) ); ?>">Home</a>
                            </li>
                            <li>
                                <a href="<?php echo esc_url( home_url( '/certified-program-video-library/' ) ); ?>">Certified Program Video Library</a>
                            </li>

                            <?php if ( ! empty( $post->post_parent ) ) {
                                $parentTitle = get_the_title( $post->post_parent ); ?>
                                <li>
                                    <a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo $parentTitle; ?></a>
                                </li>
                            <?php } ?>

                            <li><?php echo $taxname; ?></li>
                        </ul>
                    </div>

                    <div>
                        <?php echo do_shortcode( '[followUs]' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="tg-video-archive" class="tg-video-archive">
        <div class="container">

            <div class="video-menu">
                <div class="row">
                    <div class="col-sm-4">
                        <?php wp_nav_menu( array( 'menu' => 'video menu', 'container_id' => 'owncssmenu' ) ); ?>
                    </div>
                </div>
            </div>

            <div class="video-archive-title">
                <h1><?php echo $taxname; ?></h1>
            </div>

            <div class="video-archive-description">
                <p><?php echo $term->description; ?></p>
            </div>

            <div class="tg-video-archive-videos">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="archive-video-container">
                            <?php
                            if ( have_posts() ) : while ( have_posts() ) : the_post();
                                get_template_part( 'content', 'oa-chapter' );
                            endwhile;
                            else :
                                get_template_part( 'content', 'none' );
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>

<?php
get_footer();
