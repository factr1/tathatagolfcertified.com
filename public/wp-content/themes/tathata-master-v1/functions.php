<?php

define( 'MAILCHIMP_API_KEY', '94d09507936de69acf055f5686b91773-us3' );

// Require composer installed library dependencies
require_once( "vendor/autoload.php" );

// lets exclude certain pages from the search results
function jp_search_filter( $query ) {
    if ( $query->is_search && $query->is_main_query() ) {
        $query->set( 'post__not_in', array( 7609, 8558, 8338 ) );
    }
}

add_action( 'pre_get_posts', 'jp_search_filter' );

/**
 * Twenty Fourteen functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * When using a child theme you can override certain functions (those wrapped
 * in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before
 * the parent theme's file, so the child theme functions would be used.
 *
 * @link       http://codex.wordpress.org/Theme_Development
 * @link       http://codex.wordpress.org/Child_Themes
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are
 * instead attached to a filter or action hook.
 *
 * For more information on hooks, actions, and filters,
 * @link       http://codex.wordpress.org/Plugin_API
 *
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */

/**
 * Set up the content width value based on the theme's design.
 *
 * @see   twentyfourteen_content_width()
 *
 * @since Twenty Fourteen 1.0
 */
if ( ! isset( $content_width ) ) {
    $content_width = 740;
}

/**
 * Twenty Fourteen only works in WordPress 3.6 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '3.6', '<' ) ) {
    require get_template_directory() . '/inc/back-compat.php';
}

if ( ! function_exists( 'twentyfourteen_setup' ) ) :
    /**
     * Twenty Fourteen setup.
     *
     * Set up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support post thumbnails.
     *
     * @since Twenty Fourteen 1.0
     */
    function twentyfourteen_setup() {

        /*
		 * Make Twenty Fourteen available for translation.
		 *
		 * Translations can be added to the /languages/ directory.
		 * If you're building a theme based on Twenty Fourteen, use a find and
		 * replace to change 'twentyfourteen' to the name of your theme in all
		 * template files.
		 */
        load_theme_textdomain( 'twentyfourteen', get_template_directory() . '/languages' );

        // This theme styles the visual editor to resemble the theme style.
        add_editor_style( array( 'css/editor-style.css', twentyfourteen_font_url(), 'genericons/genericons.css' ) );

        // Add RSS feed links to <head> for posts and comments.
        add_theme_support( 'automatic-feed-links' );

        // Enable support for Post Thumbnails, and declare two sizes.
        add_theme_support( 'post-thumbnails' );
        set_post_thumbnail_size( 672, 372, true );
        add_image_size( 'twentyfourteen-full-width', 1038, 576, true );
        add_image_size( 'tgc-hero', 1400, 500, true );

        // This theme uses wp_nav_menu() in two locations.
        register_nav_menus( array(
            'primary'      => __( 'Top primary menu', 'twentyfourteen' ),
            'secondary'    => __( 'Secondary menu in left sidebar', 'twentyfourteen' ),
            'video-menu'   => __( 'Video Menu', 'twentyfourteen' ),
            'broken-apart' => __( 'Broken Apart Menu', 'twentyfourteen' ),
        ) );

        /*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
        add_theme_support( 'html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption'
        ) );

        /*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
        add_theme_support( 'post-formats', array(
            'aside',
            'image',
            'video',
            'audio',
            'quote',
            'link',
            'gallery',
        ) );

        // This theme allows users to set a custom background.
        add_theme_support( 'custom-background', apply_filters( 'twentyfourteen_custom_background_args', array(
            'default-color' => 'f5f5f5',
        ) ) );

        // Add support for featured content.
        add_theme_support( 'featured-content', array(
            'featured_content_filter' => 'twentyfourteen_get_featured_posts',
            'max_posts'               => 6,
        ) );

        // This theme uses its own gallery styles.
        add_filter( 'use_default_gallery_style', '__return_false' );
    }
endif; // twentyfourteen_setup
add_action( 'after_setup_theme', 'twentyfourteen_setup' );

/**
 * Adjust content_width value for image attachment template.
 *
 * @since Twenty Fourteen 1.0
 */
function twentyfourteen_content_width() {
    if ( is_attachment() && wp_attachment_is_image() ) {
        $GLOBALS['content_width'] = 810;
    }
}

add_action( 'template_redirect', 'twentyfourteen_content_width' );

add_action( 'sensei_complete_quiz_text', 'complete_quiz_text' );
function complete_quiz_text() {
    return 'SUBMIT QUIZ';
}

/**
 * Getter function for Featured Content Plugin.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return array An array of WP_Post objects.
 */
function twentyfourteen_get_featured_posts() {
    /**
     * Filter the featured posts to return in Twenty Fourteen.
     *
     * @since Twenty Fourteen 1.0
     *
     * @param array|bool $posts Array of featured posts, otherwise false.
     */
    return apply_filters( 'twentyfourteen_get_featured_posts', array() );
}

/**
 * A helper conditional function that returns a boolean value.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return bool Whether there are featured posts.
 */
function twentyfourteen_has_featured_posts() {
    return ! is_paged() && (bool) twentyfourteen_get_featured_posts();
}

/**
 * Register three Twenty Fourteen widget areas.
 *
 * @since Twenty Fourteen 1.0
 */
function twentyfourteen_widgets_init() {
    require get_template_directory() . '/inc/widgets.php';
    register_widget( 'Twenty_Fourteen_Ephemera_Widget' );

    register_sidebar( array(
        'name'          => __( 'Primary Sidebar', 'twentyfourteen' ),
        'id'            => 'sidebar-1',
        'description'   => __( 'Main sidebar that appears on the left.', 'twentyfourteen' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Content Sidebar', 'twentyfourteen' ),
        'id'            => 'sidebar-2',
        'description'   => __( 'Additional sidebar that appears on the right.', 'twentyfourteen' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>',
    ) );
    register_sidebar( array(
        'name'          => __( 'Footer Widget Area', 'twentyfourteen' ),
        'id'            => 'sidebar-3',
        'description'   => __( 'Appears in the footer section of the site.', 'twentyfourteen' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Logo Footer', 'twentyfourteen' ),
        'id'            => 'sidebar-logofooter',
        'description'   => __( 'Holds the logos in the footer', 'twentyfourteen' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h2 class="forehead-title">',
        'after_title'   => '</h2>',
    ) );
}

add_action( 'widgets_init', 'twentyfourteen_widgets_init' );

/**
 * Register Lato Google font for Twenty Fourteen.
 *
 * @since Twenty Fourteen 1.0
 *
 * @return string
 */
function twentyfourteen_font_url() {
    $font_url = '';
    /*
     * Translators: If there are characters in your language that are not supported
     * by Lato, translate this to 'off'. Do not translate into your own language.
     */
    if ( 'off' !== _x( 'on', 'Lato font: on or off', 'twentyfourteen' ) ) {
        $font_url = add_query_arg( 'family', urlencode( 'Lato:300,400,700,900,300italic,400italic,700italic' ), "//fonts.googleapis.com/css" );
    }

    return $font_url;
}

/**
 * Enqueue scripts and styles for the front end.
 *
 * @since Twenty Fourteen 1.0
 */
function twentyfourteen_scripts() {
    // Add Lato font, used in the main stylesheet.
    wp_enqueue_style( 'twentyfourteen-lato', twentyfourteen_font_url(), array(), null );
}

add_action( 'wp_enqueue_scripts', 'twentyfourteen_scripts' );

/**
 * Enqueue Google fonts style to admin screen for custom header display.
 *
 * @since Twenty Fourteen 1.0
 */
function twentyfourteen_admin_fonts() {
    wp_enqueue_style( 'twentyfourteen-lato', twentyfourteen_font_url(), array(), null );
}

add_action( 'admin_print_scripts-appearance_page_custom-header', 'twentyfourteen_admin_fonts' );

if ( ! function_exists( 'twentyfourteen_the_attached_image' ) ) :
    /**
     * Print the attached image with a link to the next attached image.
     *
     * @since Twenty Fourteen 1.0
     */
    function twentyfourteen_the_attached_image() {
        $post = get_post();
        /**
         * Filter the default Twenty Fourteen attachment size.
         *
         * @since Twenty Fourteen 1.0
         *
         * @param array $dimensions {
         *                          An array of height and width dimensions.
         *
         * @type int    $height     Height of the image in pixels. Default 810.
         * @type int    $width      Width of the image in pixels. Default 810.
         * }
         */
        $attachment_size     = apply_filters( 'twentyfourteen_attachment_size', array( 810, 810 ) );
        $next_attachment_url = wp_get_attachment_url();

        /*
		 * Grab the IDs of all the image attachments in a gallery so we can get the URL
		 * of the next adjacent image in a gallery, or the first image (if we're
		 * looking at the last image in a gallery), or, in a gallery of one, just the
		 * link to that image file.
		 */
        $attachment_ids = get_posts( array(
            'post_parent'    => $post->post_parent,
            'fields'         => 'ids',
            'numberposts'    => - 1,
            'post_status'    => 'inherit',
            'post_type'      => 'attachment',
            'post_mime_type' => 'image',
            'order'          => 'ASC',
            'orderby'        => 'menu_order ID',
        ) );

        // If there is more than 1 attachment in a gallery...
        if ( count( $attachment_ids ) > 1 ) {
            foreach ( $attachment_ids as $attachment_id ) {
                if ( $attachment_id == $post->ID ) {
                    $next_id = current( $attachment_ids );
                    break;
                }
            }

            // get the URL of the next image attachment...
            if ( $next_id ) {
                $next_attachment_url = get_attachment_link( $next_id );
            } // or get the URL of the first image attachment.
            else {
                $next_attachment_url = get_attachment_link( array_shift( $attachment_ids ) );
            }
        }

        printf( '<a href="%1$s" rel="attachment">%2$s</a>', esc_url( $next_attachment_url ), wp_get_attachment_image( $post->ID, $attachment_size ) );
    }
endif;

if ( ! function_exists( 'twentyfourteen_list_authors' ) ) :
    /**
     * Print a list of all site contributors who published at least one post.
     *
     * @since Twenty Fourteen 1.0
     */
    function twentyfourteen_list_authors() {
        $contributor_ids = get_users( array(
            'fields'  => 'ID',
            'orderby' => 'post_count',
            'order'   => 'DESC',
            'who'     => 'authors',
        ) );

        foreach ( $contributor_ids as $contributor_id ) :
            $post_count = count_user_posts( $contributor_id );

            // Move on if user has not published a post (yet).
            if ( ! $post_count ) {
                continue;
            }
            ?>

            <div class="contributor">
                <div class="contributor-info">
                    <div class="contributor-avatar"><?php echo get_avatar( $contributor_id, 132 ); ?></div>
                    <div class="contributor-summary">
                        <h2 class="contributor-name"><?php echo get_the_author_meta( 'display_name', $contributor_id ); ?></h2>
                        <p class="contributor-bio">
                            <?php echo get_the_author_meta( 'description', $contributor_id ); ?>
                        </p>
                        <a class="button contributor-posts-link" href="<?php echo esc_url( get_author_posts_url( $contributor_id ) ); ?>">
                            <?php printf( _n( '%d Article', '%d Articles', $post_count, 'twentyfourteen' ), $post_count ); ?>
                        </a>
                    </div><!-- .contributor-summary -->
                </div><!-- .contributor-info -->
            </div><!-- .contributor -->

            <?php
        endforeach;
    }
endif;

/**
 * Extend the default WordPress body classes.
 *
 * Adds body classes to denote:
 * 1. Single or multiple authors.
 * 2. Presence of header image except in Multisite signup and activate pages.
 * 3. Index views.
 * 4. Full-width content layout.
 * 5. Presence of footer widgets.
 * 6. Single views.
 * 7. Featured content layout.
 *
 * @since Twenty Fourteen 1.0
 *
 * @param array $classes A list of existing body class values.
 *
 * @return array The filtered body class list.
 */
function twentyfourteen_body_classes( $classes ) {
    if ( is_multi_author() ) {
        $classes[] = 'group-blog';
    }

    if ( get_header_image() ) {
        $classes[] = 'header-image';
    } elseif ( ! in_array( $GLOBALS['pagenow'], array( 'wp-activate.php', 'wp-signup.php' ) ) ) {
        $classes[] = 'masthead-fixed';
    }

    if ( is_archive() || is_search() || is_home() ) {
        $classes[] = 'list-view';
    }

    if ( ( ! is_active_sidebar( 'sidebar-2' ) ) || is_page_template( 'page-templates/full-width.php' ) || is_page_template( 'page-templates/contributors.php' ) || is_attachment() ) {
        $classes[] = 'full-width';
    }

    if ( is_active_sidebar( 'sidebar-3' ) ) {
        $classes[] = 'footer-widgets';
    }

    if ( is_singular() && ! is_front_page() ) {
        $classes[] = 'singular';
    }

    if ( is_front_page() && 'slider' == get_theme_mod( 'featured_content_layout' ) ) {
        $classes[] = 'slider';
    } elseif ( is_front_page() ) {
        $classes[] = 'grid';
    }

    return $classes;
}

add_filter( 'body_class', 'twentyfourteen_body_classes' );

/**
 * Extend the default WordPress post classes.
 *
 * Adds a post class to denote:
 * Non-password protected page with a post thumbnail.
 *
 * @since Twenty Fourteen 1.0
 *
 * @param array $classes A list of existing post class values.
 *
 * @return array The filtered post class list.
 */
function twentyfourteen_post_classes( $classes ) {
    if ( ! post_password_required() && ! is_attachment() && has_post_thumbnail() ) {
        $classes[] = 'has-post-thumbnail';
    }

    return $classes;
}

add_filter( 'post_class', 'twentyfourteen_post_classes' );

/**
 * Create a nicely formatted and more specific title element text for output
 * in head of document, based on current view.
 *
 * @since Twenty Fourteen 1.0
 *
 * @global int   $paged WordPress archive pagination page count.
 * @global int   $page  WordPress paginated post page count.
 *
 * @param string $title Default title text for current view.
 * @param string $sep   Optional separator.
 *
 * @return string The filtered title.
 */
function twentyfourteen_wp_title( $title, $sep ) {
    global $paged, $page;

    if ( is_feed() ) {
        return $title;
    }

    // Add the site name.
    $title .= get_bloginfo( 'name', 'display' );

    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title = "$title $sep $site_description";
    }

    // Add a page number if necessary.
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title = "$title $sep " . sprintf( __( 'Page %s', 'twentyfourteen' ), max( $paged, $page ) );
    }

    return $title;
}

add_filter( 'wp_title', 'twentyfourteen_wp_title', 10, 2 );

// Implement Custom Header features.
require get_template_directory() . '/inc/custom-header.php';

// Custom template tags for this theme.
require get_template_directory() . '/inc/template-tags.php';

// Add Theme Customizer functionality.
require get_template_directory() . '/inc/customizer.php';

/*
 * Add Featured Content functionality.
 *
 * To overwrite in a plugin, define your own Featured_Content class on or
 * before the 'setup_theme' hook.
 */
if ( ! class_exists( 'Featured_Content' ) && 'plugins.php' !== $GLOBALS['pagenow'] ) {
    require get_template_directory() . '/inc/featured-content.php';
}

add_filter( 'the_content_more_link', 'modify_read_more_link' );
function modify_read_more_link() {
    return '<span class="continueRead"><a class="more-link" href="' . get_permalink() . '">Continue Reading</a></span>';
}

/**Placeholder of comments box**/

add_filter( 'comment_form_defaults', 'my_comment_defaults' );

function my_comment_defaults( $defaults ) {
    $req           = get_option( 'require_name_email' );
    $aria_req      = ( $req ? " aria-required='true'" : '' );
    $post_id       = get_the_ID();
    $user          = wp_get_current_user();
    $user_identity = $user->exists() ? $user->display_name : '';
    $defaults      = array(

        'fields'        => array(
            'author'  => '<div class="inlineInput">' . '<input id="author" name="author" placeholder="Your Name" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></div>',
            'email'   => '<div class="inlineInput">' . '<input id="email" name="email" placeholder="E-Mail Address" type="email" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></div>',
            'website' => '<div class="inlineInput">' . '<input id="website" name="website" placeholder="Website" type="text" value="' . esc_attr( $commenter['comment_author_website'] ) . '" size="30"' . $aria_req . ' /></div>'
        ),
        'comment_field' => '<div class="fullWidthInput"><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"  placeholder="Write your comments here..... [keep it clean and respectful to others]"></textarea></div>',

        'must_log_in' => '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a comment.' ), wp_login_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',

        'logged_in_as' => '<p class="logged-in-as">' . sprintf( __( 'Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>' ), admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( $post_id ) ) ) ) . '</p>',

        'comment_notes_before' => '<fieldset>',

        'comment_notes_after' => '</fieldset>',

        'id_form' => 'commentform',

        'id_submit' => 'submit',

        'title_reply' => __( 'Post A COMMENT' ),

        'title_reply_to' => __( 'Leave a Reply %s' ),

        'cancel_reply_link' => __( 'Cancel reply' ),

        'label_submit' => __( 'Submit' ),

    );

    return $defaults;
}

// Additional display overrides - Chris Jenkins 02/01/15

function tathbb_before_main_content() {
    ?>
    <div class="container">
        <div id="bbp-container">
            <div id="bbp-content" role="main">

                <?php
                }

                function tathbb_after_main_content() {
                ?>

            </div><!-- #bbp-content -->
        </div><!-- #bbp-container -->
    </div><!-- .container -->
    <?php
}

add_filter( 'bbp_before_main_content', 'tathbb_before_main_content' );
add_filter( 'bbp_after_main_content', 'tathbb_after_main_content' );

/*Added for gear page*/
//For woocommerce sidebar
register_sidebar( array(
    'name'          => __( 'Woocommerce Sidebar', 'twentyfourteen' ),
    'id'            => 'shop',
    'description'   => __( 'Appears in the Woocommerce pages', 'twentyfourteen' ),
    'before_widget' => '<div class="pro-catagory">',
    'after_widget'  => '</div>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>',
) );

add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 4;' ), 20 );

//For removing rating in gear page
remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );

//For swapping rating and price in single product page
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 19 );
//For placing add to cart inside figure tag
remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );
add_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 10 );
// For woocommerce WooCommerce breadcrumb
add_filter( 'woocommerce_breadcrumb_defaults', 'jk_woocommerce_breadcrumbs' );
function jk_woocommerce_breadcrumbs() {
    return array(
        'delimiter'   => '',
        'wrap_before' => '<ol class="breadcrumb pull-left" itemprop="breadcrumb">',
        'wrap_after'  => '</ol >',
        'before'      => '<li>',
        'after'       => '</li>',
        'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
    );
}

// For Changing order by text
add_filter( 'gettext', 'theme_sort_change', 20, 3 );
function theme_sort_change( $translated_text, $text, $domain ) {

    if ( is_woocommerce() ) {

        switch ( $translated_text ) {

            case 'Default sorting' :

                $translated_text = __( 'Sorting', 'theme_text_domain' );
                break;
            case 'Sort by popularity' :

                $translated_text = __( 'Populartity', 'theme_text_domain' );
                break;
            case 'Sort by average rating' :

                $translated_text = __( 'Average Rating', 'theme_text_domain' );
                break;
            case 'Sort by newness' :

                $translated_text = __( 'New Products', 'theme_text_domain' );
                break;
            case 'Sort by price: low to high' :

                $translated_text = __( 'Price low to high', 'theme_text_domain' );
                break;
            case 'Sort by price: high to low' :

                $translated_text = __( 'Price high to low', 'theme_text_domain' );
                break;
        }

    }

    return $translated_text;
}

// For adding placeholders in shipping and billing fields
add_filter( 'woocommerce_checkout_fields', 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
    $fields['billing']['billing_first_name']['placeholder']   = _x( 'First Name', 'placeholder', 'woocommerce' );
    $fields['billing']['billing_last_name']['placeholder']    = _x( 'Last Name', 'placeholder', 'woocommerce' );
    $fields['billing']['billing_company']['placeholder']      = _x( 'Company', 'placeholder', 'woocommerce' );
    $fields['billing']['billing_email']['placeholder']        = _x( 'Email', 'placeholder', 'woocommerce' );
    $fields['shipping']['shipping_first_name']['placeholder'] = _x( 'First Name', 'placeholder', 'woocommerce' );
    $fields['shipping']['shipping_last_name']['placeholder']  = _x( 'Last Name', 'placeholder', 'woocommerce' );
    $fields['shipping']['shipping_company']['placeholder']    = _x( 'Company', 'placeholder', 'woocommerce' );

    return $fields;
}

// For changing woocommerce thank you page title
function bk_title_order_received( $title, $id ) {
    if ( is_order_received_page() && get_the_ID() === $id ) {
        $title = "Thank you for your order!";
    }

    return $title;
}

add_filter( 'the_title', 'bk_title_order_received', 10, 2 );

//For excerpt support to page
add_post_type_support( 'page', 'excerpt' );
//For shortcode support to excerpt
add_filter( 'the_excerpt', 'do_shortcode' );

/*End for gear page*/

/* Enable HTML in widget titles so we can style the first word different than the rest. Use square brackets instead of carets. Ex: [a href=""][strong]Text[/strong][/a] */

function html_widget_title( $title ) {
    //HTML tag opening/closing brackets
    $title = str_replace( '[', '<', $title );
    $title = str_replace( '[/', '</', $title );
    // bold -- changed from 's' to 'strong' because of strikethrough code
    $title = str_replace( 'strong]', 'strong>', $title );
    $title = str_replace( 'b]', 'b>', $title );
    // italic
    $title = str_replace( 'em]', 'em>', $title );
    $title = str_replace( 'i]', 'i>', $title );
    // underline
    // $title = str_replace( 'u]', 'u>', $title ); // could use this, but it is deprecated so use the following instead
    $title = str_replace( '<u]', '<span style="text-decoration:underline;">', $title );
    $title = str_replace( '</u]', '</span>', $title );
    // superscript
    $title = str_replace( 'sup]', 'sup>', $title );
    // subscript
    $title = str_replace( 'sub]', 'sub>', $title );
    // del
    $title = str_replace( 'del]', 'del>', $title ); // del is like strike except it is not deprecated, but strike has wider browser support -- you might want to replace the following 'strike' section to replace all with 'del' instead
    // strikethrough or <s></s>
    $title = str_replace( 'strike]', 'strike>', $title );
    $title = str_replace( 's]', 'strike>', $title ); // <s></s> was deprecated earlier than so we will convert it
    $title = str_replace( 'strikethrough]', 'strike>', $title ); // just in case you forget that it is 'strike', not 'strikethrough'
    // tt
    $title = str_replace( 'tt]', 'tt>', $title ); // Will not look different in some themes, like Twenty Eleven -- FYI: http://reference.sitepoint.com/html/tt
    // marquee
    $title = str_replace( 'marquee]', 'marquee>', $title );
    // blink
    $title = str_replace( 'blink]', 'blink>', $title ); // only Firefox and Opera support this tag
    // wtitle1 (to be styled in style.css using .wtitle1 class)
    $title = str_replace( '<wtitle1]', '<span class="wtitle1">', $title );
    $title = str_replace( '</wtitle1]', '</span>', $title );
    // wtitle2 (to be styled in style.css using .wtitle2 class)
    $title = str_replace( '<wtitle2]', '<span class="wtitle2">', $title );
    $title = str_replace( '</wtitle2]', '</span>', $title );

    return $title;
}

add_filter( 'widget_title', 'html_widget_title' );

add_action( 'pre_user_query', 'sort_connect_author_list_by_last_name' );
function sort_connect_author_list_by_last_name( &$object ) {
    global $wp, $wpdb;
    if ( preg_match( "/^connect(\/.+)?$/", $wp->request ) ) {
        // Sub query
        $sql = "
            (SELECT
                `{$wpdb->users}`.`ID` AS `c_user_id`,
                MAX(CASE WHEN `{$wpdb->usermeta}`.`meta_key` = 'location' THEN `{$wpdb->usermeta}`.`meta_value` END ) AS `c_location`,
                MAX(CASE WHEN `{$wpdb->usermeta}`.`meta_key` = 'last_name' THEN `{$wpdb->usermeta}`.`meta_value` END ) AS `c_last_name`,
                MAX(CASE WHEN `{$wpdb->usermeta}`.`meta_key` = 'first_name' THEN `{$wpdb->usermeta}`.`meta_value` END ) AS `c_first_name`
            FROM
                `{$wpdb->users}`
                LEFT JOIN `{$wpdb->usermeta}` ON ( `{$wpdb->users}`.`ID` = `{$wpdb->usermeta}`.`user_id` )
            WHERE
                1=1
            GROUP BY
                `{$wpdb->users}`.`ID`
            ) AS `C`
        ";
        // Modify existing query
        $object->query_fields = "SQL_CALC_FOUND_ROWS `{$wpdb->users}`.`ID`, `{$wpdb->users}`.`display_name`";
        $object->query_from .= " RIGHT JOIN {$sql} ON ( `{$wpdb->users}`.`ID` = `C`.`c_user_id` )";
        $object->query_orderby = "ORDER BY `C`.`c_last_name` ASC";
    }
}

function pagination( $pages = '', $range = 4 ) {
    $showitems = ( $range * 2 ) + 1;

    global $paged;
    if ( empty( $paged ) ) {
        $paged = 1;
    }

    if ( $pages == '' ) {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if ( ! $pages ) {
            $pages = 1;
        }
    }

    if ( 1 != $pages ) {
        echo "<div class=\"pagination\"><span>Page " . $paged . " of " . $pages . "</span>";
        if ( $paged > 2 && $paged > $range + 1 && $showitems < $pages ) {
            echo "<a href='" . get_pagenum_link( 1 ) . "'>&laquo; First</a>";
        }
        if ( $paged > 1 && $showitems < $pages ) {
            echo "<a href='" . get_pagenum_link( $paged - 1 ) . "'>&lsaquo; Previous</a>";
        }

        for ( $i = 1; $i <= $pages; $i ++ ) {
            if ( 1 != $pages && ( ! ( $i >= $paged + $range + 1 || $i <= $paged - $range - 1 ) || $pages <= $showitems ) ) {
                echo ( $paged == $i ) ? "<span class=\"current\">" . $i . "</span>" : "<a href='" . get_pagenum_link( $i ) . "' class=\"inactive\">" . $i . "</a>";
            }
        }

        if ( $paged < $pages && $showitems < $pages ) {
            echo "<a href=\"" . get_pagenum_link( $paged + 1 ) . "\">Next &rsaquo;</a>";
        }
        if ( $paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages ) {
            echo "<a href='" . get_pagenum_link( $pages ) . "'>Last &raquo;</a>";
        }
        echo "</div>\n";
    }
}

/**Comment List template Change**/
function custom_comments( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    switch ( $comment->comment_type ) :
        case 'pingback' :
        case 'trackback' : ?>
            <li <?php comment_class(); ?> id="comment<?php comment_ID(); ?>">
            <div class="back-link"><?php comment_author_link(); ?></div>
            <?php break;
        default : ?>
            <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
                <article <?php comment_class(); ?> class="comment">
                    <div class="comment-body">
                        <div class="author vcard">
                            <div class="authorImg"><?php echo get_avatar( $comment, 100 ); ?></div>
                            <div class="authorNamePostDetail">
                                <span class="author-name"><?php comment_author(); ?></span>
                                <time <?php comment_time( 'c' ); ?> class="comment-time">
                                    POSTED AT <span class="time">
        <?php comment_time(); ?>
        </span>, <span class="date">
        <?php
        $d        = " d F";
        $new_date = get_comment_date( $d, $comment_ID );
        echo $new_date;
        $datechange = date( 'd F', strtotime( $new_date ) );
        //  echo $datechange;
        ?>
        </span>
                                </time>
                                [ <span class="reply"><?php
                                    comment_reply_link( array_merge( $args, array(
                                        'reply_text' => 'Reply',
                                        'after'      => ' ',
                                        'depth'      => $depth,
                                        'max_depth'  => $args['max_depth']
                                    ) ) ); ?>
        </span> ]<!-- .reply -->
                            </div><!-- .vcard -->
                        </div><!-- comment-body -->

                        <div class="comm-content">
                            <p><?php echo get_comment_text( $comment_ID ); ?></p>
                        </div>

                </article><!-- #comment-<?php comment_ID(); ?> -->
            </li>
            <?php // End the default styling of comment
            break;
    endswitch;
}

//For OA greatest players
function qaplayerlist( $input_args ) {
    $args = array(
        'post_type'   => 'oagreatestplayers',
        'status'      => 'publish',
        'numberposts' => - 1,
    );
    if ( ! empty( $input_args ) ) {
        if ( ! empty( $input_args['search'] ) ) {
            $args['s'] = $input_args['search'];
        }
        if ( ! empty( $input_args['category'] ) ) {
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'oa-category',
                    'field'    => 'id',
                    'terms'    => $input_args['category']
                )
            );
        }
    }
    $players = get_posts( $args );
    $str     = '';
    if ( ! empty( $players ) ) {
        $time = time();
        $str .= '<div class="vidrow" id="pag_' . $time . '">';
        foreach ( $players as $player ) {
            $str .= '<div class="viditem">' . '<div class="viditem_internal">' . '<h5><a href="' . get_permalink( $player->ID ) . '" title="' . $player->post_title . '">' . $player->post_title . '</a></h5>' . '<div class="videoContainer">';
            $videocode = get_post_meta( $player->ID, 'videocode', true );
            if ( is_array( $videocode ) && array_key_exists( 'video-code', $videocode[0] ) ) {
                $str .= $videocode[0]['video-code'];
            }
            $str .= '</div>' . '<div class="vidlist">' . $player->post_excerpt . '</div>' . '</div>' . '</div>';
        }
        $str .= '</div>
        <script type="text/javascript">' . 'jQuery(document).ready(function() {' . 'jQuery("div#pag_' . $time . '").quickPagination({pagerLocation:"both",pageSize:"12"});' . '});' . '</script>';
    } else {
        $str .= 'No players found';
    }

    return $str;
}

add_shortcode( 'QAPLAYERLIST', 'qaplayerlist' );
// For post navigation in QA Players single page
function custom_post_nav() {
    // Don't print empty markup if there's nowhere to navigate.
    $previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
    $next     = get_adjacent_post( false, '', false );
    if ( ! $next && ! $previous ) {
        return;
    }
    if ( is_attachment() ) :
        previous_post_link( '%link', __( '<span class="meta-nav">Published In</span>%title', 'twentyfourteen' ) );
    else :
        previous_post_link( '%link', __( '<span class="PrevGreyBtn left" style="margin-right:10px; width:48%; text-align: right;">PREVIOUS VIDEO</span>', 'twentyfourteen' ) );
        next_post_link( '%link', __( ' <span class="redLinkBtn right" style="width:48%;">NEXT VIDEO</span>', 'twentyfourteen' ) );
    endif;
}

/*11032015*/
/*Added for the theme to support woocommerce support*/
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

/*11032015 - end*/

function quiz_custom_reset_button() {
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            jQuery('.quiz-controls .reset').hide();
            jQuery('#reset').click(function() {
                jQuery('.quiz-controls .reset').click();
            })
        })
    </script>
    <?php
}

add_action( 'sensei_quiz_action_buttons', 'quiz_custom_reset_button' );

// 24032015
//Line 3121
/*For showing user badges in dashboard*/
//add_action('sensei_quiz_action_buttons', 'test');
function badgeos_achievements_list_custom_shortcode( $atts = array() ) {
    $achievements = get_posts( array( 'post_type' => 'achievement-type', 'post_status' => 'publish' ) );
    $str          = '';

    foreach ( $achievements as $achievement ) {

        $user_achievements = badgeos_get_user_achievements( [
            'user_id'        => get_current_user_id(),
            'achievement_id' => $achievement->ID,
            'site_id'        => 'all' // Pull all as there were old multisite badges
        ] );

        $badges = [];
        foreach ( $user_achievements as $site_id => $badge_list ) {
            $sites = [
                get_current_blog_id(),
                5 // Pull in old multisite badges
            ];
            if ( ! in_array( $site_id, $sites ) ) {
                continue;
            }
            foreach ( $badge_list as $badge ) {
                $badges[] = $badge;
            }
        }

        if ( ! empty( $badges ) ) {
            $str .= '<ul class="otherProfileImgs">';
            foreach ( $badges as $badge ) {
                $str .= '<li>' . get_the_post_thumbnail( $badge->ID, array( 55, 55 ) ) . '</li>';
            }
            $str .= '</ul>';
        }
    }
    if ( empty( $str ) ) {
        $str .= 'No Badges Earned';
    }
    echo $str;
}

add_shortcode( 'badgeos_achievements_list_custom', 'badgeos_achievements_list_custom_shortcode' );
/*For showing user badges in dashboard - end*/
// 24032015 - end
// 25032015
/*For showing course status in dashboard page*/
function courses_status( $user_id ) {
    $course_statuses = WooThemes_Sensei_Utils::sensei_check_for_activity( array(
        'user_id' => $user_id,
        'type'    => 'sensei_course_status'
    ), true );
    // User may only be on 1 Course
    if ( ! is_array( $course_statuses ) ) {
        $course_statuses = array( $course_statuses );
    }
    $statuses = array();
    foreach ( $course_statuses as $course_status ) {
        if ( WooThemes_Sensei_Utils::user_completed_course( $course_status, $user->ID ) ) {
            $statuses[ $course_status->comment_post_ID ]['status'] = 'Completed';
        } else {
            $statuses[ $course_status->comment_post_ID ]['status'] = 'Course in process...';
        }
        $statuses[ $course_status->comment_post_ID ]['date'] = $course_status->comment_date;
    }

    return $statuses;
}

/*For showing course status in dashboard page - end*/
// 25032015 - end
/*27032015*/
// For showing popup in quiz page
if ( is_user_logged_in() ) {
    function is_quiz_page() {
        $quiz_exist  = $slug = '';
        $actual_link = explode( '/', 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] );
        foreach ( $actual_link as $key => $value ) {
            if ( $value === 'quiz' ) {
                $quiz_exist = key( $actual_link );
            }
            $slug = $actual_link[ $quiz_exist ];
        }
        if ( empty( $quiz_exist ) || empty( $slug ) ) {
            return false;
        }
        global $wpdb;

        return $wpdb->get_var( 'select ID from ' . $wpdb->prefix . 'posts where post_name = "' . $slug . '" and post_status = "publish" and post_type = "lesson"' );
    }

    if ( is_quiz_page() ) {
        add_action( 'init', 'my_setcookie' );
        function my_setcookie() {
            $cookie_name = get_current_user_id() . '_' . is_quiz_page();
            setcookie( $cookie_name, 'yes', time() + ( 3600 * 24 * 90 ), COOKIEPATH, COOKIE_DOMAIN );
        }

        add_action( 'wp_head', 'my_getcookie' );
        function my_getcookie() {
            $cookie_name = get_current_user_id() . '_' . is_quiz_page();
            if ( ! isset( $_COOKIE[ $cookie_name ] ) ) {
                echo "<script>
                    jQuery(function ($) {
                        $('#lesson-alert').dialog({
                            maxWidth: 600,
                            width: 600,
                            height: 'auto',
                            modal: true,
                            fluid: true, //new option
                            resizable: true,
                        });
                        jQuery('body')
                                .bind(
                                        'click',
                                        function (e) {
                                            if (jQuery('#lesson-alert').dialog('isOpen')
                                                    && !jQuery(e.target).is('.ui-dialog, a')
                                                    && !jQuery(e.target).closest('.ui-dialog').length
                                                    ) {
                                                jQuery('#lesson-alert').dialog('close');
                                            }
                                        }
                                );
                    });
                </script> ";
            }
        }
    }
}
// For showing popup in quiz page
// For getting order date for already purchased product
function wcr_check_user_bought( $customer_email, $user_id, $product_id ) {
    global $wpdb;
    $emails = array();
    if ( $user_id ) {
        $user = get_user_by( 'id', $user_id );
        if ( isset( $user->user_email ) ) {
            $emails[] = $user->user_email;
        }
    }
    if ( is_email( $customer_email ) ) {
        $emails[] = $customer_email;
    }
    if ( sizeof( $emails ) == 0 ) {
        return false;
    }
    $order = $wpdb->get_row( $wpdb->prepare( "
            SELECT posts.post_modified
            FROM {$wpdb->prefix}woocommerce_order_items as order_items
            LEFT JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS itemmeta ON order_items.order_item_id = itemmeta.order_item_id
            LEFT JOIN {$wpdb->postmeta} AS postmeta ON order_items.order_id = postmeta.post_id
            LEFT JOIN {$wpdb->posts} AS posts ON order_items.order_id = posts.ID
            WHERE
                posts.post_status IN ( 'wc-completed', 'wc-processing' ) AND
                itemmeta.meta_value  = %s AND
                itemmeta.meta_key    IN ( '_variation_id', '_product_id' ) AND
                postmeta.meta_key    IN ( '_billing_email', '_customer_user' ) AND
                (
                    postmeta.meta_value  IN ( '" . implode( "','", array_unique( $emails ) ) . "' ) OR
                    (
                        postmeta.meta_value = %s AND
                        postmeta.meta_value > 0
                    )
                )
            ", $product_id, $user_id ), ARRAY_A );
    if ( ! empty( $order ) ) {
        return $order['post_modified'];
    }

    return false;
}

// For getting order date for already purchased product - end
/*27032015 - end*/

function html_page_title( $title ) {
    //HTML tag opening/closing brackets
    $title = str_replace( '[', '<', $title );
    $title = str_replace( '[/', '</', $title );

    //<strong></strong>
    $title = str_replace( 'span]', 'span>', $title );

    return $title;
}

add_filter( 'the_title', 'html_page_title' );

function load_fonts() {
    wp_register_style( 'et-googleFonts', '//fonts.googleapis.com/css?family=Open+Sans:400,700,600' );
    wp_enqueue_style( 'et-googleFonts' );
}

add_action( 'wp_print_styles', 'load_fonts' );

function print_menu_shortcode( $atts, $content = null ) {
    extract( shortcode_atts( array( 'name' => null, ), $atts ) );

    return wp_nav_menu( array( 'menu' => $name, 'echo' => false ) );
}

add_shortcode( 'menu', 'print_menu_shortcode' );

add_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

function custom_pre_get_posts_query( $q ) {

    if ( ! $q->is_main_query() ) {
        return;
    }
    if ( ! $q->is_post_type_archive() ) {
        return;
    }

    if ( ! is_admin() && is_shop() ) {

        $q->set( 'tax_query', array(
            array(
                'taxonomy' => 'product_cat',
                'field'    => 'slug',
                'terms'    => array( 'RedBoxContent' ),
                // Don't display products in the knives category on the shop page
                'operator' => 'NOT IN'
            )
        ) );

    }

    remove_action( 'pre_get_posts', 'custom_pre_get_posts_query' );

}

add_filter( 'woocommerce_product_add_to_cart_text', 'woo_custom_cart_button_text' );

function woo_custom_cart_button_text() {

    return __( 'Add To Cart', 'woocommerce' );

}

add_action( 'after_setup_theme', 'declare_sensei_support' );
function declare_sensei_support() {
    add_theme_support( 'sensei' );
}

class CSS_Menu_Walker extends Walker {

    var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat( "\t", $depth );
        $output .= "\n$indent<ul>\n";
    }

    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat( "\t", $depth );
        $output .= "$indent</ul>\n";
    }

    function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        global $wp_query;
        $indent      = ( $depth ) ? str_repeat( "\t", $depth ) : '';
        $class_names = $value = '';
        $classes     = empty( $item->classes ) ? array() : (array) $item->classes;

        /* Add active class */
        if ( in_array( 'current-menu-item', $classes ) ) {
            $classes[] = 'active';
            unset( $classes['current-menu-item'] );
        }

        /* Check for children */
        $children = get_posts( array(
            'post_type'   => 'nav_menu_item',
            'nopaging'    => true,
            'numberposts' => 1,
            'meta_key'    => '_menu_item_menu_item_parent',
            'meta_value'  => $item->ID
        ) );
        if ( ! empty( $children ) ) {
            $classes[] = 'has-sub';
        }

        $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
        $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';

        $id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $item->ID, $item, $args );
        $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

        $output .= $indent . '<li' . $id . $value . $class_names . '>';

        $attributes = ! empty( $item->attr_title ) ? ' title="' . esc_attr( $item->attr_title ) . '"' : '';
        $attributes .= ! empty( $item->target ) ? ' target="' . esc_attr( $item->target ) . '"' : '';
        $attributes .= ! empty( $item->xfn ) ? ' rel="' . esc_attr( $item->xfn ) . '"' : '';
        $attributes .= ! empty( $item->url ) ? ' href="' . esc_attr( $item->url ) . '"' : '';

        $item_output = $args->before;
        $item_output .= '<a' . $attributes . '><span>';
        $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
        $item_output .= '</span></a>';
        $item_output .= $args->after;

        $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
    }

    function end_el( &$output, $item, $depth = 0, $args = array() ) {
        $output .= "</li>\n";
    }

    /*
    // Apply filter
    add_filter( 'get_avatar' , 'default_avatar' , 1 , 5 );

    function my_custom_avatar( $avatar, $id_or_email, $size, $default, $alt ) {
        $user = false;

        if ( is_numeric( $id_or_email ) ) {

            $id = (int) $id_or_email;
            $user = get_user_by( 'id' , $id );

        } elseif ( is_object( $id_or_email ) ) {

            if ( ! empty( $id_or_email->user_id ) ) {
                $id = (int) $id_or_email->user_id;
                $user = get_user_by( 'id' , $id );
            }

        } else {
            $user = get_user_by( 'email', $id_or_email );
        }

        if ( $user && is_object( $user ) ) {

            if ( $user->data->ID == '1' ) {
                $avatar = 'YOUR_NEW_IMAGE_URL';
                $avatar = "<img alt='{$alt}' src='{$avatar}' class='avatar avatar-{$size} photo' height='{$size}' width='{$size}' />";
            }

        }

        return $avatar;
    }
    */
}

function register_my_menu() {
    register_nav_menu( 'header-menu', __( 'Graduates Menu' ) );
}

add_action( 'init', 'register_my_menu' );

function my_custom_display_topic_index_query() {
    $args['orderby'] = 'title';
    $args['order']   = 'ASC';

    return $args;
}

add_filter( 'bbp_before_has_topics_parse_args', 'my_custom_display_topic_index_query' );

function followUs_icons() {
    ob_start();
    ?>
    <ul class="follow-tathata">
        <li>Follow US</li>
        <li>
            <a href="<?php echo esc_url( 'https://www.facebook.com/pages/Tathata-Golf/526700354108550' ); ?>" target="_blank"><i class="fa fa-facebook"></i></a>
        </li>
        <li>
            <a href="<?php echo esc_url( 'https://twitter.com/TG_certified' ); ?>" target="_blank"><i class="fa fa-twitter"></i></a>
        </li>
        <li>
            <a href="<?php echo esc_url( 'https://www.linkedin.com/company/tathata-golf-certified' ); ?>" target="_blank"><i class="fa fa-linkedin"></i></a>
        </li>
        <li>
            <a href="<?php echo esc_url( 'https://i.instagram.com/tg_certified' ); ?>" target="_blank"><i class="fa fa-instagram"></i></a>
        </li>
        <li>
            <a href="<?php echo esc_url( 'https://www.youtube.com/channel/UCG1AlyrH_eqgQ7cekyYc3Ig' ); ?>" target="_blank"><i class="fa fa-youtube"></i></a>
        </li>
    </ul>
    <?php
    return ob_get_clean();
}

add_shortcode( 'followUs', 'followUs_icons' );

/** *************************************************
 *   Woocommerce template actions
 *
 * ****************************************************** */

add_action( 'woocommerce_before_cart', function() {
    ?>
    <div class="cart-top-content-holder">
        <h3 class="cont-title text-uppercase"> CHECKOUT</h3>
        <p class="return-to-shop cart-continue-shopping-holder">
            <a class="button wc-backward" href="<?php echo apply_filters( 'woocommerce_return_to_shop_redirect', get_permalink( wc_get_page_id( 'shop' ) ) ); ?>"><?php _e( 'Continue Shopping', 'woocommerce' ) ?></a>
        </p>
    </div>
    <?php
} );

add_action( 'woocommerce_after_checkout_form', function() { // Pulled out of form-checkout, should be moved to theme js file
    ?>
    <script type="text/javascript">
        (function($) {
            $(document).ready(function() {
                $('#billing_phone').attr("placeholder", "Phone");
            });
        })(jQuery);
    </script>
    <?php
} );

add_action( 'woocommerce_shop_loop_item_title', function() {
    ?><h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5><?php
} );

/** *************************************************
 *   Instructor Reviews
 *
 * ****************************************************** */

/**
 *   Automatic inactivate status for instructors when receiveing X number of bad reviews
 */
add_action( 'gform_entry_created', function( $entry, $form ) {

    $review_form_id      = 6; // Hard-coded to id for now
    $rating_field_id     = 3; // Hard-coded to field id for now
    $instructor_field_id = 15; // Hard-coded to field id for now
    $activated_field_id  = 16; // Hard-coded to field id for now

    // Check for proper survey form, hard-coded to id for now
    if ( ! isset( $form['id'] ) || $form['id'] != $review_form_id ) {
        return;
    }

    // Check for rating field found and of proper type
    if ( ! isset( $entry[ $rating_field_id ] ) || ! preg_match( '#^[0-9]+$#ismu', $entry[ $rating_field_id ] ) ) {
        return;
    }
    $rating = (int) $entry[ $rating_field_id ];

    // Check for instructor field found and of proper type
    if ( ! isset( $entry[ $instructor_field_id ] ) || ! preg_match( '#^[0-9]+$#ismu', $entry[ $instructor_field_id ] ) ) {
        return;
    }
    $instructor_id = (int) $entry[ $instructor_field_id ];

    // Not bad rating, bail
    if ( $rating >= 3 ) {
        return;
    }

    // Get counts of user's reviews
    $total_review_count = GFAPI::count_entries( $review_form_id, [
            'field_filters' => [
                [
                    'key'   => $instructor_field_id,
                    'value' => $instructor_id
                ]
            ]
        ] ) + 1;

    $low_review_count = GFAPI::count_entries( $review_form_id, [
            'field_filters' => [
                [
                    'key'   => $instructor_field_id,
                    'value' => $instructor_id
                ],
                [
                    'key'      => $rating_field_id,
                    'operator' => 'not in',
                    'value'    => [ 3, 4, 5 ]
                ]
            ]
        ] ) + 1;

    // Less than 4 reviews, bail as not enough data
    if ( $total_review_count < 4 ) {
        return;
    }

    // if low review count laess than 50%, bail
    $percent_low = ( $low_review_count / $total_review_count ) * 100;
    if ( $percent_low < 50 ) {
        return;
    }

    // Inactive profile, set inactive field on entry for notifications
    update_user_meta( $instructor_id, 'profile_active', 0 );
    GFAPI::update_entry_field( $entry['id'], $activated_field_id, 1 );

}, 10, 2 );

/** *************************************************
 *   Subscription changes for activating/deactivating profiles
 * ****************************************************** */

define( 'MARKETING_SUBSCRIPTION_ID', 60124 );

add_action( 'activated_subscription', function( $user_id, $subscription_key ) {

    // Invalid subscription key, bail
    if ( ! preg_match( '#_([0-9]+)$#ismu', $subscription_key, $matches ) ) {
        return;
    }

    $product_id = (int) $matches[1];

    // Not marketing subscription, bail
    if ( $product_id !== MARKETING_SUBSCRIPTION_ID ) {
        return;
    }

    // Updated user profile active flag
    update_user_meta( $user_id, 'profile_active', 1 );

}, 10, 2 );

add_action( 'cancelled_subscription', function( $user_id, $subscription_key ) {

    // Invalid subscription key, bail
    if ( ! preg_match( '#_([0-9]+)$#ismu', $subscription_key, $matches ) ) {
        return;
    }

    $product_id = (int) $matches[1];

    // Not marketing subscription, bail
    if ( $product_id !== MARKETING_SUBSCRIPTION_ID ) {
        return;
    }

    // Updated user profile active flag
    update_user_meta( $user_id, 'profile_active', 0 );

}, 10, 2 );

add_action( 'woocommerce_subscription_status_updated', function( $subscription, $new_status, $old_status ) {

    // Check user subscription status
    $subscription_active = (int) wcs_user_has_subscription( $subscription->get_user_id(), MARKETING_SUBSCRIPTION_ID, 'active' );

    // Updated user profile active flag
    update_user_meta( $subscription->get_user_id(), 'profile_active', $subscription_active );

}, 10, 3 );

// Change the date format of the Sensei certs
add_filter( 'sensei_certificate_date_format', 'sensei_reorder_certificate_date' );

function sensei_reorder_certificate_date() {
    $date_format = 'F jS, Y';

    return $date_format;
}

/*--------------------------------------------------------------------------------------------------
  Custom Functionality
--------------------------------------------------------------------------------------------------*/
require 'inc/enqueues.php';
require 'inc/custom-pagination.php';
require 'inc/custom-acf.php';
require 'inc/custom-functions.php';
require 'inc/custom-woocommerce.php';
require 'sensei/custom/actions.php';
