<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Fourteen
 * already has tag.php for Tag archives, category.php for Category archives,
 * and author.php for Author archives.
 */

get_header();
?>

<?php if ( have_posts() ) : ?>

    <?php get_template_part( 'parts/page-title' ); ?>

    <div id="blackbar"></div>

    <?php get_template_part( 'parts/breadcrumbs' ); ?>

    <section class="contentWrapper">
        <article class="pageInnerContentWrap ourFacilitiesPage">
            <div class="container">
                <div class="row onlineTopwrap broken-apart-page-row">
                    <div class="row-inner">
                        <article class="col-sm-12">
                            <div id="form-main">
                                <div id="form-div">
                                    <?php wp_nav_menu( array(
                                        'menu'         => 'video menu',
                                        'container_id' => 'owncssmenu'
                                    ) ); ?>
                                </div>
                            </div>

                            <div>
                                <h1 class="broken-apart-title"> <?php echo get_the_title(); ?></h1>
                            </div>

                            <div class="broken-videos-wrap">
                                <?php
                                // Start the Loop.
                                while ( have_posts() ) : the_post();

                                    /*
                            			 * Include the post format-specific template for the content. If you want to
                            			 * use this in a child theme, then include a file called called content-___.php
                            			 * (where ___ is the post format) and that will be used instead.
                            			 */
                                    get_template_part( 'content', get_post_format() );

                                endwhile;
                                // Previous/next page navigation.
                                twentyfourteen_paging_nav();

                            else :
                                // If no content, include the "No posts found" template.
                                get_template_part( 'content', 'none' );

                            endif;
                            ?>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </article>
    </section>

    <div class="clearfix"></div>

<?php
get_footer();
