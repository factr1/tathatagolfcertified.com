<?php
// Template Name: Program Video Library

get_header();
getPageHero();
?>

    <div id="blackbar"></div>
<?php get_template_part( 'parts/breadcrumbs' ); ?>

    <section id="tg-broken-apart" class="tg-broken-apart">
        <div class="container">

            <div class="intro-content">
                <?php the_field( 'intro_content' ); ?>
            </div>

            <div class="main-content">
                <div class="row">
                    <div class="col-xs-12">
                        <?php the_field( 'main_content' ); ?>
                    </div>
                </div>
            </div>

            <div class="library-breakdown">
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Select your training video by category:</h2>
                        <?php wp_nav_menu( array( 'menu' => 'Broken Apart', 'container_id' => 'video-library' ) ); ?>
                    </div>
                </div>
            </div>

            <div class="additional-content">
                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-sm-offset-2">
                        <?php the_field( 'additional_content' ); ?>
                    </div>
                </div>
            </div>

        </div>
    </section>

<?php
get_footer();
