<?php
/**
 * Template Name: Testing New Features
 */

get_header();
getPageHero();
?>

    <section id="test-body">

        <section class="test-section">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <?php
                        if ( have_posts() ) : while ( have_posts() ) : the_post();
                            the_content();
                        endwhile; endif;
                        ?>
                    </div>
                </div>
            </div>
        </section>

    </section>

<?php
get_footer();
