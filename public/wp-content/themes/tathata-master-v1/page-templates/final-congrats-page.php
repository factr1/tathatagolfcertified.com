<?php
/**
 * Template Name: Final Congrats Page
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */
get_header();
?>

<?php

global $post, $woothemes_sensei, $current_user;

// Get User Meta
get_currentuserinfo();

// get status of day 60 quiz
$day_60_id = 2624;
$status    = WooThemes_Sensei_Utils::sensei_user_quiz_status_message( $day_60_id, $current_user->ID );

// if day 60 quiz pass or 100%
if ( $status['status'] == "passed" ) {

} //endif day 60 quiz pass or 100%

?>

<?php get_template_part( 'parts/page-title' ); ?>

    <div id="blackbar"></div>

    <div class="container">
        <div class="row">
            <div class="clearfix"></div>
            <div class="breadcrumbs-wrapper">
                <ol class="breadcrumb pull-left">
                    <li><a href="<?php echo site_url(); ?>">Home</a></li>
                    <?php if ( ! empty( $post->post_parent ) ) {
                        $parentTitle = get_the_title( $post->post_parent ); ?>
                        <li>
                            <a href="<?php echo get_permalink( $post->post_parent ); ?>" title="<?php echo $parentTitle; ?>"><?php echo $parentTitle; ?></a>
                        </li>
                    <?php } ?>
                    <li>Congratulations</li>
                </ol>
                <?php echo do_shortcode( '[followUs]' ); ?>
            </div>
        </div>
    </div>

    <div class="container final-congrats-wrap">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="congrats-title">Congratulations!</h1>
                <p class="congrats-message"> You have completed the Tathata Golf 60-Day Training Program</p>
            </div>
        </div>
    </div>

    <div class="final-congrats-graphics-wrap">
        <div class="final-congrats-graphics">
            <img src="/wp-content/uploads/sites/5/2015/06/final-congrats-global-graphic.png">
        </div>
    </div>

    <section class="contentWrapper">
        <div class="container final-congrats-wrap">
            <?php
            while ( have_posts() ) : the_post();
                the_content();
            endwhile;
            ?>
        </div>
    </section>

    <div class="clearfix"></div>

<?php
get_footer();
