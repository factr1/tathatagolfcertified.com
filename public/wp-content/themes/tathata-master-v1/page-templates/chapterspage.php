<?php
/**
 * Template Name: Chapters Page
 */

get_header();
$current_url = home_url( add_query_arg( array(), $wp->request ) );
$uriArray    = explode( '/', $current_url );
do_action( 'sensei_course_single_chapter', end( $uriArray ) );
global $course_module, $course_module_lessons;
$chapter_name     = $course_module->name;
$chap_name_arr    = explode( ":", $chapter_name );
$chapter_name_arr = explode( " ", $chap_name_arr[0] );
$chapter          = $chapter_name_arr[0];
$chapter_num      = ( strlen( $chapter_name_arr[1] ) < 2 ) ? "0" . $chapter_name_arr[1] : $chapter_name_arr[1];
$course_id        = $_GET['course_id'];
$args             = array(
    'post_type'  => 'page',
    'meta_query' => array(
        array(
            'key' => 'faq'
        )
    )
);
$meta_det         = new WP_Query( $args );
?>

<?php get_template_part( 'parts/page-title' ); ?>

    <div class="container">
        <div class="row">
            <ol class="breadcrumb pull-left">
                <li><a href="#">Home</a></li>
                <li><a href="<?php echo get_site_url(); ?>/courses">Training Programs</a></li>
                <li><a href="<?php echo get_site_url(); ?>/course-chapters/<?php echo $course_id; ?>">60 Day Program</a></li>
                <li><a href="#">Chapter-<?php echo $chapter_num; ?></a></li>
            </ol>
        </div>
    </div>

    <section class="pageInnerContentWrap">
        <div class="container">
            <div class="row">
                <article class="col-sm-6 col-md-7">
                    <div class="welcomeToHeading">
                        <h3>Welcome to</h3>

                        <?php
                        $chapter_name     = $course_module->name;
                        $chap_name_arr    = explode( ":", $chapter_name );
                        $chapter_name_arr = explode( " ", $chap_name_arr[0] );
                        $chapter          = $chapter_name_arr[0];
                        $chapter_num      = ( strlen( $chapter_name_arr[1] ) < 2 ) ? "0" . $chapter_name_arr[1] : $chapter_name_arr[1];
                        ?>

                        <ul>
                            <li><?php echo $chapter; ?><span><?php echo $chapter_num; ?></span></li>
                            <li>Days<span>1-10</span></li>
                        </ul>

                        <div class="clearfix"></div>

                        <h1><?php echo $chap_name_arr[1]; ?></h1>
                    </div>

                    <p><?php echo $course_module->description; ?></p>

                    <div class="clearfix"></div>

                    <?php $termid = $course_module->term_id; ?>

                    <p class="chapterWritten writtenBy text-right"><?php echo the_field( 'authorname', 'module_' . $termid ); ?></p>

                    <div class="clearfix"></div>
                </article>

                <article class="col-sm-6 col-md-5 rightPanel chaptersPageRight">
                    <div class="chaptersPageRightContent">
                        <div class="courseInProgress">
                            <h5>Course in Process...</h5>
                        </div>

                        <h3>Study Guides</h3>

                        <ul class="studyGuideList">
                            <?php
                            $pdfurl  = get_field( 'studyguidepdf', 'module_' . $termid );
                            $pdf_arr = explode( ".", $pdfurl );

                            if ( end( $pdf_arr ) == "pdf" ) {
                                ?>
                                <li>
                                    <a href="<?php echo $pdfurl; ?>" download> Chapter <?php echo $chapter_num; ?> Study Guide<span>Download</span>
                                    </a>
                                </li>
                                <?php
                            } else {
                                ?>
                                <li>
                                    There is no Study Guide
                                </li>
                                <?php
                            }
                            ?>
                        </ul>

                        <div class="clearfix"></div>

                        <h3>FAQ&#39;s</h3>

                        <?php
                        $faq = get_field( 'faq', $meta_det->post->ID );
                        echo $faq;
                        ?>
                    </div>
                </article>

                <div class="clearfix"></div>

                <article class="col-sm-6 col-md-7">
                    <div class="progressBarWrap">
                        <?php
                          $current_user = wp_get_current_user();
                          $course_lessons    = $course_module_lessons;
                          $total_lessons     = count( $course_lessons );
                          $lessons_completed = 0;
                          foreach ( $course_lessons as $lesson ) {
                            if ( WooThemes_Sensei_Utils::user_completed_lesson( $lesson->ID, $user->ID ) ) {
                              ++ $lessons_completed;
                            }
                          }
                          $html                = '<span class="course-completion-rate">PROGRESS OF CHAPTER ' . $chapter_num . "</span>";
                          $progress_percentage = abs( round( ( doubleval( $lessons_completed ) * 100 ) / ( $total_lessons ), 0 ) );
                          $html .= "<div id = 'bar4' class = 'barfiller'>
                                <div class = 'tipWrap'>
                                <span class = 'tip'></span>
                                </div>
                                <span class = 'fill' data-percentage = '$progress_percentage'></span>
                                </div><div style='display:inline;'>$progress_percentage%</div>";
                          echo $html;
                        ?>
                      </div>
                </article>

                <article class="col-sm-6 col-md-5 rightPanel">
                    <div class="training-actions">
                        <a href="<?php echo get_site_url() . "/course-chapters/" . $course_id; ?>" class="training-actions--button"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/60DayWhiteIcon.png" alt=""></a>
                        <a href="<?php echo site_url() . "/days-overview/" . $course_module_lessons[0]->ID . "/?course_id=" . $course_id; ?>" class="training-actions--button">Continue Training</a>
                    </div>
                </article>
            </div>
        </div>
    </section>

    <section class="tabSectionWrap">
        <div class="container">
            <div class="row">
                <div id="parentHorizontalTab">
                    <ul class="resp-tabs-list hor_1">
                        <li>Chapter Overview</li>
                        <li>Day Lessons</li>
                        <li>Online Academy</li>
                    </ul>

                    <div class="resp-tabs-container hor_1">
                        <div class="chapterOverViewTab welcomeToHeading">
                            <div class="chap-left">
                                <div class="chap-overview-head">
                                    <ul>
                                        <li><?php echo $chapter; ?><span><?php echo $chapter_num; ?></span></li>
                                        <li>Days<span>1-10</span></li>
                                    </ul>
                                </div>

                                <div class="chap-overview">
                                    <div class="full-div">
                                        <h4>OVERVIEW</h4>
                                    </div>
                                    <div class="full-div">
                                        <?php
                                        $chap_overview = get_field( 'chapter_overview_tab', "module_" . $termid );
                                        echo $chap_overview;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="dayLessonsTab">
                            <h3>Day Lessons</h3>

                            <div class="columnContentWrap">
                                <?php
                                $day = 1;
                                foreach ( $course_module_lessons as $value ) {
                                    $day_temp = $day;
                                    $cur_day  = ( strlen( $day ) > 1 ) ? $day : "0" . (string) $day_temp;
                                    ?>
                                    <div class="columnContent">
                                        <h4>Day <span><?php echo $cur_day; ?></span></h4>
                                        <div class="videoContainer">
                                            <?php
                                            if ( 0 < intval( $value->ID ) ) {
                                                $lesson_video_embed = get_post_meta( $value->ID, '_lesson_video_embed', true );
                                                if ( 'http' == substr( $lesson_video_embed, 0, 4 ) ) {
                                                    $lesson_video_embed = wp_oembed_get( esc_url( $lesson_video_embed ));
                                                }
                                                if ( strlen( $lesson_video_embed ) > 0 ) {
                                                    echo html_entity_decode( $lesson_video_embed );
                                                } else {
                                                    ?>
                                                    <img style="width: 100%;" src="<?php echo get_template_directory_uri() . "/assets/img/no-videos.jpeg" ?>">
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </div>

                                        <ul class="post-det">
                                            <li><?php echo $value->post_title; ?></li>
                                        </ul>

                                        <div class="clearfix"></div>
                                    </div>
                                    <?php
                                    $day ++;
                                }
                                ?>
                            </div>
                        </div>

                        <div class="onlineAcademyTab">
                            <p>Tab 3 Container</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix"></div>

<?php
get_footer();
