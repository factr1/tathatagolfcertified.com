<?php
/**
 * Template Name: Day Page
 */

global $wp, $woothemes_sensei, $course_module, $course_module_lessons, $wp, $course_lessons_arr, $current_user;

$current_url  = add_query_arg( $wp->query_string, '', home_url( $wp->request ) );
$keys         = parse_url( $current_url ); // parse the url
$path         = explode( "/", $keys['path'] ); // splitting the path
$last         = end( $path ); // get the value of the last element
$content_post = get_post( $last );
$content      = $content_post->post_content;
$course_id    = $_GET['course_id'];

$terms      = get_the_terms( $last, 'module' );
$termid     = ( $terms[0]->parent == 0 ) ? $terms[0]->term_id : $terms[0]->parent;
$prev_post  = get_previous_post();
$siteurl    = get_site_url();
$prev_title = $prev_post->post_title;
$prev_url   = $siteurl . '/' . $prev_title . '/' . $termid . '/' . '?course_id=' . $course_id;
do_action( 'sensei_course_single_chapter', $termid );

$args             = array(
    'user_id'          => $current_user->data->ID,
    'site_id'          => get_current_blog_id(),
    'achievement_id'   => false,
    'achievement_type' => false,
    'since'            => 0,
);
$badges           = badgeos_get_user_achievements( $args );
$chapter_name     = $course_module->name;
$chap_name_arr    = explode( ":", $chapter_name );
$chapter_name_arr = explode( " ", $chap_name_arr[0] );
$chapter          = $chapter_name_arr[0];
$chapter_num      = ( strlen( $chapter_name_arr[1] ) < 2 ) ? "0" . $chapter_name_arr[1] : $chapter_name_arr[1];
$curr_key         = array_search( $last, $course_lessons_arr );
$day              = $curr_key + 1;
$day              = ( strlen( $day ) > 1 ) ? (string) $day : "0" . (string) $day;
$next_url         = isset( $course_module_lessons[ $curr_key + 1 ] ) ? site_url() . "/days-overview/" . $course_module_lessons[ $curr_key + 1 ]->ID . "/?course_id=" . $course_id : "javascript:void(0)";

get_header();
get_template_part( 'parts/page-title' );
?>

    <!-- Breadcrumb Area -->
    <section id="breadcrumbs">
        <div class="hidden-xs">
            <div class="container">
                <div class="row">

                    <div class="col-sm-8">
                        <div class="flex -flex-start">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/course/certified-movement-specialist-program/' ) ); ?>">Home</a>
                                </li>
                                <li><a href="<?php echo esc_url( home_url( '/courses' ) ); ?>">Training Programs</a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/course-chapters/' . $course_id ) ); ?>">Certified Program</a>
                                </li>
                                <li>Chapter <?php echo $chapter_num; ?></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="flex -flex-end">
                            <?php echo do_shortcode( '[followUs]' ); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="visible-xs">
            <div class="container">
                <div class="flex">
                    <div>
                        <ul class="breadcrumb">
                            <li>
                                <a href="<?php echo esc_url( home_url( '/course/certified-movement-specialist-program/' ) ); ?>">Home</a>
                            </li>
                            <li><a href="<?php echo esc_url( home_url( '/courses' ) ); ?>">Training Programs</a></li>
                            <li>
                                <a href="<?php echo esc_url( home_url( '/course-chapters/' . $course_id ) ); ?>">Certified Program</a>
                            </li>
                            <li>Chapter <?php echo $chapter_num; ?></li>
                        </ul>
                    </div>

                    <div>
                        <?php echo do_shortcode( '[followUs]' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pageInnerContentWrap profilePage daysPageWrap">
        <div class="container">
            <div class="row">
                <article class="col-sm-7">
                    <?php
                    $lesson_video_embed = get_post_meta( $last, '_lesson_video_embed', true );
                    ?>
                    <div id="video-container">
                        <?php
                        if ( strlen( $lesson_video_embed ) < 1 ) {
                            echo "There is no video found for this lesson";
                        } else {
                            echo html_entity_decode( $lesson_video_embed );
                        }
                        ?>
                    </div>

                    <div class="clearfix"></div>

                    <div class="under-vid-conts">
                        <?php
                        if ( get_field( "undervids" ) ):
                            the_field( "undervids" );
                        endif;
                        ?>
                    </div>

                    <div class="lessonDay">
                        <h2>DAY <span><?php echo $day; ?></span></h2>
                    </div>

                    <div class="lesson-badges">
                        <h6>Lesson Media</h6>

                        <div class="clearfix"></div>

                        <ul class="studyGuideList">
                            <?php
                            $lesson_pdf_attchaed = get_post_meta( $last, '_attached_media', true );
                            $attach_exists       = false;
                            foreach ( $lesson_pdf_attchaed as $value ) {
                                $pdf_end = explode( ".", $value );
                                if ( "pdf" == end( $pdf_end ) ) {
                                    $attach_exists = true;
                                }
                            }
                            if ( $attach_exists == true ) {
                                foreach ( $lesson_pdf_attchaed as $key => $value ) {
                                    $pdf_end = explode( ".", $value );
                                    if ( "pdf" == end( $pdf_end ) ) {
                                        ?>
                                        <li>
                                            <a href="<?php echo $value; ?>"> Day <?php echo $day; ?> <?php echo get_the_title( $last ); ?>
                                                                             Media <?php echo $key + 1; ?>
                                                <span>Download </span> </a>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>

                                <?php
                            } else {
                                ?>
                                <li>
                                    There is no Study Guide
                                </li>
                                <?php
                            }
                            ?>
                        </ul>

                        <ul class="otherProfileImgs">
                            <?php
                            $count = 1;

                            foreach ( $badges as $value ) {
                                if ( $value->post_type == "badges" && $i <= 4 ) {
                                    $attachment_id = get_post_thumbnail_id( $value->ID );
                                    ?>
                                    <li><?php echo wp_get_attachment_image( $attachment_id, array(
                                            50,
                                            50
                                        ), false ); ?></li>
                                    <?php
                                    $i ++;
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </article>

                <article class="col-xs-12 col-sm-5 col-md-5 rightPanel">
                    <div class="clearfix"></div>

                    <h3><?php echo get_the_title( $last ); ?> </h3>

                    <article class="trainingDetailTextWrap">
                        <p><?php echo $content; ?></p>
                    </article>

                    <?php $chapterpdfurl = get_field( 'studyguidepdf', 'module_' . $termid ); ?>

                    <div class="forum-chap">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/suppForumBg.png"><a href="<?php echo site_url() . "/forums/forum/60-day-training-program/"; ?>">FAQ/Forum</a>
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/pdfIcon.png" height="20">
                        <?php
                        $pdf_arr = explode( ".", $chapterpdfurl );

                        if ( end( $pdf_arr ) == "pdf" ) {
                            ?>
                            <a href="<?php echo $chapterpdfurl; ?>" class="studyGuideList" download>Chapter Study Guide</a>
                            <?php
                        } else {
                            ?>
                            <span>There is no Chapter Guide</span>
                            <?php
                        }
                        ?>
                    </div>

                    <div class="ratingContainerWrap">
                        <?php
                        global $j, $rating_span, $loading_span;

                        $video_ratings = do_shortcode( "[ratings id='$last']" );
                        preg_match_all( '/(id|class|data-nonce|itemtype|itemscope)=("[^"]*")/i', $video_ratings, $mat_arr );

                        $rating_span  = implode( " ", array_slice( $mat_arr[ 0 ], 0, 4 ) );
                        $loading_span = implode( " ", array_slice( $mat_arr[ 0 ], - 3, 2 ) );

                        $j = 1;

                        function pre_entities( $matches ) {
                        global $j, $rating_span, $loading_span;

                        if ( $j == 1 ) {
                            $j           = $j + 1;
                            $pos         = strpos( $matches[ 1 ], '<strong>' );
                            $end         = strpos( $matches[ 1 ], '</strong>' );
                            $votes_count = (int) substr( $matches[ 1 ], $pos + 8, $end );

                            if ( $votes_count >= 1 ) {
                                $votes_count  = "(<em><strong>$votes_count</strong> vote(s)</em>)";
                                $matches[ 1 ] = str_replace( array( '(', ')' ), '', $matches[ 1 ] );
                            } else {
                                $votes_count = "";
                            }

                            $matches[ 1 ] = preg_replace( '/<(em)(?:(?!<\/\1).)*?<\/\1>/s', '', $matches[ 1 ] );
                            $con = "<span " . $rating_span . ">" . preg_replace( "/(^)?(<br\s*\/?>\s*)+$/", "", $matches[ 1 ] );
                            $con .= $votes_count;
                            $con .= "<span class='rate-this-video'>Rate this Video</span></span>";

                            return $con;
                        } else {
                            return "<span " . $loading_span . ">" . preg_replace( "/(^)?(<br\s*\/?>\s*)+$/", "", $matches[ 1 ] ) . "</span>";
                        }
                        }

                        $content = preg_replace_callback( '/<span.*?>(.*?)<\/span>/imsu', pre_entities, $video_ratings );
                        echo $content;
                        ?>
                    </div>

                    <?php
                    global $quiz_id;
                    do_action( 'sensei_lesson_quiz_meta_custom', $last, $current_user->ID );
                    ?>

                    <div class="training-actions">
                        <a href="javascript:void(0)" class="training-actions--button">Back</a>
                        <a href="<?php echo site_url() . "/lesson-quiz/" . $quiz_id; ?>" class="training-actions--button">Lesson Test</a>
                        <a href="<?php echo $prev_url; ?>" class="training-actions--link">Back to Chapter</a>
                        <a href="<?php echo $next_url; ?>" class="training-actions--link">Next Day</a>
                    </div>

                    <ul class="shareUs pull-right">
                        <li>SHARE US</li>
                        <?php
                        if ( function_exists( 'ssb_share_icons' ) ) {
                            echo ssb_share_icons();
                        } else {
                            echo "s";
                        }
                        ?>
                    </ul>
                </article>

                <div class="clearfix"></div>
            </div>
        </div>
    </section>

    <section class="tabSectionWrap">
        <div class="container">
            <div class="row">
                <div id="parentHorizontalTab">
                    <ul class="resp-tabs-list hor_1">
                        <li>Reviews</li>
                    </ul>

                    <div class="resp-tabs-container hor_1">
                        <div class="chapterOverViewTab">
                            <h3 class="reportLink">Report a Post / Review</h3>

                            <?php
                            $comments = get_comments( array(
                                'number'  => 2,
                                'post_id' => $last,
                                'order'   => 'DESC',
                                'orderby' => 'comment_ID',
                            ) );

                            if ( count( $comments ) < 1 ) {
                                echo "There is no review";
                            }
                            foreach ( $comments as $key => $value ) {
                                ?>
                                <div class="commtWrap">
                                    <ul class="commtList">
                                        <li>
                                            <div class="authorImg"><?php echo get_avatar( $comment, 32 ); ?></div>

                                            <div class="authorNameCommt">
                                                <h5><?php echo $comments[ $key ]->comment_author; ?></h5>
                                                <p class="postedDate">posted at <?php
                                                    $getdate1   = $comments[ $key ]->post_date;
                                                    $datechange = date( 'H:i d F', strtotime( $getdate1 ) );
                                                    echo $datechange;
                                                    ?>
                                                </p>
                                                <p><?php echo $comments[ $key ]->comment_content; ?></p>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            <?php } ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix"></div>

<?php
get_footer();
