<?php
/**
 * Template Name: MS Training Broken Apart
 */

get_header();
get_template_part( 'parts/page-title' );
?>

    <div id="blackbar"></div>

    <?php get_template_part( 'parts/breadcrumbs' ); ?>

    <section class="contentWrapper">
        <article class="pageInnerContentWrap ourFacilitiesPage">
            <div class="container">
                <div class="row onlineTopwrap broken-apart-page-row">
                    <div class="row-inner">
                        <section class="col-xs-12">
                            <h3 style="margin-top: 20px; font-size: 25px; margin-bottom: 15px;">Welcome to the Movement Specialist Training Program
                                <span class="red-light">Broken Apart</span>
                            </h3>
                            <h3>
                                <span class="red-light">Congratulations</span> on graduating from the Movement Specialist Training Program
                            </h3>

                            <?php if ( has_post_thumbnail() ) {
                                the_post_thumbnail( 'full' );
                            } ?>
                        </section>

                        <article class="col-md-6 " style="padding:40px 20px 20px;">
                            <?php
                            while ( have_posts() ) : the_post();
                                the_content();
                            endwhile;
                            ?>
                        </article>

                        <section class="col-md-6" style="padding:20px;"></section>

                        <section class="col-xs-12" style="margin-top:25px;">
                            <h2>Select your training video by category: </h2>
                            <?php wp_nav_menu( array(
                                'menu'         => 'Broken Apart',
                                'container_id' => 'brokenoutlist'
                            ) ); ?>
                        </section>
                    </div>
                </div>
            </div>
        </article>
    </section>

    <div class="clearfix"></div>

<?php
get_footer();
