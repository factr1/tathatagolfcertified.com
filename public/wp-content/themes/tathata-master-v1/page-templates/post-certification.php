<?php
/**
 * Template Name: Post Certification
 * This page displays after someone has successfully become a Certified Movement Specialist
 */

get_header();
?>

    <!-- Post Certification Area -->
    <section id="post-certification" class="post-cert">
        <div class="container">
            <h1 class="post-cert-title"><?php echo get_the_title(); ?></h1>

            <?php the_field( 'pc_intro_text' ); ?>

            <nav class="pc-items">
                <?php if ( have_rows( 'pc_action_items' ) ) : ?>
                    <ul>
                        <?php while ( have_rows( 'pc_action_items' ) ) : the_row(); ?>
                            <?php
                            $title   = get_sub_field( 'ai_title' );
                            $liClass = str_replace( ' ', '', $title );
                            ?>
                            <li class="<?php echo $liClass; ?>">
                            <?php if ( get_sub_field( 'ai_internal_link' ) ) : ?>
                                <a href="<?php the_sub_field( 'ai_internal_link' ); ?>">
                            <?php elseif ( get_sub_field( 'ai_external_link' ) ) : ?>
                                <a href="<?php the_sub_field( 'ai_external_link' ); ?>">
                            <?php else : ?>
                                <a data-remodal-target="<?php the_sub_field( 'ai_id_link' ); ?>">
                            <?php endif; ?>
                                    <div class="pc-single-item">
                                        <div class="pc-title">
                                            <h3><?php the_sub_field( 'ai_title' ); ?></h3>
                                        </div>

                                        <div class="pc-icon">
                                            <img src="<?php the_sub_field( 'ai_icon' ); ?>" class="img-responsive" alt="">
                                        </div>
                                    </div>
                                </a>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                <?php endif; ?>
            </nav>
        </div>
    </section>

<?php if ( get_field( 'pc_modal_title' ) ) : ?>
    <div class="remodal pc-modal" data-remodal-id="materials-modal">
        <button data-remodal-action="close" class="remodal-close"></button>
        <h1><?php the_field( 'pc_modal_title' ); ?></h1>

        <div class="flex">
            <?php if ( get_field( 'pc_modal_image' ) ) : ?>
                <div class="pc-modal-image">
                    <img src="<?php the_field( 'pc_modal_image' ); ?>" alt="" class="img-responsive">
                </div>
                <div class="pc-modal-content">
                    <?php the_field( 'pc_modal_content' ); ?>
                </div>
            <?php else : ?>
                <div class="pc-modal-content">
                    <?php the_field( 'pc_modal_content' ); ?>
                </div>
            <?php endif; ?>
        </div>

        <a class="remodal-confirm" href="<?php the_field( 'pc_modal_link' ); ?>">Continue To Print Shop</a>
    </div>
<?php endif; ?>

<?php
get_footer();


