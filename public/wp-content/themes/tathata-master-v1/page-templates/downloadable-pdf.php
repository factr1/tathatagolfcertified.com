<?php
/**
 * Template Name: Downloadable PDF's
 */

get_header();
?>

    <section id="dpdfPage" class="container">

        <!-- Page Title -->
        <h1><?php echo get_the_title(); ?></h1>

        <!-- Intro Content -->
        <?php if ( get_field( 'dpdf_content' ) ) : ?>
            <section class="dpdf-intro">
                <?php the_field( 'dpdf_content' ); ?>
            </section>
        <?php endif; ?>

        <!-- PDF Sections -->
        <?php if ( have_rows( 'dpdf_section' ) ) : while ( have_rows( 'dpdf_section' ) ) : the_row(); ?>
            <section class="dpdf-section">

                <!-- Section Tile -->
                <h3><?php the_sub_field( 'dpdf_title' ); ?></h3>

                <?php if ( have_rows( 'dpdf_pdfs' ) ) : ?>
                    <nav class="dpdf-pdfs">
                        <ul>
                            <?php while ( have_rows( 'dpdf_pdfs' ) ) : the_row(); ?>
                                <li>
                                    <a href="<?php the_sub_field( 'dpdf_ppdf' ); ?>" download>
                                        <?php if ( get_sub_field( 'dpdf_pimage' ) ) : ?>
                                            <img class="dpdf-thumbnail" src="<?php the_sub_field( 'dpdf_pimage' ); ?>">
                                        <?php else : ?>
                                            <img class="dpdf-thumbnailDefault" src="<?php echo get_template_directory_uri(); ?>/assets/img/PostCert-Pdf300.png">
                                        <?php endif; ?>
                                        <h4><?php the_sub_field( 'dpdf_ptitle' ); ?></h4>
                                        <p><?php the_sub_field( 'dpdf_desc' ); ?></p>
                                    </a>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </nav>
                <?php endif; ?>
            </section>
        <?php endwhile; endif; ?>
    </section>

<?php
get_footer();
