<?php
/**
 * The template for displaying the edit profile page
 */

get_header();
?>

<?php get_template_part( 'parts/page-title' ); ?>

    <section class="pageInnerContentWrap">
        <div class="container">
            <div class="row">
                <ol class="breadcrumb pull-left">
                    <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a></li>
                    <li><?php echo get_the_title(); ?></li>
                </ol>

                <?php echo do_shortcode( '[followUs]' ); ?>

                <article class="col-xs-12 col-sm-8">
                    <?php
                    if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
                        // Include the featured content template.
                        get_template_part( 'featured-content' );
                    }
                    ?>

                    <div id="primary" class="content-area">
                        <div id="content" class="site-content" role="main">
                            <?php
                            // Start the Loop.
                            while ( have_posts() ) : the_post();
                                // Include the page content template.
                                get_template_part( 'content', 'page' );

                                // If comments are open or we have at least one comment, load up the comment template.
                                if ( comments_open() || get_comments_number() ) {
                                    comments_template();
                                }
                            endwhile;
                            ?>
                        </div><!-- #content -->
                    </div><!-- #primary -->
                </article>

                <article class="col-xs-12 col-sm-4 rightPanel">
                    <?php get_sidebar(); ?>
                </article>
            </div>
        </div><!-- #main-content -->
    </section>

<?php
get_footer();
