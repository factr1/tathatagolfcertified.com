<?php
// Template Name: Login Template

if ( is_user_logged_in() ) :
    header( "Location: /dashboard" );
    die();
else :
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title><?php wp_title( '|', true, 'right' ); ?></title>

        <link rel="profile" href="//gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

        <meta name="theme-color" content="#000">
        <meta name="msapplication-TileColor" content="#000">
        <meta name="msapplication-TileImage" content="/favicons/ms-icon-144x144.png">

        <link rel="manifest" href="/favicons/manifest.json">
        <link rel="apple-touch-icon" href="apple-touch-icon-iphone.png" />
        <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/Tathata-57x57-White.png" />
        <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/Tathata-72x72-White.png" />
        <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/Tathata-114x114-White.png" />
        <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/assets/img/icons/Tathata-144x144-White.png" />

        <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script><![endif]-->
        <?php wp_head(); ?>
    </head>
<body <?php body_class(); ?> role="document">

    <header>
        <div class="container">
            <div class="row">
                <div class="header-wrap">
                    <aside class="logo pull-left"><a href="<?php echo home_url(); ?>"></a></aside>
                    <aside class="pull-right topRightCotent"></aside>
                </div>
            </div>
        </div>
    </header>

    <!-- Main Content -->
    <main id="primary-site-content" class="contentWrapper" role="main">
        <section id="custom-login" class="login-page">
            <div class="login-content">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-md-9">
                            <div class="-content">
                                <?php the_field( 'login_message' ); ?>
                            </div>
                        </div>

                        <div class="col-xs-12 col-md-3">
                            <div class="-content">
                                <img class="tt-loginLogo img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo/tathata-certified-logo5.svg">

                                <?php
                                while ( have_posts() ) : the_post();
                                    the_content();
                                endwhile;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

<?php
get_footer();
endif;
