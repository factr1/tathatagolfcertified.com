<?php
/**
 * Template Name: Courses Modules Template
 * This is the template that displays home page with multiple sections.
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */
get_header();
?>

<!-- Slider -->
<div class="sliderWrap">
    <div id="owl-demo" class="owl-carousel owl-theme">
        <div class="item">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fullimage1.jpg" alt="The Last of us">
            <div class="banner-content">
                <div class="container">
                    <h1 class="text-center">Become Your <span>Greatness</span></h1>

                    <div class="banner-content-align">
                        <h2><span>Create </span>the Foundation.<br><span>Fine Tune</span> the Performance. </h2>
                        <p>Begin Your Journey with Tathata’s Incredible 60-Day Program. Continue Your Development Through the Online Academy.</p>
                        <a href="javascript:void(0)" class="darkBtn linkBtn">Launch your training</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="item">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fullimage1.jpg" alt="The Last of us">
            <div class="banner-content">
                <div class="container">
                    <h1 class="text-center">Become Your <span>Greatness</span></h1>

                    <div class="banner-content-align">
                        <h2><span>Create </span>the Foundation.<br><span>Fine Tune</span> the Performance. </h2>
                        <p>Begin Your Journey with Tathata’s Incredible 60-Day Program. Continue Your Development Through the Online Academy.</p>
                        <a href="javascript:void(0)" class="darkBtn linkBtn">Launch your training</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="item">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fullimage1.jpg" alt="The Last of us">
            <div class="banner-content">
                <div class="container">
                    <h1 class="text-center">Become Your <span>Greatness</span></h1>

                    <div class="banner-content-align">
                        <h2><span>Create </span>the Foundation.<br><span>Fine Tune</span> the Performance. </h2>
                        <p>Begin Your Journey with Tathata’s Incredible 60-Day Program. Continue Your Development Through the Online Academy.</p>
                        <a href="javascript:void(0)" class="darkBtn linkBtn">Launch your training</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<article class="chapterlinkContentWrap">
    <div class="container">
        <div class="row">
            <ul class="chapterLinks">
                <?php echo do_shortcode( '[newcourses]' ); ?>
            </ul>

            <div class="clearfix"></div>

            <ol class="breadcrumb pull-left">
                <li><a href="#">Home</a></li>
                <li><a href="#">Training Programs</a></li>
                <li>60 Day Program</li>
                <li><a href="#">Chapter-01</a></li>
            </ol>

            <ul class="shareUs pull-right">
                <li>SHARE US</li>
                <?php
                if ( function_exists( 'ssb_share_icons' ) ) {
                    echo ssb_share_icons();
                } else {
                    echo "s";
                }
                ?>
            </ul>
        </div>
    </div>
</article>

<section class="pageInnerContentWrap">
    <div class="container">
        <div class="row">
            <article class="col-sm-6">
                <p>
                    <?php
                    global $wp;
                    $current_url = home_url( add_query_arg( array(), $wp->request ) );
                    $uriArray    = explode( '/', $current_url );
                    $content_id  = "";
                    if ( isset( $uriArray[4] ) ) {
                        $content_page = $uriArray[4];
                        $content_id = $content_page;
                    }
                    echo do_shortcode( "[coursecontent content_id='$content_id']" );
                    ?>
                </p>
            </article>

            <article class="col-sm-6 demoTrainingWrap">
                <div class="overlayText">
                    <h4 class="pull-left">Get familiar with the<br>60-Day Training Program</h4>
                    <a href="javascript:void(0)" class="redLinkBtn">Demo Training</a>
                </div>
            </article>
        </div>
    </div>
</section>

<section class="profileThumbnailWrap">
    <div class="container">
        <div class="row">
            <article class="col-sm-4">
                <div>
                    <h2 class="quickTips">Quick Tips & Reminders</h2>

                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/profileThumbnailImg.png" alt="">

                    <div class="textContent">
                        <p>At vero eos et accusamus et iusto oddignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>

                        <ul>
                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                        </ul>
                    </div>
                </div>
            </article>

            <article class="col-sm-4">
                <div>
                    <h2 class="accProfile">Account / Profile</h2>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/profileThumbnailImg.png" alt="">
                    <div class="textContent">
                        <p>At vero eos et accusamus et iusto oddignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>
                        <a href="javascript:void(0)" class="redLinkBtn">Go to Profile</a>
                    </div>
                </div>
            </article>

            <article class="col-sm-4">
                <div>
                    <h2 class="suppForum">Support & Forum</h2>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/profileThumbnailImg.png" alt="">
                    <div class="textContent">
                        <p>At vero eos et accusamus et iusto oddignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque</p>

                        <ul>
                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                            <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit</li>
                        </ul>

                        <a href="javascript:void(0)" class="redLinkBtn">Check our Forum</a>
                    </div>
                </div>
            </article>
        </div>
    </div>
</section>

<div class="clearfix"></div>

<?php get_footer(); ?>
