<?php

/**
 * Template Name: Lesson Quiz Template
 *
 * This is the template that displays home page with multiple sections.
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */
get_header();

global $woothemes_sensei, $post, $current_user, $first_module_content, $chapter_name_glob;

$current_url = home_url( add_query_arg( array(), $wp->request ) );
$uriArray    = explode( '/', $current_url );

$post      = wp_get_single_post( $uriArray[4], "OBJECT" );
$post_meta = get_post_meta( $post->ID );
$lesson_id = $post_meta['_quiz_lesson'][0];

do_action( 'sensei_quiz_questions_custom' );

get_footer();
