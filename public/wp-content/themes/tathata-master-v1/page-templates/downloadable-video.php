<?php
/**
 * Template Name: Downloadable Videos
 */

get_header();
?>

    <section id="dvideoPage" class="container">

        <!-- Page Title -->
        <h1><?php echo get_the_title(); ?></h1>

        <!-- Intro Content -->
        <?php if ( get_field( 'dvideo_content' ) ) : ?>
            <section class="dvideo-intro">
                <?php the_field( 'dvideo_content' ); ?>
            </section>
        <?php endif; ?>

        <!-- Video Sections -->
        <?php if ( have_rows( 'dvideo_section' ) ) : while ( have_rows( 'dvideo_section' ) ) : the_row(); ?>
            <section class="dvideo-section">

                <!-- Section Tile -->
                <h3><?php the_sub_field( 'dvideo_title' ); ?></h3>

                <?php if ( have_rows( 'dvideo_videos' ) ) : ?>
                    <nav class="dvideo-videos">
                        <ul>
                            <?php while ( have_rows( 'dvideo_videos' ) ) : the_row(); ?>
                                <li>
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/<?php the_sub_field( 'dvideo_video' ); ?>" width="500" height="281" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                    </div>

                                    <h4><?php the_sub_field( 'dvideo_title' ); ?></h4>
                                    <p><?php the_sub_field( 'dvideo_desc' ); ?></p>

                                    <?php if ( get_sub_field( 'dvideo_hd' ) ) : ?>
                                        <a href="<?php the_sub_field( 'dvideo_hd' ); ?>" download>Download HD Video</a>
                                    <?php endif; ?>

                                    <?php if ( get_sub_field( 'dvideo_sd' ) ) : ?>
                                        <a href="<?php the_sub_field( 'dvideo_sd' ); ?>" download>Download SD Video</a>
                                    <?php endif; ?>
                                </li>
                            <?php endwhile; ?>
                        </ul>
                    </nav>
                <?php endif; ?>
            </section>
        <?php endwhile; endif; ?>
    </section>

<?php
get_footer();
