<?php
/**
 * Template Name: Course Chapters Template
 * This is the template that displays home page with multiple sections.
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */
get_header();
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

global $woothemes_sensei, $post, $current_user, $first_module_content, $chapter_name_glob, $chapters_global, $first_module_id;

$current_url = home_url( add_query_arg( array(), $wp->request ) );
$uriArray    = explode( '/', $current_url );
$termId      = end( $uriArray );
$post        = wp_get_single_post( $termId, "OBJECT" );
$args        = array(
    'post_type'  => 'page',
    'meta_query' => array(
        array(
            'key' => 'quick_tips_and__remainders'
        )
    )
);
$meta_det    = new WP_Query( $args );
?>

<article class="chapterlinkContentWrap">
    <div class="container">
        <div class="row">
            <ul class="chapterLinks">
                <?php echo do_shortcode( '[menu name="Secondary Menu Course Chapter"]' ) ?>
            </ul>
        </div>
    </div>
</article>

<article class="captionWrap">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <?php
                $course_chapter_top_left = get_field( 'course_chapter_top_left', $meta_det->post->ID );
                echo $course_chapter_top_left;
                ?>
            </div>

            <div class="col-sm-6">
                <div class="redBoxContent pull-right">
                    <?php
                    $course_chapter_top_right = get_field( 'course_chapter_top_right', $meta_det->post->ID );
                    echo $course_chapter_top_right;
                    ?>
                </div>
            </div>
        </div>
    </div>
</article>

<div class="container">
    <div class="row">
        <div class="clearfix"></div>

        <ol class="breadcrumb pull-left">
            <li><a href="#">Home</a></li>
            <li><a href="<?php echo get_site_url(); ?>/courses">Training Programs</a></li>
            <li>60 Day Program</li>
            <li><a href="#"><?php echo $chapter_name_glob; ?></a></li>
        </ol>

        <ul class="shareUs pull-right">
            <li>SHARE US</li>
            <li>
                <a href="mailto:?subject=I wanted you to see this site&amp;body=Check out this site /.com." class="icon-mail" onclick="window.location.href='mailto:email@email.co'"><i class="fa fa-envelope"></i></a>
            </li>
            <li><a href="https://twitter.com/TG_certified" target="_blank"><i class="fa fa-twitter"></i></a>
            <li>
            <li>
                <a href="https://www.facebook.com/pages/Tathata-Golf/526700354108550" target="_blank"><i class="fa fa-facebook"></i></a>
            </li>
            <li><a href="https://i.instagram.com/tg_certified" target="_blank"><i class="fa fa-instagram"></i></a></li>
            <li>
                <a href="https://www.linkedin.com/company/tathata-golf-certified" target="_blank"><i class="fa fa-linkedin"></i></a>
            </li>
        </ul>
    </div>
</div>

<div class="clearfix"></div>

<section class="pageInnerContentWrap courseChapter">
    <div class="container">
        <div class="row course-chap">
            <article style="float:left;" class="col-sm-6 chap">
                <div class="welcomeToHeading">
                    <h1>What to Expect</h1>
                </div>

                <p>
                    <?php
                    $con_left  = get_field( 'mem_page_content_left', $meta_det->post->ID );
                    $con_right = get_field( 'mem_page_content_right', $meta_det->post->ID );
                    echo $con_left;
                    ?>
                </p>
            </article>

            <article style="float:right;" class="col-sm-6 chap">
                <?php
                echo $con_right;
                ?>
            </article>
        </div>
    </div>
</section>

<section class="profileThumbnailWrap courseChaptersThumbnail">
    <div class="container">
        <div class="row">
            <article class="col-sm-6 col-md-4">
                <div>
                    <?php
                    $quick_data = get_field( 'quick_tips_and__remainders', $meta_det->post->ID );
                    ?>
                    <h2 class="quickTips">Chapter <span class="red-light">Support</span></h2>
                    <?php echo $quick_data; ?>
                </div>
            </article>

            <article class="col-sm-6 col-md-4">
                <div>
                    <?php
                    $account_or_profile = get_field( 'account_or_profile', $meta_det->post->ID );
                    ?>
                    <h2 class="accProfile">Account & <span class="red-light">Profile</span></h2>
                    <?php echo $account_or_profile; ?>
                </div>
            </article>

            <article class="col-sm-12 col-md-4 col-last">
                <div>
                    <?php
                    $support_and_forum = get_field( 'support_and_forum', $meta_det->post->ID );
                    ?>
                    <h2 class="suppForum">Support &<span class="red-light"> Forum</span></h2>
                    <?php echo $support_and_forum; ?>
                </div>
            </article>
        </div>
    </div>
</section>

<div class="clearfix"></div>

</section>

<?php get_footer(); ?>
