<?php
/**
 * Template Name: Profile Template
 *
 * This is the template that displays home page with multiple sections.
 */

get_header();
?>

    <div id="main-content" class="main-content">
        <div id="primary" class="content-area">

            <div id="content" class="site-content profile-page" role="main">

                <?php
                // Start the Loop.
                while ( have_posts() ) : the_post();

                    // Include the page content template.
                    get_template_part( 'content', 'page' );

                endwhile;
                ?>

            </div><!-- #content -->
        </div><!-- #primary -->
    </div><!-- #main-content -->

<?php
get_footer();
