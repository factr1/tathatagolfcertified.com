<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link       http://codex.wordpress.org/Template_Hierarchy
 *
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */
get_header();
?>

<?php get_template_part( 'parts/page-title' ); ?>
    <div id="blackbar"></div>

    <?php setBreadcrumbs( 'Blog' ); ?>

    <section class="pageInnerContentWrap">
        <div class="container blog-page-container">
            <div class="row">
                <div class="clearfix"></div>

                <article class="col-sm-9">
                    <?php
                    if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
                        // Include the featured content template.
                        get_template_part( 'featured-content' );
                    }
                    ?>

                    <div id="primary" class="content-area">
                        <div id="content" class="site-content" role="main">

                            <?php
                            if (have_posts()) :

                                echo '<h1 class="contentHead"><span class="red">WELCOME TO</span> <span>OUR BLOG</span></h1>';

                                // Start the Loop.
                                while (have_posts()) : the_post();

                                    /*
                                     * Include the post format-specific template for the content. If you want to
                                     * use this in a child theme, then include a file called called content-___.php
                                     * (where ___ is the post format) and that will be used instead.
                                     */
                                    get_template_part('content', get_post_format());

                                endwhile;
                                // Previous/next post navigation.
                                twentyfourteen_paging_nav();

                            else :
                                // If no content, include the "No posts found" template.
                                get_template_part('content', 'none');

                            endif;
                            ?>

                        </div><!-- #content -->
                    </div><!-- #primary -->

                </article>

                <article class="col-sm-3 rightPanel">
                    <?php
                    get_sidebar();
                    get_sidebar( 'content' );
                    ?>
                </article>
            </div>
        </div><!-- #main-content -->
    </section>
<?php
get_footer();
