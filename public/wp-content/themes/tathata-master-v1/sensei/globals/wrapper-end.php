<?php
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
/**
 * Content wrappers
 *
 * All support theme wrappers can be found in includes/theme-integrations
 *
 * @author      Automattic
 * @package     Sensei
 * @category    Templates
 * @version     1.9.0
 */

// CUSTOM - Not used
return;

?>
</div>
<?php get_sidebar(); ?>
</div>
