<?php
/**
 * The Template for displaying all single course meta information.
 *
 * Override this template by copying it to yourtheme/sensei/single-course/course-lessons.php
 *
 * @author      WooThemes
 * @package     Sensei/Templates
 * @version     1.0.0
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

global $post, $woothemes_sensei, $current_user, $cour_id;
$html = '';

$UserMeta          = get_user_meta( $current_user->data->ID );
$lessons_completed = 0;
$mid               = isset( $_GET['mid'] ) ? $_GET['mid'] : '';
//do_action('sensei_course_single_chapter', $mid);
//global $course_module, $course_module_lessons;
//$course_lessons = $course_module_lessons;
//$ChapterID = @trim(str_ireplace('Chapter', '', strstr($course_module->name, ':', true)));
//$total_lessons = count($course_lessons);
$total_lessons = 1;

// Check if the user is taking the course
$is_user_taking_course = $course_ids ? WooThemes_Sensei_Utils::user_started_course( $course_ids[0], $current_user->ID ) : false;

$course_page_id  = intval( $woothemes_sensei->settings->settings['course_page'] );
$course_page_url = ( 0 < $course_page_id ? get_permalink( $course_page_id ) : get_post_type_archive_link( 'course' ) );

// Get User Meta
get_currentuserinfo();

if ( 0 < $total_lessons ) {

    $lessons_completed   = 0;
    $show_lesson_numbers = false;
    $post_classes        = array( 'course', 'post' );
    ?>
    <section class="contentWrapper">

        <?php get_template_part( 'parts/page-title' ); ?>

        <div class="clearfix"></div>

        <div id="blackbar"></div>

        <?php setBreadcrumbs( 'My Profile' ); ?>

        <section class="pageInnerContentWrap profilePage">
            <div class="container">
                <div class="row">
                    <article class="col-xs-12 col-sm-6 col-md-6 <?= tath_role_class( 'learnervisibility' ); ?>">
                        <div class="clearfix"></div>
                        <?php
                        foreach ( $courses as $course ) {
                            $course_modules          = $sen_modules->get_course_modules( $course->ID );
                            $cour_id                 = $course->ID;
                            $total_lessons_count     = 0;
                            $lessons_completed_count = 0;
                            ?>
                            <h1 class="day-program-head-for-private-profile"> <?php echo $course->post_title; ?></h1>

                            <ul class="accordion_example smk_accordion">
                                <?php
                                $chap_id       = $start_lesson_count = 1;
                                $total_lessons = 0;
                                $key           = 1;
                                $next_lesson   = null;
                                foreach ( $course_modules as $course_module ) {
                                    if ( $course_module->parent == 0 ) {
                                        do_action( 'sensei_course_single_chapter', $course_module->term_id );
                                        global $course_module, $course_module_lessons;
                                        $course_lessons = $course_module_lessons;
                                        //$ChapterID = @trim(str_ireplace('Chapter', '', strstr($course_module->name, ':', true)));

                                        $start_lesson_count = $total_lessons + $start_lesson_count;
                                        $total_lessons      = count( $course_lessons );
                                        $total_lessons_count += $total_lessons;

                                        ?>
                                        <!-- Section 1 -->
                                        <li class="acc_active accordion_in">
                                            <div class="welcomeToHeading acc_head">
                                                <div class="acc_icon_expand"></div>
                                                <ul>
                                                    <li>Chapter<span><?php printf( "%02s", $chap_id ); ?></span></li>
                                                    <li>Days<span><?php echo $course_module->lessons_covered; ?></span>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="acc_content">

                                                <ul class="trainingProgressLevel">
                                                    <?php
                                                    $lesson_count = 1;
                                                    foreach ( $course_lessons as $lesson_item ) {
                                                        $single_lesson_complete = false;
                                                        $user_lesson_end        = '';
                                                        if ( is_user_logged_in() ) {
                                                            // Check if Lesson is complete
                                                            $user_lesson_status = WooThemes_Sensei_Utils::user_lesson_status( $lesson_item->ID, $current_user->ID );
                                                            $user_lesson_end    = WooThemes_Sensei_Utils::user_completed_lesson( $user_lesson_status );
                                                            // var_dump($user_lesson_end);
                                                            if ( '' != $user_lesson_end ) {
                                                                //Check for Passed or Completed Setting
                                                                $course_completion = $woothemes_sensei->settings->settings['course_completion'];
                                                                if ( 'passed' == $course_completion ) {
                                                                    $lessons_completed ++;
                                                                    $single_lesson_complete = true;
                                                                    $post_classes[]         = 'lesson-completed';
                                                                }
                                                            }
                                                        }

                                                        if ( ! $single_lesson_complete && is_null( $next_lesson ) ) {
                                                            $next_lesson = $lesson_item->ID;
                                                        }

                                                        // Get Lesson data
                                                        $complexity_array  = $woothemes_sensei->frontend->lesson->lesson_complexities();
                                                        $lesson_length     = get_post_meta( $lesson_item->ID, '_lesson_length', true );
                                                        $lesson_complexity = get_post_meta( $lesson_item->ID, '_lesson_complexity', true );
                                                        if ( '' != $lesson_complexity ) {
                                                            $lesson_complexity = $complexity_array[ $lesson_complexity ];
                                                        }
                                                        $user_info     = get_userdata( absint( $lesson_item->post_author ) );
                                                        $is_preview    = WooThemes_Sensei_Utils::is_preview_lesson( $lesson_item->ID );
                                                        $preview_label = '';
                                                        if ( $is_preview && ! $is_user_taking_course ) {
                                                            $preview_label  = $woothemes_sensei->frontend->sensei_lesson_preview_title_text( $post->ID );
                                                            $preview_label  = '<span class="preview-heading">' . $preview_label . '</span>';
                                                            $post_classes[] = 'lesson-preview';
                                                        }
                                                        ?>

                                                        <?php if ( '' != $user_lesson_end && $single_lesson_complete ) { ?>
                                                            <li class="completed">
                                                            <span class="completed-level-complete completedLevel">
                                        <?php $comp_lesson ++; ?>
                                        <?php print apply_filters( 'sensei_complete_text', __( 'Complete', 'woothemes-sensei' ) ) . '!'; ?>
                                      </span>
                                                        <?php } else { ?>
                                                            <li>
                                                            <span class="test2 completed-level-inProgress completedLevel">
                                        <?php print apply_filters( 'sensei_in_progress_text', __( 'In Progress', 'woothemes-sensei' ) ); ?>
                                      </span>

                                                        <?php } ?>
                                                        <div class="dayCount">
                                                            <a href="<?php print esc_url( get_permalink( $lesson_item->ID ) ); ?>">Day
                                                                <span><?php echo $lesson_item->day; ?></span></a></div>
                                                        <div class="trainingTimeDetail">
                                                            <span>Total Time (<?php echo $lesson_length; ?> Minutes)</span>
                                                            <span style="display:none;">
                                        <?php
                                        if ( '' != $lesson_complexity ) {
                                            print apply_filters( 'sensei_complexity_text', __( 'Complexity: ', 'woothemes-sensei' ) ) . $lesson_complexity;
                                        }
                                        ?>
                                      </span>
                                                        </div>
                                                        </li>
                                                        <?php
                                                        $lesson_count ++;
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    $chap_id ++;
                                }
                                ?></ul>
                            <?php
                        }
                        ?>

                    </article>
                    <article class="col-xs-12 col-sm-6 col-md-6 <?= tath_role_class( 'advisibility' ); ?>">
                        <p>No courses active. <a href="/60-day-program/">Get started</a>.</p>
                    </article>
                    <article class="col-xs-12 col-sm-6 col-md-6 profile_details">
                        <?php
                        $statuses = courses_status( $current_user->ID );
                        foreach ( $courses as $course ) {
                            if ( array_key_exists( $course->ID, $statuses ) ) {
                                echo '<div class="courseInProgress"> <span style="display:none;">' . $course->post_title . '-</span><h5 class="status in-progress"> ' . $statuses[ $course->ID ]['status'] . '</h5> </div>';
                            }
                        }
                        ?>
                        <div class="clearfix"></div>
                        <h3>Profile Details</h3>
                        <article class="profileDetailWrap">
                            <div class="leftPanelDetail">
                                <div class="profileImg"><?php echo get_avatar( $current_user->ID, 230 ); ?></div>
                                <h6>badges earned</h6>
                                <!-- For badges -->
                                <?php do_shortcode( '[badgeos_achievements_list_custom]' ); ?>
                                <!-- For badges - end -->
                                <?php
                                if ( ! empty( $UserMeta['user_thumbnail'][0] ) ) {
                                $user_thumb       = get_post_meta( $UserMeta['user_thumbnail'][0] );
                                $profile_img_path = site_url() . '/wp-content/uploads/' . $user_thumb['_wp_attached_file'][0];
                                ?>
                                <style>
                                    .profilePage .profileDetailWrap .leftPanelDetail .profileImg {
                                        background: url("<?php print $profile_img_path; ?>") no-repeat scroll 10px 10px transparent;
                                    }

                                    <?php } ?>
                                </style>
                            </div>

                            <div class="rightPanelDetail">
                                <h1><span><?php print ucfirst( $current_user->data->user_nicename ); ?></span></h1>
                                <!-- <h5><?php print ucfirst( $current_user->data->user_nicename ); ?></h5> -->

                                <ul class="memberActivityList <?= tath_role_class( 'learnervisibility' ); ?>">
                                    <?php
                                    $order_date_combine        = wcr_check_user_bought( $current_user->user_email, $current_user->ID, 72 );
                                    $order_date_combine_status = 0;
                                    if ( $order_date_combine ) {
                                        $order_date_combine_status = 1;
                                        $order_date                = date( 'd/m/y', strtotime( $order_date_combine ) );
                                        echo '<li class="profSixtyDayPro">Certified Movement Specialist Program Member<span class="startDate">Start Date: ' . $order_date . '</span></li>';
                                        echo '<li class="profOnlineAccademy">Online Academy Member<span class="startDate">Start Date: ' . $order_date . '</span></li>';
                                    }
                                    if ( $order_date_combine_status === 0 ) {
                                        $order_date_60 = wcr_check_user_bought( $current_user->user_email, $current_user->ID, 69 );
                                        if ( $order_date_60 ) {
                                            echo '<li class="profSixtyDayPro">Certified Movement Specialist Program Member<span class="startDate">Start Date: ' . date( 'd/m/y', strtotime( $order_date_60 ) ) . '</span></li>';
                                        } else {
                                            echo '<li class="profSixtyDayPro">Certified Movement Specialist Program Member<span class="startDate">Start Date: Manually Added</span></li>';
                                        }
                                        $order_date_online = wcr_check_user_bought( $current_user->user_email, $current_user->ID, 70 );
                                        if ( $order_date_online ) {
                                            echo '<li class="profOnlineAccademy">Online Academy Member<span class="startDate">Start Date: ' . date( 'd/m/y', strtotime( $order_date_online ) ) . '</span></li>';
                                        } else {
                                            echo '<li class="profOnlineAccademy">Online Academy Member<span class="startDate">Start Date: Manually Added</span></li>';
                                        }
                                    }
                                    ?>
                                </ul>

                                <!--<a href="javascript:void(0)">Update Account Info</a>-->
                                <?php echo do_shortcode( '[usereditlink]' ); ?>

                                <div class="clearfix"></div>
                                <!--<a href="javascript:void(0)" class="userId">TG000978122223-02</a>-->
                                <address>
                                    <span>Address</span>
                                    <?php
                                    if ( ! empty( $UserMeta['address'][0] ) ) {
                                        print $UserMeta['address'][0];
                                    }
                                    ?>
                                    <span>Phone </span>
                                    <?php
                                    if ( ! empty( $UserMeta['phone'][0] ) ) {
                                        print $UserMeta['phone'][0];
                                    }
                                    ?>
                                    <span>eMail </span>
                                    <?php print $current_user->data->user_email; ?>
                                </address>
                            </div>

                        </article>
                        <?php //echo do_shortcode('[usercourses]');   ?>

                        <div class="<?= tath_role_class( 'learnervisibility' ); ?>">
                <?php
                  $html = '';

                  if ( is_user_logged_in() && $is_user_taking_course ) {

                    $html .= '<span class="course-completion-rate">' . sprintf( __( 'Currently completed %1$s of %2$s in total', 'woothemes-sensei' ), '######', $total_lessons ) . '</span>';
                    $html .= '<div class="meter+++++"><span style="width: @@@@@%">@@@@@%</span></div>';

                    // Add dynamic data to the output
                    $html = str_replace( '######', $comp_lesson, $html );

                    $progress_percentage = abs( round( ( doubleval( $comp_lesson ) * 100 ) / ( $total_lessons_count ), 0 ) );
                    if ( 0 == $progress_percentage ) {
                      $progress_percentage = 0;
                    }
                    $html = str_replace( '@@@@@', $progress_percentage, $html );
                    if ( 50 < $progress_percentage ) {
                      $class = ' green';
                    } elseif ( 25 <= $progress_percentage && 50 >= $progress_percentage ) {
                      $class = ' orange';
                    } else {
                      $class = ' red';
                    }

                    $html = str_replace( '+++++', $class, $html );
                    $html = '';
                    //                            $html = '<span class="course-completion-rate">PROGRESS OF CHAPTER ' . $chapter_num . "</span>";
                  } // End If Statement
                  $html .= "<div class='barfiller-container'><div id = 'bar4' class = 'barfiller'>
                    <div class = 'tipWrap'>
                    <span class = 'tip'></span>
                    </div>
                    <span class = 'fill' data-percentage = '$progress_percentage'></span>
                    </div> <div style='display:none;'>$progress_percentage%</div>
            </div>";
                  echo $html;
                ?>
              </div>

                        <div class="<?= tath_role_class( 'learnervisibility' ); ?>">
                            <div class="training-actions">
                                <a href="<?php echo get_permalink( $course->ID ); ?>" class="training-actions--button"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/60DayWhiteIcon.png" alt=""></a>
                                <a href="<?php echo esc_url( get_permalink( $next_lesson ) ); ?>" class="training-actions--button">Continue Training</a>
                            </div>
                        </div>

                    </article>
                    <div class="col-xs-12 col-sm-6 col-md-6 myAccountBox">
              <?php echo do_shortcode( '[woocommerce_my_account]' ); ?>
            </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
    </section>
<?php } ?>
