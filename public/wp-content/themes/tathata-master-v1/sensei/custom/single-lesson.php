<?php
/**
 * single-lesson.php template actions
 */

remove_action( 'sensei_pagination', array( 'Sensei_Lesson', 'output_comments' ), 90 );

remove_all_actions( 'sensei_course_single_lessons' );
remove_all_actions( 'sensei_lesson_single_meta' );

remove_action( 'sensei_single_lesson_content_inside_before', array( 'Sensei_Lesson', 'the_title' ), 15 );
remove_action( 'sensei_single_lesson_content_inside_after', array( 'Sensei_Lesson', 'footer_quiz_call_to_action' ) );

add_filter( 'sensei_breadcrumb_output', function() {
    return '';
} );

/**
 *  Hook inside the single lesson above the content
 *
 * @priority 1
 */
add_action( 'sensei_single_lesson_content_inside_before', function( $post_id ) {
    $post = get_post( $post_id );

    $course_id = get_post_meta( $post_id, '_lesson_course', true );
    $module    = ( $modules = wp_get_post_terms( $post_id, 'module' ) ) ? $modules[0] : null;
    $title     = $module ? $module->name : '';

    ?>
    <?php setPageBanner( get_the_title( $post_id ) ); ?>

    <div class="clearfix"></div>

    <div id="blackbar"></div>

    <!-- Breadcrumb Area -->
    <section id="breadcrumbs">
        <div class="hidden-xs">
            <div class="container">
                <div class="row">

                    <div class="col-xs-12 col-sm-8">
                        <div class="flex -flex-start">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/course/certified-movement-specialist-program/' ) ); ?>">Home</a>
                                </li>
                                <li>
                                    <a href="<?php echo get_permalink( $course_id ); ?>"><?php echo get_the_title( $course_id ); ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo get_term_link( $module->term_id, 'module' ) . '?course_id=' . $course_id; ?>"><?php echo $module->name; ?></a>
                                </li>
                                <li><?php echo $post->post_title; ?></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                        <div class="flex -flex-end">
                            <?php echo do_shortcode( '[followUs]' ); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="visible-xs">
            <div class="container">
                <div class="flex">

                    <div>
                        <ul class="breadcrumb">
                            <li>
                                <a href="<?php echo esc_url( home_url( '/course/certified-movement-specialist-program/' ) ); ?>">Home</a>
                            </li>
                            <li>
                                <a href="<?php echo get_permalink( $course_id ); ?>"><?php echo get_the_title( $course_id ); ?></a>
                            </li>
                            <li>
                                <a href="<?php echo get_term_link( $module->term_id, 'module' ) . '?course_id=' . $course_id; ?>"><?php echo $module->name; ?></a>
                            </li>
                            <li><?php echo $post->post_title; ?></li>
                        </ul>
                    </div>

                    <div>
                        <?php echo do_shortcode( '[followUs]' ); ?>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section>
    <div class="container">
    <?php
}, 1 );

/**
 *   Hook the single lesson content
 *
 * @priority default 10
 */
add_action( 'sensei_single_lesson_content_inside', function( $post_id ) {

    // User has not completed previous lesson, bail on content
    if ( ! custom_sensei_has_user_completed_prerequisite_lesson( $post_id, get_current_user_id() ) ) {
        return;
    }

    $course_id = get_post_meta( $post_id, '_lesson_course', true );
    $module    = ( $modules = wp_get_post_terms( $post_id, 'module' ) ) ? $modules[0] : null;

    $post               = get_post( $post_id );
    $lesson_video_embed = get_post_meta( $post_id, '_lesson_video_embed', true );

    $pdfs = ( $meta = get_post_meta( $post_id, '_attached_media', true ) ) ? $meta : [];
    $pdfs = array_filter( $pdfs, function( $media ) {
        return preg_match( "#\.pdf$#ismu", $media );
    } );

    $undervids = get_field( "undervids", $post_id );

    $badges = ( function_exists( 'badgeos_get_user_achievements' ) ) ? badgeos_get_user_achievements( [
        'user_id'          => get_current_user_id(),
        'site_id'          => get_current_blog_id(),
        'achievement_id'   => false,
        'achievement_type' => false,
        'since'            => 0,
    ] ) : [];

    $content = apply_filters( 'the_content', $post->post_content );

    $chapterpdfurl = get_field( 'studyguidepdf', 'module_' . $module->term_id );

    $quiz_id = Sensei()->lesson->lesson_quizzes( $post_id );

    $prev_url = get_term_link( $module->term_id, 'module' ) . "?course_id=" . $course_id;

    $module->lessons = ( class_exists( 'Sensei_Custom' ) ) ? Sensei_Custom::getLessons( $course_id, $module->term_id ) : [];

    $current_lesson = array_pop( array_filter( $module->lessons, function( $lesson ) use ( $post ) {
        return $lesson->ID === $post->ID;
    } ) );

    ?>
    <div class="row">
        <?php if ( sensei_can_user_view_lesson() ) { ?>
            <article class="col-xs-12 col-sm-7">
                <div id="video-container" class="embed-responsive embed-responsive-16by9">
                    <?php
                    if ( strlen( $lesson_video_embed ) < 1 ) {
                        echo "There is no video found for this lesson";
                    } else {
                        echo apply_filters( 'the_content', "[embed height=\"350\"]" . $lesson_video_embed . "[/embed]" );
                    }
                    ?>
                </div>
            </article>

            <article class="col-xs-12 col-sm-5">
                <div class="lessonDay">
                    <h2>DAY <span><?php echo $current_lesson->day; ?></span></h2>
                </div>

                <div class="clearfix"></div>

                <article id="video-details" class="trainingDetailTextWrap">
                    <?php echo $content; ?>
                </article>

                <div id="forum-chap" class="forum-chap">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/suppForumBg.png">
                    <a href="<?php echo site_url() . "/forums/"; ?>">Forum</a>
                </div>

                <div style="margin-bottom: 10px;" class="clearfix"></div>

                <?php do_action( 'sensei_lesson_quiz_meta_custom', $post_id, get_current_user_id() ); ?>
            </article>

            <?php
        } else {
            if ( $lesson_prerequisite > 0 ) {
                echo sprintf( __( '<div class="alert">You must first complete %1$s before viewing this Lesson</div>', 'woothemes-sensei' ), '<a href="' . esc_url( get_permalink( $lesson_prerequisite ) ) . '" title="' . esc_attr( sprintf( __( 'You must first complete: %1$s', 'woothemes-sensei' ), get_the_title( $lesson_prerequisite ) ) ) . '">' . get_the_title( $lesson_prerequisite ) . '</a>' );
            } else {
                echo '<div class="alert">You do not have access to this resource.</div>';
            }
        }
        ?>
    </div>

    <div style="margin-bottom: 30px"></div>

    <div class="row">
        <div class="col-xs-12 col-sm-7">
            <?php if ( $undervids ) : ?>
                <div class="under-vid-conts ">
                    <?php the_field( "undervids" ); ?>
                </div>
            <?php endif; ?>

            <!-- Lesson Media -->
            <div id="lesson-badges" class="lesson-badges">
                <?php if ( $pdfs ) { ?>
                    <h6>Lesson Media</h6>

                    <div class="clearfix"></div>

                    <ul class="studyGuideList">
                        <?php foreach ( $pdfs as $pdf ) { ?>
                            <li>
                                <a href="<?php echo $pdf; ?>">Day <?php echo $current_lesson->day; ?> <?php echo get_the_title( $post->ID ); ?> Media <?php echo $key + 1; ?>
                                    <span>Download</span></a>
                            </li>
                        <?php } ?>
                    </ul>
                <?php } ?>

                <ul class="otherProfileImgs">
                    <?php
                    $count = 1;
                    foreach ( $badges as $value ) {
                        if ( $value->post_type == "badges" && $i <= 6 ) {
                            $attachment_id = get_post_thumbnail_id( $value->ID );
                            ?>
                            <li><?php echo wp_get_attachment_image( $attachment_id, array( 100, 100 ), false ); ?></li>
                            <?php
                            $i ++;
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 col-sm-5">
            <div id="training-actions" class="training-actions">
                <a href="javascript:void(0);" class="training-actions--button" onclick="history.go(-1);">Back</a>
                <a id="lesson-quiz" href="<?php echo get_permalink( $quiz_id ); ?>" class="training-actions--button">Lesson Test</a>

                <?php if ( $prev_url ) { ?>
                    <a href="<?php echo $prev_url; ?>" class="training-actions--link">Back to Chapter</a>
                <?php } ?>

                <?php if ( $next_url ) { ?>
                    <a href="<?php echo $next_url; ?>" class="training-actions--link">Next Day</a>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php
    if ( $current_lesson->day == '30.2' || $current_lesson->day == '60.2' ) {
        get_template_part( 'parts/follow-up' );
    }
} );

/**
 *   Hook inside the single lesson template after the content
 *
 * @priority 98
 */
add_action( 'sensei_single_lesson_content_inside_after', function( $post_id ) {
    ?>
    </div>
    </section>

    <?php
    // User has not completed previous lession, bail on content
    if ( ! custom_sensei_has_user_completed_prerequisite_lesson( $post_id, get_current_user_id() ) ) {
        return;
    }

    /* Not being used, uncomment if needed to load any promos below
    $course_id = get_post_meta( $post_id, '_lesson_course', true );
    $module = ($modules=wp_get_post_terms($post_id,'module')) ? $modules[0] : null;
    $post = get_post($post_id);

    $lessons = (class_exists('Sensei_Custom')) ? Sensei_Custom::getLessons($course_id,$module->term_id) : [];

    $lesson_key = array_search( $post_id, array_map(function($post) {
    return $post->ID;
    },$lessons) );

    $day = $lesson_key + 1;
    $day = (strlen($day) > 1) ? (string) $day : "0" . (string) $day;
    $day = (($chapter_num - 1) * 10) + $day;
    $day_next = $day + 1;
    */

    // Add items/promos here if completed prerequisite

}, 98 );

/**
 *   Hook inside the single lesson template after the content
 *
 * @priority 99
 */
add_action( 'sensei_single_lesson_content_inside_after', function( $post_id ) {

    $course_id = get_post_meta( $post_id, '_lesson_course', true );
    $module    = ( $modules = wp_get_post_terms( $post_id, 'module' ) ) ? $modules[0] : null;
    $post      = get_post( $post_id );

    $chapter_name     = $module->name;
    $chap_name_arr    = explode( ":", $chapter_name );
    $chapter_name_arr = explode( " ", $chap_name_arr[0] );
    $chapter          = $chapter_name_arr[0];
    $chapter_num      = ( strlen( $chapter_name_arr[1] ) < 2 ) ? "0" . $chapter_name_arr[1] : $chapter_name_arr[1];

    $module->lessons = ( class_exists( 'Sensei_Custom' ) ) ? Sensei_Custom::getLessons( $course_id, $module->term_id ) : [];

    $first_lesson = ( $module->lessons ) ? $module->lessons[0] : null;
    $last_lesson  = ( $module->lessons > 1 ) ? end( $module->lessons ) : null;

    $module->lessons_covered = ( ( $first_lesson ) ? $first_lesson->day : '' ) . ( ( $last_lesson ) ? '-' . $last_lesson->day : '' );

    $current_lesson = array_pop( array_filter( $module->lessons, function( $lesson ) use ( $post ) {
        return $lesson->ID === $post->ID;
    } ) );

    include_once __DIR__ . '/chapter-tabs.php';
}, 99 );
