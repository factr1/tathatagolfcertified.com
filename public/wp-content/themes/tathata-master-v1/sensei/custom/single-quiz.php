<?php
/**
*  single-quiz.php template actions
*/

remove_action( 'sensei_quiz_back_link', array( Sensei_Share_Your_Grade(), 'output_sharing_message' ), 5 );
remove_action( 'sensei_quiz_back_link', array( Sensei_Share_Your_Grade(), 'output_sharing_buttons' ), 5 );

add_action('sensei_single_quiz_questions_after',function() {
    ?>
    <script type="text/javascript">
      jQuery( document ).ready( function() {
        jQuery( '.quiz-submit.reset' ).hide();
        jQuery( '#reset' ).click( function() {
          jQuery( '.quiz-submit.reset' ).click();
        } )
      } )
    </script>
    <?php
},11,0);

/**
*   Check for prerequisite and add notice
*/
add_action('sensei_single_quiz_content_inside_before',function($post_id) {

    $quiz_lesson = get_post_meta( $post_id, '_quiz_lesson', true);

    $lesson_prerequisite =  WooThemes_Sensei_Lesson::get_lesson_prerequisite_id( $quiz_lesson );
    $lesson_has_pre_requisite = $lesson_prerequisite > 0;
    if ( ! WooThemes_Sensei_Lesson::is_prerequisite_complete(  $quiz_lesson, get_current_user_id() ) && $lesson_has_pre_requisite ) {

        $prerequisite_lesson_link  = '<a href="' . esc_url( get_permalink( $lesson_prerequisite ) ) . '" title="' . esc_attr(  sprintf( __( 'You must first complete: %1$s', 'woothemes-sensei' ), get_the_title( $lesson_prerequisite ) ) ) . '">' . get_the_title( $lesson_prerequisite ). '</a>';

        add_filter('sensei_user_quiz_status_not_started',function($message) use($prerequisite_lesson_link) {
            return sprintf( __( 'You must first complete %1$s before completing this quiz', 'woothemes-sensei' ), $prerequisite_lesson_link );
        },10,4);

    }

},1,1);

add_action('sensei_single_quiz_content_inside_before',function($post_id) {

    $post = get_post($post_id);

    $module = ($modules=wp_get_post_terms($post_id,'module')) ? $modules[0] : null;
    $user_id = get_current_user_id();

    $chapter_name     = $module->name;
    $chap_name_arr    = explode( ":", $chapter_name );
    $chapter_name_arr = explode( " ", $chap_name_arr[ 0 ] );
    $chapter          = $chapter_name_arr[ 0 ];
    $chapter_num      = (strlen( $chapter_name_arr[ 1 ] ) < 2) ? "0" . $chapter_name_arr[ 1 ] : $chapter_name_arr[ 1 ];

    $quiz_lesson = get_post_meta( $post_id, '_quiz_lesson', true);
    $lesson_alert = get_post_meta( $quiz_lesson, 'lesson_alert', true );

    $status = WooThemes_Sensei_Utils::sensei_user_quiz_status_message( $quiz_lesson, $user_id );

    $lesson_course_id = absint( get_post_meta( $quiz_lesson, '_lesson_course', true ) );

    $course_page_id = intval( $woothemes_sensei->settings->settings[ 'course_page' ] );
    $course_page_url = (0 < $course_page_id) ? get_permalink( $course_page_id ) : get_post_type_archive_link( 'course' );

    ?>
    <?php setPageBanner($post->post_title . ' Quiz'); ?>

    <div id="blackbar"></div>

    <section id="breadcrumbs">
      <div class="hidden-xs">
        <div class="container">
          <div class="row">

            <div class="col-xs-12 col-sm-8">
              <div class="flex -flex-start">
                <ul class="breadcrumb">
                  <li><a href="<?php echo esc_url(home_url('/course/certified-movement-specialist-program/')); ?>">Home</a></li>
                  <li><a href="<?php echo get_permalink($lesson_course_id); ?>"><?php echo get_the_title($lesson_course_id); ?></a></li>
                  <li><a href="<?php echo get_term_link($module->term_id, 'module') . '?course_id=' . $lesson_course_id; ?>"><?php echo $module->name; ?></a></li>
                  <li><?php echo $post->post_title; ?> Quiz</li>
                </ul>
              </div>
            </div>

            <div class="col-xs-12 col-sm-4">
              <div class="flex -flex-end">
                <?php echo do_shortcode( '[followUs]' ); ?>
              </div>
            </div>

          </div>
        </div>
      </div>

      <div class="visible-xs">
        <div class="container">
          <div class="flex">
            <div>
              <ul class="breadcrumb">
                <li><a href="<?php echo esc_url(home_url('/course/certified-movement-specialist-program/')); ?>">Home</a></li>
                <li><a href="<?php echo get_permalink($lesson_course_id); ?>"><?php echo get_the_title($lesson_course_id); ?></a></li>
                <li><a href="<?php echo get_term_link($module->term_id, 'module') . '?course_id=' . $lesson_course_id; ?>"><?php echo $module->name; ?></a></li>
                <li><?php echo $post->post_title; ?> Quiz</li>
              </ul>
            </div>

            <div>
              <?php echo do_shortcode( '[followUs]' ); ?>
            </div>
          </div>
        </div>
      </div>
    </section>

  <section>
    <div class="container">
      <?php
        });

        add_action('sensei_single_quiz_questions_before',function($post_id) {

        $post = get_post($post_id);

        $module = ($modules=wp_get_post_terms($post_id,'module')) ? $modules[0] : null;
        $quiz_lesson = get_post_meta( $post_id, '_quiz_lesson', true);
        $course_id = get_post_meta( $quiz_lesson, '_lesson_course', true );
        $module->lessons = (class_exists('Sensei_Custom')) ? Sensei_Custom::getLessons($course_id,$module->term_id) : [];

        $current_lesson = array_pop( array_filter( $module->lessons, function( $lesson ) use ( $quiz_lesson ) {
          return intval($lesson->ID) === intval($quiz_lesson);
        } ) );
      ?>
      <div class="row">
        <article class="col-xs-12 col-sm-7 questionPageWrap lessonQuizzWrap">
          <div class="welcomeToHeading">
            <ul>
              <li>Day<span><?php echo $current_lesson->day; ?></span></li>
              <li>quiz</li>
            </ul>
            <div class="clearfix"></div>
          </div>

          <div class="lessonQuizQuestionsWrap">
            <?php
              });

              add_action('sensei_single_quiz_questions_list',function($post_id) {

                $module = ($modules=wp_get_post_terms($post_id,'module')) ? $modules[0] : null;
                $user_id = get_current_user_id();

                $quiz_lesson = get_post_meta( $post_id, '_quiz_lesson', true);
                $status = WooThemes_Sensei_Utils::sensei_user_quiz_status_message( $quiz_lesson, $user_id );

                // Lesson Quiz Meta
                $n_value = isset($_GET[ 'n' ]) ? $_GET[ 'n' ] : 1;

                global $woothemes_sensei, $sensei_question_loop;
                $lesson_quiz_questions = $woothemes_sensei->quiz->data->lesson_quiz_questions;

                if ( count( $lesson_quiz_questions ) <= 0 ) {
                  return;
                }

                $question_count = 1;
                $odd            = [];
                $even           = [];
                $kk             = 1;

                foreach ( $lesson_quiz_questions as $k => $v ) {
                  $terms = get_the_terms( $v->ID, 'question-category' );
                  $v->question_cat = ($terms) ? $terms[ key( $terms ) ]->name : null;
                  if ( $kk % 2 == 0 ) {
                      $even[ $kk ] = $v;
                  } else {
                      $odd[ $kk ] = $v;
                  }
                  $kk++;
                }
            ?>
        <ol id="sensei-quiz-list-custom">
        <?php

        foreach ( $odd as $k => $question_item ) {
          // Setup current Frontend Question
          $woothemes_sensei->quiz->data->question_item = $question_item;
          $woothemes_sensei->quiz->data->question_count = $question_count;

          /**
          *   Set the current loop item according to sensei_the_question_content
          *
          *   @since 1.9.0
          */
          $sensei_question_loop['current_question'] = $question_item;
          sensei_the_question_content();

          $question_count++;
        } // End For Loop

        foreach ( $even as $k => $question_item ) {

            // Setup current Frontend Question
            $woothemes_sensei->quiz->data->question_item = $question_item;
            $woothemes_sensei->quiz->data->question_count = $question_count;

            /**
            *   Set the current loop item according to sensei_the_question_content
            *
            *   @since 1.9.0
            */
            $sensei_question_loop['current_question'] = $question_item;
            sensei_the_question_content();

            $question_count++;
        } // End For Loop

        ?>
    </ol>
    <?php
});

add_action('sensei_single_quiz_questions_after',function($post_id) {

    $reset_quiz_allowed = get_post_meta( $post_id, '_enable_quiz_reset', true );

    $quiz_lesson = get_post_meta( $post_id, '_quiz_lesson', true);
    $nav_id_array = sensei_get_prev_next_lessons( $quiz_lesson );
    $next_lesson_id = absint( $nav_id_array[ 'next_lesson' ] );

    $module = ($modules=wp_get_post_terms($post_id,'module')) ? $modules[0] : null;
    $lesson_course_id = absint( get_post_meta( $quiz_lesson, '_lesson_course', true ) );

    ?>
              <div class="quiz-controls">
                <?php do_action( 'sensei_quiz_action_buttons' ); ?>
              </div>
            </div>
          </article>

          <article class="col-xs-12 col-sm-5">
            <h1 class="lessonQuiz">Grade: <span>Credit 01</span></h1>

            <div class="training-actions">
              <?php if ( isset($reset_quiz_allowed) && $reset_quiz_allowed ) : ?>
              <a href="javascript:void(0)" id="reset" class="training-actions--button">Reset Quiz</a>
              <?php endif; ?>

              <?php if ( 0 < $next_lesson_id ) : ?>
              <a href="<?php echo esc_url( get_permalink( $next_lesson_id ) ); ?>" class="training-actions--button"><?php echo get_the_title( $next_lesson_id ); ?></a>
              <?php endif; ?>

              <a href="<?php echo get_permalink( $quiz_lesson ); ?>" class="training-actions--link">Back</a>
              <a href="<?php echo get_term_link( $module->term_id, 'module' ) . '?course_id=' . $lesson_course_id; ?>" class="training-actions--link">Back to Chapter</a>
            </div>

            <p class="quiz-fail-note">Note: If you fail please reset quiz to take again.</p>
          </article>
    <?php
});

add_action('sensei_single_quiz_content_inside_after',function($post_id) {
    ?>
    </div>
  </section>
<?php
});
