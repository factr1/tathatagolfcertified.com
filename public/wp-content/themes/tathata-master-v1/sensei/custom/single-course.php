<?php

remove_all_actions( 'sensei_single_course_content_inside_before' );
remove_all_actions( 'sensei_single_course_content_inside_after' );

add_action( 'sensei_single_course_content_inside_before', function( $post_id ) {

    $post = get_post( $post_id );

    $thumbnail_url = has_post_thumbnail( $post ) ? wp_get_attachment_image_src( get_post_thumbnail_id( $post_id ), 'tgc-hero' )[0] : null;

    ?>

    <?php if ( $thumbnail_url ) { ?>

        <section class="tgc-hero" style="background: url('<?php echo $url; ?>') center center/cover no-repeat"></section>

    <?php } else { ?>

        <section class="tgc-hero">
            <div class="tgc-hero--logo"></div>
            <div class="tgc-hero--content">
                <h1>Welcome to the <span>Certified Movement Specialist Program</span></h1>
                <p>This is your place for creating <span>new</span> and <span>incredible</span> outcomes at
                    <span>your own</span> training center.</p>
            </div>
        </section>

    <?php } ?>

    <article class="chapterlinkContentWrap">
        <div class="container">
            <div class="row">
                <ul class="chapterLinks">
                    <?php do_action( 'sensei_course_single_lessons_custom' ); ?>
                </ul>
            </div>
        </div>
    </article>

    <article class="captionWrap">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <?php if ( get_field( 'course_chapter_top_left' ) ) {
                        echo the_field( 'course_chapter_top_left' );
                    } ?>
                </div>

                <div class="col-xs-12 col-sm-6 col-md-6">
                    <div class="redBoxContent pull-right">
                        <?php if ( get_field( 'course_chapter_top_right' ) ) {
                            echo the_field( 'course_chapter_top_right' );
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </article>

    <!-- Breadcrumb Area -->
    <section id="breadcrumbs">
        <div class="hidden-xs">
            <div class="container">
                <div class="row">

                    <div class="col-xs-12 col-sm-8">
                        <div class="flex -flex-start">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/course/certified-movement-specialist-program/' ) ); ?>">Home</a>
                                </li>
                                <li>
                                    <a href="<?php echo get_permalink( $post_id ); ?>"><?php echo get_the_title( $post_id ); ?></a>
                                </li>
                                <li><?php echo get_the_title(); ?></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-4">
                        <div class="flex -flex-end">
                            <?php echo do_shortcode( '[followUs]' ); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="visible-xs">
            <div class="container">
                <div class="flex">
                    <div>
                        <ul class="breadcrumb">
                            <li>
                                <a href="<?php echo esc_url( home_url( '/course/certified-movement-specialist-program/' ) ); ?>">Home</a>
                            </li>
                            <li>
                                <a href="<?php echo get_permalink( $post_id ); ?>"><?php echo get_the_title( $post_id ); ?></a>
                            </li>
                            <li><?php echo get_the_title(); ?></li>
                        </ul>
                    </div>
                    <div>
                        <?php echo do_shortcode( '[followUs]' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pageInnerContentWrap courseChapter marginBottom">
        <div class="container">
            <?php
        } );
        add_action( 'sensei_single_course_content_inside', function( $post_id ) {
            ?>
            <div class="row">
                <article class="col-xs-12 col-sm-6">
                    <div class="welcomeToHeading">
                        <h1>Welcome</h1>
                    </div>

                    <p>
                        <?php if ( get_field( 'mem_page_content_left' ) ) {
                            echo the_field( 'mem_page_content_left' );
                        } ?>
                    </p>
                </article>

                <article class="col-xs-12 col-sm-6">
                    <?php if ( get_field( 'mem_page_content_right' ) ) {
                        echo the_field( 'mem_page_content_right' );
                    } ?>

                    <ul class="chaptersList">
                        <li>
                            <a href="<?php echo esc_url( home_url( '/modules/chapter-1/?course_id=224' ) ); ?>">Chapter 1 | Days 1 - 10.4 | Body, Stretching &amp; Mind 1</a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url( home_url( '/modules/chapter-2/?course_id=224' ) ); ?>">Chapter 2 | Days 11 - 20.1 | Hands, Arms &amp; Mind 2 Training</a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url( home_url( '/modules/chapter-3/?course_id=224' ) ); ?>">Chapter 3 | Days 21 - 30.2 | Pressure, Impact &amp; Mind 3 Training</a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url( home_url( '/modules/chapter-4/?course_id=224' ) ); ?>">Chapter 4 | Days 31 - 40.1 | Speed, Strength &amp; Mind 4 Training</a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url( home_url( '/modules/chapter-5/?course_id=224' ) ); ?>">Chapter 5 | Days 41 - 50.1 | Short Game, Putting &amp; Mind 5 Training</a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url( home_url( '/modules/chapter-6/?course_id=224' ) ); ?>">Chapter 6 | Days 51 - 60.3 | Shape, Trajectory &amp; Mind 6 Training</a>
                        </li>
                        <li>
                            <a href="<?php echo esc_url( home_url( '/modules/chapter-7/?course_id=224' ) ); ?>">Chapter 7 | Days 61 - 67 | Career Support</a>
                        </li>
                    </ul>
                </article>
            </div>
            <?php
        } );

        add_action( 'sensei_single_course_content_inside_after', function() {

            ?>
            </div>
    </section>

    <section class="profileThumbnailWrap courseChaptersThumbnail">
        <div class="container">
            <div class="row">
                <article class="col-sm-6">
                    <div>
                        <h2 class="accProfile">Account / Profile</h2>

                        <?php if ( get_field( 'account_or_profile' ) ) {
                            echo the_field( 'account_or_profile' );
                        } ?>
                    </div>
                </article>

                <article class="col-sm-6">
                    <div>
                        <h2 class="suppForum">Support & Forum</h2>

                        <?php if ( get_field( 'support_and_forum' ) ) {
                            echo the_field( 'support_and_forum' );
                        } ?>
                    </div>
                </article>
            </div>
        </div>
    </section>
    <?php

} );
