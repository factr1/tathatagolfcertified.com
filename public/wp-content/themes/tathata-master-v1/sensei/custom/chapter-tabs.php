<?php
/**
 * Custom Tathata Addon
 */

set_time_limit( 60 );

if ( is_array( $terms ) ) {
    $terms = $terms[0];
}

if ( ! isset( $post ) || ! $post ) {
    $post = get_queried_object();
}

// $lesson_media = ($post->post_type === 'lesson') ? get_field( 'lesson_media', $post->ID ) : '';
?>

<section id="senseiChapterTabs" class="tabSectionWrap">
    <div class="container">
        <div class="row">
            <div id="parentHorizontalTab">

                <!-- Chapter Tab Titles -->
                <ul class="resp-tabs-list hor_1">
                    <li>Chapter Overview</li>
                    <li<?php echo( ( isset( $post->taxonomy ) && $post->taxonomy == 'module' ) ? ' class="default"' : '' ); ?>><?php echo $module->name; ?> | Days
                        <span><?php echo $module->lessons_covered; ?></span> Lessons
                    </li>
                    <?php if ( get_field( 'lesson_media' ) && !is_archive() ) : ?>
                        <li class="lesson-video-tab">Daily Extras</li>
                    <?php endif; ?>
                </ul>

                <div class="resp-tabs-container hor_1">

                    <!-- Chapter Overview Tab Content -->
                    <div class="chapterOverViewTab welcomeToHeading">
                        <div class="chap-overview-head">
                            <div class="overview-title">
                                <p><?php echo $chapter; ?> <span><?php echo $chapter_num; ?></span></p>
                                <p>Days <span><?php echo $module->lessons_covered; ?></span></p>
                            </div>
                        </div>

                        <div class="chap-overview">
                            <div class="overview-content">
                                <h4>Overview</h4>

                                <div class="row">
                                    <?php echo get_field( 'chapter_overview_tab', "module_" . $module->term_id ); ?>
                                </div>

                            </div>
                        </div>
                    </div>

                    <!-- Video Lessons Tab Content -->
                    <div class="dayLessonsTab">
                        <div class="lessonsContainer">
                            <?php foreach ( $module->lessons as $value ) { ?>
                                <div class="columnContent">
                                    <h4>
                                        <a href="<?php echo get_permalink( $value->ID ); ?>">Day
                                            <span><?php echo $value->day; ?></span></a>
                                    </h4>

                                    <a href="<?php echo get_permalink( $value->ID ); ?>" class="play_button_hover">
                                        <?php
                                        if ( 0 < intval( $value->ID ) ) {
                                            $video_thumbnail = get_video_thumbnail( $value->ID );
                                            $thumbnail       = ( empty( $video_thumbnail ) || $video_thumbnail instanceof WP_Error ) ? get_template_directory_uri() . "/assets/img/no-videos.jpeg" : $video_thumbnail;
                                            $show            = ( empty( $video_thumbnail ) || $video_thumbnail instanceof WP_Error ) ? "hide" : "show";
                                            ?>

                                            <div class="videoContainer" style="background-image: url('<?php echo $thumbnail; ?>');">
                                                <?php if ( $show == "show" ) : ?>
                                                    <i class="fa fa-play"></i>
                                                <?php endif; ?>
                                            </div>

                                            <?php
                                        }
                                        $video_field_arr = get_post_custom_values( 'video_field', $value->ID );
                                        if ( ! empty( $video_field_arr ) ) {
                                            echo $video_field_arr[0];
                                        } else {
                                            ?>
                                            <ul class="post-det"></ul>
                                        <?php } ?>
                                    </a>

                                </div>
                            <?php } ?>
                        </div>
                    </div>

                    <?php if ( get_field( 'lesson_media' ) ) : ?>
                        <div class="lessonMediaTab">
                            <div class="lessonMedia-container">
                                <?php if ( have_rows( 'lesson_media' ) ) : while ( have_rows( 'lesson_media' ) ) : the_row(); ?>
                                    <div class="lessonMedia-video">
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/<?php the_sub_field( 'de_video' ); ?>?title=0&byline=0&portrait=0" width="500" height="210" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                        </div>

                                        <?php if ( get_sub_field( 'de_title' ) ) : ?>
                                            <h3><?php the_sub_field( 'de_title' ); ?></h3>
                                        <?php endif; ?>
                                    </div>
                                <?php endwhile; endif; ?>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
</section>


