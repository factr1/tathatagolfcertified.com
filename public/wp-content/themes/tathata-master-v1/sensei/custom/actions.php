<?php

// Single Lessons
include __DIR__ . '/single-lesson.php';

// Single Chapter - Lesson Archive
include __DIR__ . '/archive-lesson.php';

// Single Course
include __DIR__ . '/single-course.php';

// Single Quiz
include __DIR__ . '/single-quiz.php';
