<?php
/**
 * Template part that displays a Money Back Guarantee banner
 */
?>

<section id="mb-guarantee" class="money-back">
    <div class="container">
        <div class="row">
            <div class="col-xs-3">
                <div class="mb-content" style="background: url(<?php echo get_template_directory_uri(); ?>/assets/img/MoneyBack.png) center center / contain no-repeat"></div>
            </div>

            <div class="col-xs-9">
                <div class="mb-content">
                    <p>If this program isn't the most transformational Golf Training you have ever received, e-mail us within 30 days of your purchase for a full
                        <span>100%</span> refund!</p>
                </div>
            </div>
        </div>
    </div>
</section>
