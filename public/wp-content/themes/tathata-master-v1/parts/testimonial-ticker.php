<?php
/**
 * Template part that displays a ticker of selected customer testimonials
 */

$args         = array(
    'post_type'      => 'testimonial',
    'tax_query'      => array(
        array(
            'taxonomy' => 'testimonial_category',
            'field'    => 'slug',
            'terms'    => 'ticker'
        )
    ),
    'posts_per_page' => 50,
    'orderby'        => 'rand',
    'order'          => DESC
);
$testimonials = new WP_Query( $args );
?>


<?php if ( $testimonials->have_posts() ) : ?>
    <div class="tt-ticker">
        <div class="container">
            <div class="col-md-7">
                <div class="tt-content">
                    <div id="testimonial-ticker">
                        <?php while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
                            <?php
                            $ttContent = get_field( 'tt_testimonial' );

                            // Social Source Settings
                            $source = get_field( 'social_source' );

                            if ( $source == 'facebook' ):
                                $social_icon  = 'fa-facebook';
                                $social_color = '#3b5998';
                            elseif ( $source == 'twitter' ):
                                $social_icon  = 'fa-twitter';
                                $social_color = '#55acee';
                            elseif ( $source == 'web' ):
                                $social_icon  = 'fa-globe';
                                $social_color = '#fd9306';
                            else:
                                $social_icon  = 'fa-envelope';
                                $social_color = '#CF0A2C';
                            endif;
                            ?>
                            <blockquote style="border-left: 5px solid <?php echo $social_color; ?>">
                                <?php echo $ttContent; ?>
                                <footer>
                                    <cite title="A testimonial from <?php the_title(); ?>" style="color: <?php echo $social_color; ?>">
                                        <?php the_title(); ?> |
                                        <i class="fa <?php echo $social_icon; ?>" style="color: <?php echo $social_color; ?>; font-size: 14px"></i>
                                    </cite>
                                </footer>
                            </blockquote>
                        <?php endwhile;
                        wp_reset_postdata(); ?>
                    </div>
                </div>

                <a href="<?php echo esc_url( home_url() ); ?>/tathata-golf-testimonials" class="btn-new">Submit Yours / See All</a>
                <!-- <button class="tg-btn" disabled="true">Submit Yours / See All</button> -->
            </div>

            <div class="col-md-5">
                <div class="tt-content">
                    <p id="tickerHours"></p>
                    <p>Hours of Learning &amp; Training by Tathata Students since September 1, 2015</p>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
