<?php
/**
 * Template part that highlights a company and their partnership with Tathata Golf
 */
?>


<?php if ( get_field( 'affiliate_logo' ) ) : ?>
    <section id="corporatePartner">
        <div class="container">
            <div class="cpLogos">
                <div class="cpLogos-img">
                    <img src="<?php the_field( 'affiliate_logo' ); ?>" alt="A proud Tathata Golf corporate partner">
                </div>

                <span>Partners With</span>

                <div class="cpLogos-img">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tgLogo800.png" alt="Logo for Tathata Golf">
                </div>
            </div>

            <p id="cpLogos-content">&amp; The <span>"Grow The Game Through Better Golf"</span> Initiative</p>
        </div>
    </section>
<?php endif; ?>
