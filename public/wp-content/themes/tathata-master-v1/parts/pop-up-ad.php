<?php
/**
 * Template part that displays a pop up advertisement in the bottom right of the screen
 */
?>

<div id="pop-up"><!-- Pop Up Ad -->
    <div class="pop-up-close">
        Close <i class="fa fa-times"></i>
    </div>

    <a href="<?php echo esc_url( home_url() ); ?>/certified/">
        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/CMS-Popup.jpg" alt="Become a Certified Tathata Movement Specialist Now!">
    </a>
</div>
