<section id="hero-slider">

    <div class="hero-slider">
        <!-- Slides -->
        <?php while ( have_rows( 'slider_slides' ) ) : the_row(); ?>
            <div class="hero-slide" style="background: url('<?php the_sub_field( 'slide_background_image' ); ?>') center center/cover no-repeat <?php the_sub_field( 'slide_background_color' ); ?>">
                <div class="container">
                    <div class="flex">
                        <?php if ( get_sub_field( 'slide_image' ) ) : ?>
                            <div class="slide-image">
                                <img class="img-responsive" src="<?php the_sub_field( 'slide_image' ); ?>">
                            </div>
                        <?php endif; ?>

                        <div class="slide-content">
                            <h1 class="slide-title"><?php the_sub_field( 'slide_title' ); ?></h1>
                            <?php if ( get_sub_field( 'slide_content' ) ) : ?>
                                <p><?php the_sub_field( 'slide_content' ); ?></p>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    </div>

</section>
