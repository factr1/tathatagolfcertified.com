<?php
$heroBackground = '';
$heroTitle      = get_the_title();

switch ( get_field( 'hero_background_source' ) ) :
    case 'featured':
        $heroBackground = getFeaturedImage();
        break;
    case 'upload':
        $heroBackground = get_field( 'hero_background_image' );
        break;
endswitch;

if ( get_field( 'hero_title' ) ) {
    $heroTitle = get_field( 'hero_title' );
}
?>

<section id="page-hero" class="hero" style="background: url('<?php echo $heroBackground; ?>') center center/cover no-repeat <?php the_field( 'hero_background_color' ); ?>">
    <div class="container">
        <div class="flex">

            <?php if ( get_field( 'hero_image' ) ) : ?>
                <div class="hero-image">
                    <img class="img-responsive" src="<?php the_field( 'hero_image' ); ?>">
                </div>
            <?php endif; ?>

            <div class="hero-content">
                <h1 class="page-title"><?php echo $heroTitle; ?></h1>
                <?php if ( get_field( 'hero_content' ) ) : ?>
                    <p><?php the_field( 'hero_content' ); ?></p>
                <?php endif; ?>
            </div>

        </div>
    </div>
</section>

