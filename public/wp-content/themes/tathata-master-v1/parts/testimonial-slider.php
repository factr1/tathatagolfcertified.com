<?php
/**
 * New testimonial slider
 *
 * - the current slider appears to be a full custom build
 * - why? Just use a plugin! - Slick Slider (it is already on this site)
 * - will need to update/create an ACF to pull data from
 */

// Grab the testimonials
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

$args = array(
    'post_type'      => 'testimonial',
    'tax_query'      => array(
        array(
            'taxonomy' => 'testimonial_category',
            'field'    => 'slug',
            'terms'    => 'slider'
        )
    ),
    'posts_per_page' => 50,
    'paged'          => $paged
);

$testimonials = new WP_Query( $args );
?>

<!-- Desktop Slider -->
<section id="testimonial-slickslider--desktop" class="hidden-xs">
    <div class="container">
        <p class="ts-title">Read what others are saying about Tathata!</p>

        <div class="testimonial-slides">
            <?php if ( $testimonials->have_posts() ) : while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
                <div>
                    <div class="testimonial-slide">
                        <div class="ts-info">
                            <?php if ( get_field( 'tss_image' ) ) : ?>
                                <img class="img-circle" src="<?php the_field( 'tss_image' ); ?>" alt="Another happy user of the Tathata Golf system!">
                            <?php else : ?>
                                <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar_male.png" alt="Another happy user of the Tathata Golf system!">
                            <?php endif; ?>
                            <p class="ts-source"><?php the_field( 'tss_source' ); ?></p>
                            <p class="ts-source--meta"><?php the_field( 'tss_source_meta' ); ?></p>
                        </div>

                        <div class="ts-content">
                            <blockquote>
                                "<?php the_field( 'tss_content' ); ?>"
                            </blockquote>
                        </div>
                    </div>
                </div>
            <?php endwhile; endif;
            wp_reset_postdata(); ?>
        </div>
    </div>
</section>

<!-- Mobile Slider -->
<section id="testimonial-slickslider--mobile" class="visible-xs">
    <div class="container">
        <p class="ts-title">Read what others are saying about Tathata!</p>

        <div class="testimonial-slides">
            <?php if ( $testimonials->have_posts() ) : while ( $testimonials->have_posts() ) : $testimonials->the_post(); ?>
                <div>
                    <div class="testimonial-slide">
                        <div class="ts-info">
                            <?php if ( get_field( 'tss_image' ) ) : ?>
                                <img class="img-circle" src="<?php the_field( 'tss_image' ); ?>" alt="Another happy user of the Tathata Golf system!">
                            <?php else : ?>
                                <img class="img-circle" src="<?php echo get_template_directory_uri(); ?>/assets/img/avatar_male.png" alt="Another happy user of the Tathata Golf system!">
                            <?php endif; ?>
                            <p class="ts-source"><?php the_field( 'tss_source' ); ?></p>
                            <p class="ts-source--meta"><?php the_field( 'tss_source_meta' ); ?></p>
                        </div>

                        <div class="ts-content">
                            <blockquote>
                                "<?php the_field( 'tss_content' ); ?>"
                            </blockquote>
                        </div>
                    </div>
                </div>
            <?php endwhile; endif;
            wp_reset_postdata(); ?>
        </div>
    </div>
</section>
