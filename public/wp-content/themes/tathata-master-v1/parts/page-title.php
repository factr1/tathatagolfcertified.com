<?php
/**
 * Template part for custom page banners/titles
 *
 * This is almost the same as the banner template part, but this should be used on archive/category pages so that a
 * custom page title can be set.
 */

$pageTitle = get_the_title();
$customPageTitle;

if ( $customPageTitle ) {
    $pageTitle = $customPageTitle;
}
?>

<section id="title-section">
    <div class="container">
        <h1><?php echo $pageTitle; ?></h1>
    </div>
</section>
