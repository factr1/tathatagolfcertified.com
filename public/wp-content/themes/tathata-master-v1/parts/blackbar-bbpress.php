<?php
/**
 * Template part to display a custom Black Bar area for bbPress pages.
 *
 * This black bar has links for the user in relation to bbPress and forums.
 */
?>

<div id="blackbar">
    <div class="container">
        <div class="flex-row">
            <a href="<?php global $current_user;
            get_currentuserinfo();
            echo esc_url( home_url() ) . "/forums/members/" . $current_user->user_login . "/"; ?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/profile-icon-gray.png" class="course-lessons-head-icon"></a>
            <a href="<?php echo esc_url( home_url() ); ?>/dashboard/#"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/60-day-member-icon.png" class="course-lessons-head-icon"></a>
            <a href="<?php echo esc_url( home_url() ); ?>/movement-specialists/"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/movement-specialist-icon.png" class="course-lessons-head-icon"></a>
        </div>
    </div>
</div>
