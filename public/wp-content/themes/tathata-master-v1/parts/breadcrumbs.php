<?php
/**
 * Template part for displaying the breadcrumbs and follow us section
 *
 * This template part can not be used on archive or category pages as the page title will not be pulled.
 * Instead, use the setBreadcrumbs($title) function, where $title is the title that you want displayed for the page
 */

?>

<!-- Breadcrumb Area -->
<section id="breadcrumbs">
    <div class="hidden-xs">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="flex -flex-start">
                        <ul class="breadcrumb">
                            <li>
                                <a href="<?php echo esc_url( home_url( '/course/certified-movement-specialist-program/' ) ); ?>">Home</a>
                            </li>

                            <?php if ( ! empty( $post->post_parent ) ) {
                                $parentTitle = get_the_title( $post->post_parent ); ?>
                                <li>
                                    <a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo $parentTitle; ?></a>
                                </li>
                            <?php } ?>

                            <?php if ( $title ) : ?>
                                <li><?php echo $title; ?></li>
                            <?php else : ?>
                                <li><?php echo $post->post_title; ?></li>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="flex -flex-end">
                        <?php echo do_shortcode( '[followUs]' ); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="visible-xs">
        <div class="container">
            <div class="flex">
                <div>
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo esc_url( home_url( '/course/certified-movement-specialist-program/' ) ); ?>">Home</a>
                        </li>

                        <?php if ( ! empty( $post->post_parent ) ) {
                            $parentTitle = get_the_title( $post->post_parent ); ?>
                            <li>
                                <a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo $parentTitle; ?></a>
                            </li>
                        <?php } ?>

                        <?php if ( $title ) : ?>
                            <li><?php echo $title; ?></li>
                        <?php else : ?>
                            <li><?php echo $post->post_title; ?></li>
                        <?php endif; ?>
                    </ul>
                </div>

                <div>
                    <?php echo do_shortcode( '[followUs]' ); ?>
                </div>
            </div>
        </div>
    </div>
</section>
