<?php
$bannerBackground = '';
$bannerTitle      = get_the_title();

switch ( get_field( 'banner_background_source' ) ) :
    case 'featured':
        $bannerBackground = getFeaturedImage();
        break;
    case 'upload':
        $bannerBackground = get_field( 'banner_background_image' );
        break;
endswitch;

if ( get_field( 'banner_title' ) ) {
    $bannerTitle = get_field( 'banner_title' );
} elseif ( is_category() ) {
    $bannerTitle = 'Category - ' . printf( __( '%s', 'twentyfourteen' ), single_cat_title( '', false ) );
} elseif ( is_author() ) {
    $bannerTitle = 'Posts by ' . printf( __( '%s', 'twentyfourteen' ), get_the_author() );
} elseif ( is_search() ) {
    $bannerTitle = 'Search Results';
}
?>

<section id="page-banner" class="banner" style="background: linear-gradient(rgba(0, 0, 0, 0.10), rgba(0, 0, 0, 0.60)), url('<?php echo $bannerBackground; ?>') center center/cover no-repeat <?php the_field( 'banner_background_color' ); ?>">
    <div class="container">
        <h1 class="page-title"><?php echo $bannerTitle; ?></h1>
    </div>
</section>
