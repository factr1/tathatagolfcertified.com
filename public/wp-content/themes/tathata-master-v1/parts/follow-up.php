<div id="lesson-follow-up" class="remodal" data-remodal-id="lesson-follow-up" data-remodal-options="hashTracking: false">
    <style scoped>
        h1 {
            font-size: 36px;
        }

        p,
        .remodal-cancel {
            font-size: 18px;
        }

        .remodal-cancel {
            width: 50%;
            margin: 0 auto;
        }

        .remodal-cancel {
            background: transparent;
            color: rgb(28, 155, 28);
        }

        .remodal-cancel:hover {
            background: transparent;
        }
    </style>
    <button data-remodal-action="close" class="remodal-close"></button>

    <div class="row">
        <h1>It's Time To Hear From you!</h1>
        <p>Congratulations on making it this far in the program! Check your email as you should have just gotten instructions on getting in touch with Tathata to schedule your Follow-Up Call.</p>
        <button data-remodal-action="cancel" class="remodal-cancel">Got It!</button>
    </div>
</div>

<script defer>
    'use strict';
    jQuery(document).ready(function($) {
        var inst = $('[data-remodal-id=lesson-follow-up]').remodal();

        inst.open();

        $(document).on('closed', '#lesson-follow-up', function() {
            inst.destroy();
        });
    });
</script>
