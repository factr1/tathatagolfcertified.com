<div class="remodal" data-remodal-id="30-day-promo">
    <style scoped>
        img {
            margin: 0 auto;
            float: none !important;
        }

        h1 {
            font-size: 30px;
        }

        .price {
            font-size: 24px;
            padding: 10px 0;
        }

        .remodal-confirm,
        .remodal-cancel {
            min-width: 49%;
        }

        .remodal-confirm {
            background: #399734;
        }

        .remodal-confirm:hover {
            color: #fff;
        }

        .remodal-cancel {
            background: transparent;
            color: #600;
        }

        .remodal-cancel:hover {
            background: transparent;
        }

        @media screen and (min-width: 992px) {
            .remodal-cancel {
                min-width: 20%;
            }
        }

        .promocontent--btns {
            text-align: left;
        }

        @media screen and (min-width: 768px) {
            h1, p {
                text-align: left;
            }

            h1 {
                margin: 0 0 20px;
            }
        }
    </style>
    <button data-remodal-action="close" class="remodal-close"></button>

    <div class="row">
        <div class="col-sm-4">
            <img src="<?php echo get_template_directory_uri(); ?>/images/memberdvds.png" class="img-responsive" alt="Tathata Golf 30-Day DVD Bundle">
        </div>

        <div class="col-sm-8 promocontent">
            <h1>30-Disc DVD Set Available!</h1>
            <p>As a purchaser of 60-Day Training Program, you are eligible to purchase the 30-Disc hardback of DVDs. These DVDs are great to have for offline training. Please note, the DVDs contain only the 60 lessons of the program, no chapter support or daily extras.</p>
            <p class="price">$89.95</p>

            <div class="promocontent--btns">
                <a href="<?php echo esc_url( 'http://www.tathatagolf.com/?add_to_cart=42489' ); ?>" class="remodal-confirm">Add to Cart</a>
                <button data-remodal-action="cancel" class="remodal-cancel">No Thanks</button>
            </div>
        </div>
    </div>
</div>

<script defer>
    jQuery(document).ready(function($) {
        var inst = $('[data-remodal-id=30-day-promo]').remodal();
        // Open the modal
        inst.open();
    });
</script>
