<?php
/**
 * Template part that displays the icons of Tathata sponsors
 */
?>

<section id="affiliations">
    <h3>Support &amp; Affiliation</h3>

    <?php if ( have_rows( 'tathata_supporters', 'option' ) ) : ?>
        <ul class="tg-affiliations">
            <?php while ( have_rows( 'tathata_supporters', 'option' ) ) : the_row(); ?>
                <li>
                    <img src="<?php the_sub_field( 'ts_logo', 'option' ); ?>" alt="A proud Tathata Golf supporter">
                </li>
            <?php endwhile; ?>
        </ul>
    <?php endif; ?>
</section>
