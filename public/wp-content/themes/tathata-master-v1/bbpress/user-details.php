<?php

/**
 * User Details
 *
 * @package    bbPress
 * @subpackage Theme
 */

?>

<?php
// This page has to do a lot of work. First, if we're on the edit view, we don't want to show our custom display fields, so build a switch:
if ( bbp_is_single_user_edit() ) {
    $istathprofile = 'true';
}

// check what type of user this is
$tathuserid = bbp_get_displayed_user_id();
$key        = 'SPECIALIST';
$specialist = get_cimyFieldValue( $tathuserid, $key );

// ...and check who our current user is; if it's the same profile, we're going to give them some additional controls
$currentuser = get_current_user_id();
if ( $tathuserid == $currentuser ) {
    $IsMyProfile = 'YES';
}

?>

<?php do_action( 'bbp_template_before_user_details' ); ?>


<?php
// If this profile is a specialist, show our fancy specialist profile
?>
<?php if ( $specialist == 'YES' ) { ?>
    <div id="bbp-single-user-details">
        <div id="bbp-user-avatar">

            <div class='vcard'>
                <a class="url fn n" href="<?php bbp_user_profile_url(); ?>" title="<?php bbp_displayed_user_field( 'display_name' ); ?>" rel="me">
                    <?php echo get_avatar( bbp_get_displayed_user_field( 'user_email', 'raw' ), apply_filters( 'bbp_single_user_details_avatar_size', 150 ) ); ?>
                </a>
                <h2 class="entry-title"><?php bbp_displayed_user_field( 'display_name' ); ?></h2>

                <p>Member Title/Rank</p>
                <p>City, State, Zip</p>
                <p>Certified since: </p>
                <div class="ratings-box">
                    <p>Member ratings</p>
                </div>
            </div>
            <div class="associations">
                <p><strong>Memberships and Associations</strong></p>
                <p></p>
                <p>Associated facilities:</p>
            </div>
            <div class="clear"></div>
        </div><!-- #author-avatar -->
    </div><!-- #bbp-single-user-details -->

    <?php
    // Otherwise, show our learner profile
    ?>
<?php } else if ( ! $istathprofile ) { ?>
    <div class="learner-profile">
        <h3><?php bbp_displayed_user_field( 'display_name' ); ?></h3>

        <div class="learner-profile-right">
            <ul class="memberActivityList">
                <?php
                $order_date_combine        = wcr_check_user_bought( $current_user->user_email, $current_user->ID, 72 );
                $order_date_combine_status = 0;
                if ( $order_date_combine ) {
                    $order_date_combine_status = 1;
                    $order_date                = date( 'd/m/y', strtotime( $order_date_combine ) );
                    echo '<li class="profSixtyDayPro">60 Day Program Member<span class="startDate">Start Date: ' . $order_date . '</span></li>';
                    echo '<li class="profOnlineAccademy">Online Academy Member<span class="startDate">Start Date: ' . $order_date . '</span></li>';
                }
                if ( $order_date_combine_status === 0 ) {
                    $order_date_60 = wcr_check_user_bought( $current_user->user_email, $current_user->ID, 69 );
                    if ( $order_date_60 ) {
                        echo '<li class="profSixtyDayPro">Certified Movement Specialist Program Member<span class="startDate">Start Date: ' . date( 'd/m/y', strtotime( $order_date_60 ) ) . '</span></li>';
                    } else {
                        echo '<li class="profSixtyDayPro">Certified Movement Specialist Program Member<span class="startDate">Start Date: Manually Added</span></li>';
                    }
                    $order_date_online = wcr_check_user_bought( $current_user->user_email, $current_user->ID, 70 );
                    if ( $order_date_online ) {
                        echo '<li class="profOnlineAccademy">Online Academy Member<span class="startDate">Start Date: ' . date( 'd/m/y', strtotime( $order_date_online ) ) . '</span></li>';
                    } else {
                        echo '<li class="profOnlineAccademy">Online Academy Member<span class="startDate">Start Date: Manually Added</span></li>';
                    }
                }
                ?>
            </ul>
        </div>
        <div class="clear"></div>

        <div class="learner-profile-left">
            <div class="learner-image">
                <?php echo get_avatar( bbp_get_displayed_user_field( 'user_email', 'raw' ), apply_filters( 'bbp_single_user_details_avatar_size', 150 ) ); ?>
            </div>
        </div>

        <div class="learner-profile-left">
            <h6>badges earned</h6>
            <!-- For badges -->
            <?php do_shortcode( '[badgeos_achievements_list_custom]' ); ?>
            <!-- For badges - end -->
        </div>
    </div>
<?php } ?>

<?php do_action( 'bbp_template_after_user_details' ); ?>
