<?php

/**
 * User Profile
 *
 * @package    bbPress
 * @subpackage Theme
 */

/* We're modifying the BBPress user profile for our public profiles */
// check what type of user this is
$tathuserid = bbp_get_displayed_user_id();
$key        = 'SPECIALIST';
$specialist = get_cimyFieldValue( $tathuserid, $key );

?>

<?php do_action( 'bbp_template_before_user_profile' ); ?>







<?php if ( $specialist == 'YES' ) { ?>

    <div id="bbp-user-profile" class="bbp-user-profile">
        <div class="bbp-user-section">

            <?php if ( bbp_get_displayed_user_field( 'description' ) ) : ?>

                <p class="bbp-user-description"><?php bbp_displayed_user_field( 'description' ); ?></p>

            <?php endif; ?>
        </div>
        <div id="bbp-user-navigation">
            <ul>
                <li class="<?php if ( bbp_is_single_user_profile() ) : ?>current<?php endif; ?>">
					<span class="vcard bbp-user-profile-link">
						<a class="url fn n" href="<?php bbp_user_profile_url(); ?>" title="<?php printf( esc_attr__( "%s's Profile", 'bbpress' ), bbp_get_displayed_user_field( 'display_name' ) ); ?>" rel="me"><?php _e( 'Profile', 'bbpress' ); ?></a>
					</span>
                </li>

                <li class="<?php if ( bbp_is_single_user_topics() ) : ?>current<?php endif; ?>">
					<span class='bbp-user-topics-created-link'>
						<a href="<?php bbp_user_topics_created_url(); ?>" title="<?php printf( esc_attr__( "%s's Topics Started", 'bbpress' ), bbp_get_displayed_user_field( 'display_name' ) ); ?>"><?php _e( 'Topics Started', 'bbpress' ); ?></a>
					</span>
                </li>

                <li class="<?php if ( bbp_is_single_user_replies() ) : ?>current<?php endif; ?>">
					<span class='bbp-user-replies-created-link'>
						<a href="<?php bbp_user_replies_created_url(); ?>" title="<?php printf( esc_attr__( "%s's Replies Created", 'bbpress' ), bbp_get_displayed_user_field( 'display_name' ) ); ?>"><?php _e( 'Replies Created', 'bbpress' ); ?></a>
					</span>
                </li>

                <?php if ( bbp_is_favorites_active() ) : ?>
                    <li class="<?php if ( bbp_is_favorites() ) : ?>current<?php endif; ?>">
						<span class="bbp-user-favorites-link">
							<a href="<?php bbp_favorites_permalink(); ?>" title="<?php printf( esc_attr__( "%s's Favorites", 'bbpress' ), bbp_get_displayed_user_field( 'display_name' ) ); ?>"><?php _e( 'Favorites', 'bbpress' ); ?></a>
						</span>
                    </li>
                <?php endif; ?>

                <?php if ( bbp_is_user_home() || current_user_can( 'edit_users' ) ) : ?>

                    <?php if ( bbp_is_subscriptions_active() ) : ?>
                        <li class="<?php if ( bbp_is_subscriptions() ) : ?>current<?php endif; ?>">
							<span class="bbp-user-subscriptions-link">
								<a href="<?php bbp_subscriptions_permalink(); ?>" title="<?php printf( esc_attr__( "%s's Subscriptions", 'bbpress' ), bbp_get_displayed_user_field( 'display_name' ) ); ?>"><?php _e( 'Subscriptions', 'bbpress' ); ?></a>
							</span>
                        </li>
                    <?php endif; ?>

                <?php endif; ?>

            </ul>
        </div><!-- #bbp-user-navigation -->
    </div>
<?php } else { ?>
    <div class="non-msu">
        <h3>About</h3>
        <?php if ( bbp_get_displayed_user_field( 'description' ) ) : ?>

            <p class="bbp-user-description"><?php bbp_displayed_user_field( 'description' ); ?></p>

        <?php endif; ?>
    </div>

<?php } ?>
<?php do_action( 'bbp_template_after_user_profile' ); ?>
