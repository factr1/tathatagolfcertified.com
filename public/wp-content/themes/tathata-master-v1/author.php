<?php
/**
 * The template for displaying Author archive pages
 *
 * @link       http://codex.wordpress.org/Template_Hierarchy
 *
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */

get_header(); ?>

<?php get_template_part( 'parts/page-title' ); ?>

    <section class="pageInnerContentWrap">
        <div class="container">
            <div class="row">
                <ol class="breadcrumb pull-left">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Posts by <?php printf( __( '%s', 'twentyfourteen' ), get_the_author() ); ?></a></li>
                </ol>

                <ul class="shareUs pull-right">
                    <li>SHARE US</li>
                    <?php
                    if ( function_exists( 'ssb_share_icons' ) ) {
                        echo ssb_share_icons();
                    } else {
                        echo "s";
                    }
                    ?>
                </ul>

                <div class="clearfix"></div>

                <article class="col-sm-9">
                    <?php
                    if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
                        // Include the featured content template.
                        get_template_part( 'featured-content' );
                    }
                    ?>

                    <div id="primary" class="content-area">
                        <div id="content" class="site-content" role="main">

                            <?php
                            if ( have_posts() ) :

                                // Start the Loop.
                                while ( have_posts() ) : the_post();

                                    /*
									 * Include the post format-specific template for the content. If you want to
									 * use this in a child theme, then include a file called called content-___.php
									 * (where ___ is the post format) and that will be used instead.
									 */
                                    get_template_part( 'content', get_post_format() );

                                endwhile;
                                // Previous/next post navigation.
                                twentyfourteen_paging_nav();

                            else :
                                // If no content, include the "No posts found" template.
                                get_template_part( 'content', 'none' );

                            endif;
                            ?>

                        </div><!-- #content -->
                    </div><!-- #primary -->

                </article>

                <article class="col-sm-3 rightPanel">
                    <?php
                    get_sidebar();
                    get_sidebar( 'content' );
                    ?>
                </article>
            </div>
        </div><!-- #main-content -->
    </section>
<?php
get_footer();
