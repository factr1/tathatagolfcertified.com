'use strict';

const LOCALURL = 'tgcertified:8888'; // change this to whatever your local install URL is
const PROJECT = 'tathata-certified'; // leave this alone
const PATHS = new function() {
    this.root = './',
    this.assets = `${this.root}assets`,
    this.css = `${this.assets}/css`,
    this.js = `${this.assets}/js`,
    this.fonts = `${this.assets}/fonts`,
    this.img = `${this.assets}/img`,
    this.videos = `${this.assets}/videos`;
};
const SOURCES = {
    php: [
        `${PATHS.root}*.php`,
        `${PATHS.root}**/*.php`
    ],
    html: [
        `${PATHS.root}*.html`,
        `${PATHS.root}**/*.html`
    ],
    css: [
        `${PATHS.css}/*.css`,
        `!${PATHS.css}/*.min.css`
    ],
    sass: [
        `${PATHS.assets}/scss/**/*.scss`
    ],
    js: [
        `${PATHS.js}/custom/custom-scripts.js`
    ],
    img: [
        `${PATHS.img}/**/*.{jpg,png,gif,svg}`
    ],
    concatjs: [
        `${PATHS.js}/vendor/bootstrap.js`,
        `${PATHS.js}/base/functions.js`,
        `${PATHS.js}/vendor/jquery-ui.js`,
        `${PATHS.js}/vendor/ui-fix.js`,
        `${PATHS.js}/base/ie-emulation-modes-warning.js`,
        `${PATHS.js}/base/ie10-viewport-bug-workaround.js`,
        `${PATHS.js}/plugins/slinky.js`,
        `${PATHS.js}/plugins/elastic.js`,
        `${PATHS.js}/plugins/matchHeight.js`,
        `${PATHS.js}/plugins/remodal.js`,
        `${PATHS.js}/plugins/easyResponsiveTabs.js`,
        `${PATHS.js}/plugins/nice_select.js`,
        `${PATHS.js}/plugins/jquery.quick.pagination.js`,
        `${PATHS.js}/plugins/jquery.metisMenu.js`,
        `${PATHS.js}/plugins/jquery.barfiller.js`,
        `${PATHS.js}/plugins/owl.carousel.js`,
        `${PATHS.js}/plugins/jquery.waypoints.js`,
        `${PATHS.js}/plugins/slick.js`,
        `${PATHS.js}/custom/custom-scripts.js`
    ]
};
const OPTIONS = {
    sass: {
        outputStyle: 'compact'
    },
    autoprefixer: {
        browsers: [
            '> 1%',
            'Android 2.3',
            'Android >= 4',
            'Chrome >= 20',
            'Firefox >= 24',
            'Explorer >= 8',
            'iOS >= 6',
            'Opera >= 12',
            'Safari >= 6'
        ]
    },
    browsersync: {
        proxy: LOCALURL,
        ghostMode: {
            clicks: true,
            forms: true,
            scroll: false
        },
        browser: [
            'google chrome',
            'safari'
        ],
        reloadOnRestart: true,
        injectChanges: true
    },
    cssnano: {
        autoprefixer: false,
        calc: {
            mediaQueries: true
        },
        colormin: false,
        convertValues: {
            precision: 0
        },
        discardComments: {
            removeAll: true
        },
        discardUnused: false,
        mergeIdents: false,
        reduceIdents: false,
        svgo: {
            encode: true
        },
        zindex: false
    },
    imagemin: {
        interlaced: true,
        progressive: true
    },
    loadplugins: {
        lazy: true
    },
    stylelint: {
        reporters: [{
            formatter: 'string',
            console: true
        }]
    }
};

// Include gulp and plugins
import gulp from 'gulp';
import pngquant from 'imagemin-pngquant';
import del from 'del';
import postscss from 'postcss-scss';
import reporter from 'postcss-reporter';

const BROWSERSYNC = require('browser-sync').create();
const $ = require('gulp-load-plugins')(OPTIONS.loadplugins);

/* -------------------------------------------------------------------------------------------------
  Test Tasks
------------------------------------------------------------------------------------------------- */
gulp.task('test', () => {
    console.log(SOURCES.php);
});

/* -------------------------------------------------------------------------------------------------
  Utility Tasks
------------------------------------------------------------------------------------------------- */
// Delete the contents of the CSS folder, except any prior minified files
gulp.task('css:clean', () => {
    del([`${PATHS.css}/${PROJECT}.css`, `${PATHS.css}/${PROJECT}.css.map`]);
});

// Delete the generated project JS file
gulp.task('js:clean', () => {
    del([`${PATHS.js}/${PROJECT}.*.*`, `${PATHS.js}/${PROJECT}.*`]);
});

/* -------------------------------------------------------------------------------------------------
  Server Tasks
------------------------------------------------------------------------------------------------- */
// Launch a development server
gulp.task('server', () => {
    if (!BROWSERSYNC.active) {
        BROWSERSYNC.init(OPTIONS.browsersync);
    }
});

/* -------------------------------------------------------------------------------------------------
  Sass & CSS Tasks
------------------------------------------------------------------------------------------------- */
// Lint Sass/CSS
gulp.task('sass:lint', () => {
    return gulp
    .src(SOURCES.sass)
    .pipe($.plumber())
    .pipe($.stylelint(OPTIONS.stylelint));
});

// Compile Sass
gulp.task('sass', ['css:clean'], () => {
    return gulp
    .src(SOURCES.sass)
    .pipe($.sourcemaps.init())
    .pipe($.plumber())
    .pipe($.sass(OPTIONS.sass)
        .on('error', $.sass.logError))
    .on('error', $.notify.onError('Error compiling Sass!'))
    .pipe($.autoprefixer(OPTIONS.autoprefixer))
    .pipe($.rename({
        basename: PROJECT,
        extname: '.css'
    }))
    .pipe($.sourcemaps.write('/'))
    .pipe($.plumber.stop())
    .pipe(gulp.dest(PATHS.css))
    .pipe(BROWSERSYNC.stream());
});

// Minify CSS
// gulp.task('css:minify', () => {
//     return gulp
//     .src(`${PATHS.css}/${PROJECT}.css`)
//     .pipe($.plumber())
//     .pipe($.cssnano(OPTIONS.cssnano))
//     .pipe($.rename({
//         basename: PROJECT,
//         suffix: '.min',
//         extname: '.css'
//     }))
//     .pipe(gulp.dest(PATHS.css))
//     .pipe(BROWSERSYNC.stream());
// });

/* -------------------------------------------------------------------------------------------------
  JavaScript Tasks
------------------------------------------------------------------------------------------------- */
// Transform a single file
gulp.task('babel', () => {
    return gulp
    .src(`${PATHS.root}gulpfile.babel.js`)
    .pipe($.plumber())
    .pipe($.print())
    .pipe($.babel())
    .pipe($.plumber.stop())
    .pipe(gulp.dest('./gulp'))
    .pipe($.rename({
        basename: 'gulpfile',
        suffix: '',
        extname: '.js'
    }));
});

// Lint JavaScript
gulp.task('js:lint', () => {
    return gulp
    .src(SOURCES.js)
    .pipe($.plumber())
    .pipe($.babel())
    .pipe($.eslint())
    .pipe($.eslint.format())
    .pipe($.eslint.failAfterError());
});

// Concatenate JavaScript
gulp.task('js', ['js:clean'], () => {
    return gulp
    .src(SOURCES.concatjs)
    .pipe($.sourcemaps.init())
    .pipe($.plumber())
    .pipe($.print())
    .pipe($.concat(`${PROJECT}.js`))
    .pipe($.sourcemaps.write('/'))
    .pipe($.plumber.stop())
    .pipe(gulp.dest(PATHS.js));
});

// Minify JavaScript
gulp.task('js:minify', () => {
    return gulp
    .src(`${PATHS.js}/${PROJECT}.js`)
    .pipe($.plumber())
    .pipe($.uglify())
    .pipe($.rename({
        basename: PROJECT,
        suffix: '.min',
        extname: '.js'
    }))
    .pipe($.plumber.stop())
    .pipe(gulp.dest(PATHS.js))
    .pipe(BROWSERSYNC.stream());
});

/* -------------------------------------------------------------------------------------------------
  Image Tasks
------------------------------------------------------------------------------------------------- */
// Optimize images
gulp.task('img', () => {
    return gulp
    .src(SOURCES.img)
    .pipe($.plumber())
    .pipe($.imagemin(OPTIONS.imagemin))
    .pipe($.print())
    .pipe(gulp.dest(PATHS.img));
});

/* -------------------------------------------------------------------------------------------------
  Default Tasks
------------------------------------------------------------------------------------------------- */
// Default task
gulp.task('default', ['sass', 'js', 'server', 'watch']);

// Watch files for changes
gulp.task('watch', () => {
    gulp.watch(SOURCES.sass, ['sass']);
    gulp.watch(SOURCES.js, ['js']);
    gulp.watch(SOURCES.php, BROWSERSYNC.reload);
    gulp.watch(SOURCES.html, BROWSERSYNC.reload);
});
