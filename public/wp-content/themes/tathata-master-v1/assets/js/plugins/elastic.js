jQuery(document).ready(function($) {

	// Elastic Text
	var totalHeight = '100%', $tathexpand = $('a.tath-expand');
	$tathcontract = $('a.tath-contract');
	$elastic = $('div.elastic');
	$elasticwrap = $('div.elastic-wrap');
	$elasticsize = $('div.elastic-wrap').children($elastic);

	$elasticsize.each(function(i, el) {
		$(this).data('height', $(this).height());
	});

	$tathexpand.click(function() {
		$(this).closest($elasticwrap).find($elastic).animate({height: totalHeight}, 500);
		$(this).hide();
		$(this).closest($elasticwrap).find($tathcontract).show();
	});

	$tathcontract.click(function() {
		$(this).closest($elasticwrap).find($elastic).animate({
			height: $(this).closest($elasticwrap).find($elastic).data('height')
		}, 200);
		$(this).hide();
		$(this).closest($elasticwrap).find($tathexpand).show();
	});

});
jQuery(document).ready(function($) {
	$('.menu-item-5604').click(function() {
		$('#owncssmenu li#menu-item-5604').toggleClass('li-clicked');
		$('li.menu-item-5604 a').toggleClass('clicked');
		$('#owncssmenu ul.sub-menu').toggleClass('visible');
	});
});

jQuery(document).ready(function($) {

	$('div#more').click(function() {
		if ($('div#more span').hasClass('magic-btn-close')) {
			$('div#more').html('<span class="magic-btn-open"><img class="img-tog" src="http://www.tathatagolf.com/wp-content/themes/tathata-master-v1/assets/img/LinkBtnBgUpdark.png"/>Collapse. . . </span>');
		} else {
			$('div#more').html('<span class="magic-btn-close"><img class="img-tog" src="http://www.tathatagolf.com/wp-content/themes/tathata-master-v1/assets/img/LinkBtnBgDowndark.png"/>Expand. . . </span>');
		}
	});

	$('div#more-two').click(function() {
		if ($('div#more-two span').hasClass('magic-btn-close-two')) {
			$('div#more-two').html('<span class="magic-btn-open-two"><img class="img-tog" src="http://www.tathatagolf.com/wp-content/themes/tathata-master-v1/assets/img/LinkBtnBgUpdark.png"/>Collapse. . . </span>');
		} else {
			$('div#more-two').html('<span class="magic-btn-close-two"><img class="img-tog" src="http://www.tathatagolf.com/wp-content/themes/tathata-master-v1/assets/img/LinkBtnBgDowndark.png"/>Expand. . . </span>');
		}
	});

	$('div#more-three').click(function() {
		if ($('div#more-three span').hasClass('magic-btn-close-three')) {
			$('div#more-three').html('<span class="magic-btn-open-three"></span><img class="img-tog" src="http://www.tathatagolf.com/wp-content/themes/tathata-master-v1/assets/img/LinkBtnBgUpdark.png"/>Collapse. . . ');
		} else {
			$('div#more-three').html('<span class="magic-btn-close-three"></span><img class="img-tog" src="http://www.tathatagolf.com/wp-content/themes/tathata-master-v1/assets/img/LinkBtnBgDowndark.png"/>Expand. . . ');
		}
	});

	$('div#more-four').click(function() {
		if ($('div#more-four span').hasClass('magic-btn-close-four')) {
			$('div#more-four').html('<span class="magic-btn-open-four"></span><img class="img-tog" src="http://www.tathatagolf.com/wp-content/themes/tathata-master-v1/assets/img/LinkBtnBgUpdark.png"/>Collapse. . . ');
		} else {
			$('div#more-four').html('<span class="magic-btn-close-four"></span><img class="img-tog" src="http://www.tathatagolf.com/wp-content/themes/tathata-master-v1/assets/img/LinkBtnBgDowndark.png"/>Expand. . . ');
		}
	});

	$('div#more-five').click(function() {
		if ($('div#more-five span').hasClass('magic-btn-close-five')) {
			$('div#more-five').html('<span class="magic-btn-open-five"></span><img class="img-tog" src="http://www.tathatagolf.com/wp-content/themes/tathata-master-v1/assets/img/LinkBtnBgUpdark.png"/>Collapse. . . ');
		} else {
			$('div#more-five').html('<span class="magic-btn-close-five"></span><img class="img-tog" src="http://www.tathatagolf.com/wp-content/themes/tathata-master-v1/assets/img/LinkBtnBgDowndark.png"/>Expand. . . ');
		}
	});

	$('div#more-six').click(function() {
		if ($('div#more-six span').hasClass('magic-btn-close-six')) {
			$('div#more-six').html('<span class="magic-btn-open-six"></span><img class="img-tog" src="http://www.tathatagolf.com/wp-content/themes/tathata-master-v1/assets/img/LinkBtnBgUpdark.png"/>Collapse. . . ');
		} else {
			$('div#more-six').html('<span class="magic-btn-close-six"></span><img class="img-tog" src="http://www.tathatagolf.com/wp-content/themes/tathata-master-v1/assets/img/LinkBtnBgDowndark.png"/>Expand. . . ');
		}
	});

});
