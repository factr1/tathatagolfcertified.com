'use strict';

jQuery(function($) {
    /* --------------------------------------------------------------
		## Owl Carousel
	-------------------------------------------------------------- */
    $('#owl-demo').owlCarousel({
        navigation: true, // Show next and prev buttons
        slideSpeed: 300, paginationSpeed: 400, singleItem: true, navigationText: [
            '<i class="icon-chevron-left icon-control"></i>', '<i class="icon-chevron-right icon-control"></i>'
        ]
    });

    $('#owl-demo2').owlCarousel({
        navigation: true, // Show next and prev buttons
        slideSpeed: 300, paginationSpeed: 400, singleItem: true, navigationText: [
            '<i class="icon-chevron-left icon-control"></i>', '<i class="icon-chevron-right icon-control"></i>'
        ]
    });

    /* --------------------------------------------------------------
		## Easy Responsive Tabs
	-------------------------------------------------------------- */
    // Horizontal Tabs in Courses
    var tabs$ = $('#parentHorizontalTab').find('.resp-tabs-list');

    if (tabs$.length && ! window.location.hash) {
        tabs$.children('li').each(function(index) {
            if ($(this).is('.default')) {
                location.href = location.href + '#parentHorizontalTab' + (index + 1);
            }
        });
    }

    $('#parentHorizontalTab').easyResponsiveTabs({
        type: 'default', width: 'auto', fit: true, tabidentify: 'hor_1', activate: function(event) {
            var $tab = $(this);
            var $info = $('#nested-tabInfo');
            var $name = $('span', $info);

            $name.text($tab.text());
            $info.show();
        }
    });

    /* --------------------------------------------------------------
		## Single Course(s)
	-------------------------------------------------------------- */
    // Course Progress bars
    $('#bar4').barfiller({
        barColor: '#900'
    });

    $('.accordion_example').accordion();

    // Course pagination
    $('.pagination-quiz').hide();
    $('.pagi-10').show();
    $('.page-num').click(function() {
        var pageView = $(this).data('value') * 10;

        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        $('.pagination-quiz').hide();
        $('.pagi-' + pageView).show();
    });

    /* --------------------------------------------------------------
		## matchHeight
	-------------------------------------------------------------- */
    $('#ms-testimonials').find('.content').matchHeight();
    $('.gearPart, .mb-content, .ms-description--container, .msComparison-item, .card.-as-nav-link').matchHeight();
    $('.chapter-support-video-item').find('.title').matchHeight();

    // temp until have a better solution
    $('.-mh').matchHeight();

    /* --------------------------------------------------------------
		## Mobile Menu
	-------------------------------------------------------------- */
    // Add the needed ul classes to the new header navigation
    $('.header-bottom .menu').addClass('nav navbar-nav');

    $('#mobile-nav .menu').each(function() {
        $(this).parent().attr({
            id: 'mobile-menu', class: 'slinky-menu'
        });
    });

    // Prevent Default on Submenu items
    $('#mobile-nav .menu .menu-item-has-children > a').on('click', function(e) {
        e.preventDefault();
    });

    // Move the parent item into its own child sub-menu
    $('#mobile-nav .menu .menu-item-has-children > a').each(function() {
        var $this = $(this);
        var menuLink = $this.attr('href');
        var menuText = $this.html();
        var newParentItem = '<li class="new-parent-item"><a href="' + menuLink + '">' + menuText + '</a></li>';

        $this.next().prepend(newParentItem);
    });

    // Slinky Mobile Menu
    $('#mobile-menu').slinky({
        label: 'Back', speed: 200, resize: true
    });

    /* --------------------------------------------------------------
		## Slick Sliders
	-------------------------------------------------------------- */
    // Hero slider
    $('.hero-slider').slick({
        dots: true,
        arrows: false,
        infinite: true,
        adaptiveHeight: true,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        autoplaySpeed: 3000,
        lazyLoad: 'ondemand'
    });

    /* --------------------------------------------------------------
		## Misc.
	-------------------------------------------------------------- */
    // Utilize the default Bootstrap responsive embeds
    $('.embed-responsive iframe').addClass('embed-responsive-item');

    // Move Elements Down in the DOM
    $.fn.moveDown = function() {
        var after = $(this).next();

        $(this).insertAfter(after);
    };
});
