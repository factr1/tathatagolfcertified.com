// Define the tour
var tour = {
	id: "day-1-tour", steps: [
		{
			title: "Welcome to the 60 Day Program!",
			content: "Welcome to Day 1 of the 60-Day Program, we look forward to you beginning your training. First, we would like to share a brief overview of how the course works, where to go for training and customer support, and also share the additional training resources and materials available to you to access as you progress through each lesson/day in the program.",
			target: "video-container",
			placement: "top"
		},
		{
			title: "My Profile",
			content: "Your profile page is your customized home page that displays your profile details and information, where you may select an image to display next any comments or questions posted to the website and Tathata Forum. Within your profile you will also find your training progress, days completed, badges and certificates earned. ",
			target: "dashboard-icon",
			placement: "bottom",
			xOffset: - 23,
			yOffset: 7
		},
		{
			title: "60-Day Program",
			content: "Easily navigate to any chapter or day in the course.",
			target: "60day-icon",
			placement: "bottom",
			xOffset: - 23
		},
		// {
		//   title: "Tathata Golf Movement Specialists",
		//   content: "Launched February 2016, professionals and teachers from fitness, golf and education industries are currently enrolled in the Tathata Golf Movement Specialist Program to become trained and certified in the Tathata Movements to further support Tathata 60-Day students all over the world, beginning fall/winter of 2016.",
		//   target: "ms-nav",
		//   placement: "bottom",
		//   xOffset: -23
		// },
		{
			title: "Daily Content",
			content: "These bullets outline the exercises and training that will be covered in the day’s lesson.",
			target: "video-details",
			placement: "left"
		},
		{
			title: "60-Day Program Support",
			content: "These links take you to Frequently Asked Questions about Tathata Golf, the 60-Day Program and/or other Tathata products/services. You may also join in on the Tathata community discussions through the Tathata forum, available only to 60-Day Program members to talk to other each other, share about their experiences with the training or ask the Tathata Staff a question.",
			target: "forum-chap",
			placement: "top",
			xOffset: 20
		},
		{
			title: "Lesson Media",
			content: "Upon completing each chapter you will be awarded a badge that will be displayed here and on your profile page.",
			target: "lesson-badges",
			placement: "top"
		},
		{
			title: "Lesson Test",
			content: "After watching your lesson video, you will take a 5-8 question quiz that is required to receive a grade of 100% to move on to the next day/lesson in the program. If you receive a grade less than 100%, you may scroll down to review what questions you answered correctly (marked with a green check mark) and incorrectly (marked with a red exclamation point). To clear your previous answered and re-take the quiz, click “Reset Quiz” at the bottom of the quiz page.",
			target: "lesson-quiz",
			placement: "top"
		},
		{
			title: "Chapter Overview",
			content: "This tab provides a outline of the Movement Routines, Video Tests, Deeper Discussions and Mental Training Exercises you will be introduced to in the chapter. As well as a brief description of the Chapter Support resources available to you found in the third tab such as the Tathata Forum and additional learning materials and videos to dive deeper into the fundamentals of the Tathata Movements.",
			target: "chapter-overview",
			placement: "top",
			xOffset: 30
		},
		{
			title: "Chapter Lesson Video Library",
			content: "This tab consists of a thumbnail and outline of the video lessons within the chapter to look ahead of what’s coming in the program, as well as another place to go directly to the day/lesson you are on in the chapter. As mentioned above with the daily quizzes, note you must complete and successfully pass the Day 1 lesson video and quiz to move onto Day 2.",
			target: "chapter-lessons",
			placement: "top",
			xOffset: 30
		},
		{
			title: "Chapter Support",
			content: "In addition to Days 1 – 60 video lessons, within this tab on the left, you will see a thumbnail of the additional learning materials that you may click to dive deeper into the fundamentals introduced to you within the chapter. On the right side of this tab are the Chapter Support Discussions and Forum Topics for you to view or join in on any training discussions, ask a question or share your thoughts and experiences from the training with the Tathata community.",
			target: "chapter-support",
			placement: "top",
			xOffset: 30
		},
		{
			title: "Daily Extras",
			content: "This tab consists of additional daily video content to help further support you and your training throughout the program. Note, not every day has a daily extra.",
			target: "daily-extras",
			placement: "top"
		}
	]
};

// Configure the tour
hopscotch.configure({
	showPrevButton: true
});

// Start the tour
hopscotch.startTour(tour);


