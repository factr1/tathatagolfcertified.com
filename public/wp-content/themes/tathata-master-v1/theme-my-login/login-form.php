<?php
/*
  If you would like to edit this file, copy it to your current theme's directory and edit it there.
  Theme My Login will always look in your theme's directory first, before using this default template.
  */
?>

<style>
    .login {
        padding: 5px;
    }

    .login p.error {
        padding: 5px;
        border: 1px solid #c00;
        background-color: #ffebe8;
        color: #333;
    }

    .login p.message {
        padding: 5px;
        border: 1px solid #e6db55;
        background-color: #ffffe0;
        color: #333;
    }

    .login form label {
        display: block;
    }

    .login form p.forgetmenot label {
        display: inline;
    }

    .login input {
        margin: 5px 0;
    }

    .profile .screen-reader-text,
    .profile .screen-reader-text span {
        height: 1px;
        left: -1000em;
        overflow: hidden;
        position: absolute;
        width: 1px;
    }

    .mu_register .hint {
        font-size: 12px;
        margin-bottom: 10px;
        display: block;
    }

    .mu_register label.checkbox {
        display: inline;
    }

    #pass-strength-result {
        border: 1px solid;
        margin: 12px 5px 5px 1px;
        padding: 3px 5px;
        text-align: center;
        width: 200px;
    }

    #pass-strength-result.strong,
    #pass-strength-result.short {
        font-weight: bold;
    }

    #pass-strength-result {
        background-color: #eee;
        border-color: #ddd !important;
    }

    #pass-strength-result.bad {
        background-color: #ffb78c;
        border-color: #ff853c !important;
    }

    #pass-strength-result.good {
        background-color: #ffec8b;
        border-color: #fc0 !important;
    }

    #pass-strength-result.short {
        background-color: #ffa0a0;
        border-color: #f04040 !important;
    }

    #pass-strength-result.strong {
        background-color: #c3ff88;
        border-color: #8dff1c !important;
    }

    .login form .input, .login input[type="text"] {
        font-size: 14px !important;
    }

    ul.tml-action-links li:first-child,
    ul.tml-action-links li:first-child a {
        display: none !important;
    }
</style>

<div class="login" id="theme-my-login<?php $template->the_instance(); ?>">
    <?php $template->the_action_template_message( 'login' ); ?>

    <?php $template->the_errors(); ?>

    <form name="loginform" id="loginform<?php $template->the_instance(); ?>" action="<?php $template->the_action_url( 'login' ); ?>" method="post">
        <div class="form-group">
            <label for="user_login<?php $template->the_instance(); ?>"><?php _e( 'Username' ); ?></label>
            <input id="user_login<?php $template->the_instance(); ?>" class="form-control" type="text" name="log" placeholder="Username" value="<?php $template->the_posted_value( 'log' ); ?>">
        </div>

        <div class="input-group">
            <input id="user_pass<?php $template->the_instance(); ?>" class="form-control" type="password" name="pwd" placeholder="Password">
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
            </span>
        </div>

        <?php do_action( 'login_form' ); ?>

        <div class="checkbox">
            <label for="rememberme<?php $template->the_instance(); ?>">
                <input id="rememberme<?php $template->the_instance(); ?>" type="checkbox" name="rememberme" value="forever">
                <?php esc_attr_e( 'Remember Me' ); ?>
            </label>
        </div>

        <div class="form-group">
            <input type="hidden" name="redirect_to" value="<?php $template->the_redirect_url( 'login' ); ?>">
            <input type="hidden" name="instance" value="<?php $template->the_instance(); ?>">
            <input type="hidden" name="action" value="login">
        </div>
    </form>

    <?php $template->the_action_links( array( 'login' => false ) ); ?>
</div>
