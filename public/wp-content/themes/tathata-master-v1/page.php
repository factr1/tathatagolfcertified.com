<?php
/**
 * The template for displaying the edit profile page
 */

get_header();
get_template_part( 'parts/page-title' );
get_template_part( 'parts/breadcrumbs' );
?>

    <section class="pageInnerContentWrap">
        <div class="container">
            <div class="row">

                <article class="col-sm-8">
                    <?php
                    if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
                        get_template_part( 'featured-content' );
                    }
                    ?>

                    <div id="primary" class="content-area">
                        <div id="content" class="site-content" role="main">
                            <?php
                            while ( have_posts() ) : the_post();
                                get_template_part( 'content', 'page' );

                                if ( comments_open() || get_comments_number() ) {
                                    comments_template();
                                }
                            endwhile;
                            ?>
                        </div>
                    </div>
                </article>

                <article class="col-sm-4 rightPanel">
                    <?php get_sidebar(); ?>
                </article>
            </div>
        </div>
    </section>

<?php
get_footer();
