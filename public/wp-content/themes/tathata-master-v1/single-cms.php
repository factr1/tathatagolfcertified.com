<?php
/**
 * The Template for displaying all single posts
 *
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */

get_header(); ?>

<?php
// Start the Loop.
while ( have_posts() ) : the_post(); ?>

    <div class="sliderWrap">
        <div id="owl-demo" class="owl-carousel owl-theme">
            <div class="item">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/cms-bg.jpg" alt="Tathata Certified Movement Specialists">
                <div class="banner-content">
                    <div class="container">
                        <div class="banner-content-align">
                            <?php
                            if ( get_field( "banner_content" ) ) {
                                the_field( "banner_content" );
                            }
                            ?>
                        </div><!-- end banner-content-align -->
                    </div><!-- container -->
                </div><!-- end banner content -->
            </div><!-- item -->
        </div><!-- owl-carousel -->

        <div class="cms-header">
            <div class="cms-detail">
                <ul>
                    <li>
                        <div class="cms-image"><?php echo get_avatar( get_the_author_meta( 'ID' ), 230 ); ?></div>
                    </li>
                    <li class="cms-second">
                        <div class="cms-personal">
                            <p class="cms-name">
                                <span class="wtitle1"><?php the_field( 'first_name' ); ?></span> <?php the_field( 'last_name' ); ?>
                            </p>
                            <p style="text-transform:uppercase;">Tathata Golf Certified Movement Specialist</p>
                            <p><?php the_field( 'city' ); ?>, <?php the_field( 'state' ); ?> <?php the_field( 'zip' ); ?></p>
                            <p style="margin-bottom:30px;">
                                <em style="color:#990000;">Certified since: {date placeholder }</em>
                            </p>
                            <div class="ratingContainerWrap">
                                <?php echo do_shortcode( "[ratings]" ); ?>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="cms-affiliation">
                            <p>Memberships & Associations</p>
                            <p><?php the_field( 'memberships_associations' ); ?></p>
                            <p>Associated Facilities</p>
                            <p><?php the_field( 'associated_facility' ); ?></p>
                        </div>
                    </li>
                </ul>

                <div class="clear"></div>
            </div>
        </div>
    </div>

    <section class="pageInnerContentWrap">
        <div class="container">
            <div class="row">
                <article class="container">

                    <div id="primary" class="content-area">
                        <div id="content" class="site-content" role="main">
                			<?php
        					/*
        					 * Include the post format-specific template for the content. If you want to
        					 * use this in a child theme, then include a file called called content-___.php
        					 * (where ___ is the post format) and that will be used instead.
        					 */
        					get_template_part( 'content', 'cms' );

        					echo '<div class="experience">'.the_field('experience').'</div>';

        					echo '<div class="clear"></div><div class="cms_videos">'.get_cms_videos().'<div class="clear"></div></div>';

        					 the_tags( '<footer class="entry-meta tags"><h2 class="tagsDetail">TAGS:<span class="tag-links">', '', '</span></h2></footer>' );
        					// Previous/next post navigation.
        					twentyfourteen_post_nav();

        					// If comments are open or we have at least one comment, load up the comment template.
        					if ( comments_open() || get_comments_number() ) {
        						comments_template();
        					} ?>
            			</div><!-- #content -->
                    </div><!-- #primary -->

                </article>
            </div>
        </div><!-- #main-content -->
    </section>
<?php endwhile; ?>

<?php
get_footer();
