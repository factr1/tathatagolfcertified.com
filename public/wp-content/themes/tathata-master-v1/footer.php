</main><!-- /.contentWrapper -->

<!-- Footer -->
<footer id="primary-footer" class="footer" role="contentinfo">
    <?php if ( have_rows( 'tathata_supporters', 'option' ) ) : ?>
        <?php get_template_part( 'parts/affiliations' ); ?>
    <?php endif; ?>

    <section class="tg-footer-top">
        <div class="container">
            <div class="row">

                <div class="col-md-4 footer-col">
                    <h3 class="footer-title">Quick links</h3>
                    <?php wp_nav_menu( array( 'menu' => 'footer_menu' ) ); ?>
                </div>

                <div class="col-md-4 -bordered footer-col"></div>

                <div class="col-md-4 footer-col">

                    <div class="tg-social-shares">
                        <h4 class="footer-title">Where else to find us:</h4>

                        <a target="_blank" href="<?php echo esc_url( 'https://www.facebook.com/pages/Tathata-Golf/526700354108550' ); ?>">
                            <i class="fa fa-facebook" aria-hidden="true"></i> </a>
                        <a target="_blank" href="<?php echo esc_url( 'https://twitter.com/TG_certified' ); ?>">
                            <i class="fa fa-twitter" aria-hidden="true"></i> </a>
                        <a target="_blank" href="<?php echo esc_url( 'https://www.linkedin.com/company/tathata-golf-certified' ); ?>">
                            <i class="fa fa-linkedin" aria-hidden="true"></i> </a>
                        <a target="_blank" href="<?php echo esc_url( 'https://i.instagram.com/tg_certified' ); ?>">
                            <i class="fa fa-instagram" aria-hidden="true"></i> </a>
                        <a target="_blank" href="<?php echo esc_url( 'https://www.youtube.com/channel/UCG1AlyrH_eqgQ7cekyYc3Ig' ); ?>">
                            <i class="fa fa-youtube" aria-hidden="true"></i> </a>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <!-- Copyright and Legal info -->
    <section class="tg-footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="footer-logo"></div>

                    <p class="copyright">&copy; <?php echo date( "Y" ) ?> Tathata, LLC. &copy; 2012-16 Hepler Golf, LLC. All Rights Reserved.</p>

                    <span class="legal-info">
                        <a href="<?php echo esc_url( home_url( '/privacy-policy/' ) ); ?>">Privacy Policy</a> /
                        <a href="<?php echo esc_url( home_url( '/terms/' ) ); ?>">Terms of Use</a> /
                        <a href="<?php echo esc_url( home_url( '/returns-and-refunds/' ) ); ?>">Return Policy</a>
                    </span>

                    <p class="image-permission">No imagery or logos contained within this site may be used without the express permission of Hepler Golf, LLC.</p>
                </div>

                <div class="col-md-3">
                    <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/cash-icons.png" alt="">
                </div>
            </div>
        </div>
    </section>
</footer>

<?php wp_footer(); ?>

</body>
</html>
