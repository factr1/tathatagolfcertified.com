<?php
/**
 * Searchform.php
 *
 * Overrides the default HTML5 search form with this custom search form.
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
    <div class="input-group">
        <span class="screen-reader-text sr-only"><?php echo _x( 'Search:', 'label' ) ?></span>

        <input type="search" class="search-field form-control"
            placeholder="<?php echo esc_attr_x( 'Search', 'placeholder' ) ?>" value="<?php echo get_search_query() ?>"
            name="s" title="<?php echo esc_attr_x( 'Search:', 'label' ) ?>" />

        <span class="input-group-btn">
            <button class="btn" type="submit"><i class="fa fa-search"></i></button>
        </span>
    </div>
</form>
