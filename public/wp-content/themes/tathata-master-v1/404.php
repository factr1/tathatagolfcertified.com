<?php
/**
 * The template for displaying 404 pages (Not Found)
 */

get_header();
?>

    <section id="title-section">
        <div class="container">
            <h1 class="page-title"><?php _e( 'Sorry, that page has not been found', 'twentyfourteen' ); ?></h1>
        </div>
    </section>

    <section id="page404">
        <div id="primary" class="content-area">
            <div id="content" class="site-content" role="main">
                <div class="page-content">
                    <div class="container">
                        <p><?php _e( 'The page you have requested does not seem to exist... Maybe try a search?', 'twentyfourteen' ); ?></p>

                        <?php get_search_form(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
