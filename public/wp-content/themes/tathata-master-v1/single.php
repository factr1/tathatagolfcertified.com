<?php
/**
 * The Template for displaying all single posts
 */
get_header();
get_template_part( 'parts/page-title' );
?>

    <div id="blackbar"></div>

<?php setBreadcrumbs( get_the_title() ); ?>

    <section class="pageInnerContentWrap">
        <div class="container">
            <div class="row blog-page-container">
                <div class="clearfix"></div>
                <article class="single-left-panel col-sm-9">

                    <div id="primary" class="content-area">
                        <div id="content" class="site-content" role="main">
                            <?php
                                // Start the Loop.
                                while ( have_posts() ) : the_post();

                                /*
                                * Include the post format-specific template for the content. If you want to
                                * use this in a child theme, then include a file called called content-___.php
                                * (where ___ is the post format) and that will be used instead.
                                */
                                get_template_part( 'content', get_post_format() );
                                the_tags( '<footer class="entry-meta tags"><h2 class="tagsDetail">TAGS:<span class="tag-links">', '', '</span></h2></footer>' );
                                // Previous/next post navigation.
                                twentyfourteen_post_nav();

                                // If comments are open or we have at least one comment, load up the comment template.
                                if ( comments_open() || get_comments_number() ) {
                                comments_template();
                                }
                                endwhile;
                            ?>
                        </div><!-- #content -->
                    </div><!-- #primary -->

                </article>

                <article class="col-sm-3 rightPanel">
                    <?php
                    get_sidebar();
                    get_sidebar( 'content' );
                    ?>
                </article>
            </div>
        </div><!-- #main-content -->
    </section>

<?php
get_footer();
