<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

get_header();
?>

<div class="gear-light-grayBg">

    <article class="captionWrap single-product-wrap"></article>

    <div class="container storypage">
        <div class="row">
            <ol class="woocommerce-single-prod-breadcrumb story-breadcrumb breadcrumb pull-left">

                <?php
                /**
                 * woocommerce_before_main_content hook
                 *
                 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
                 * @hooked woocommerce_breadcrumb - 20
                 */
                // do_action( 'woocommerce_before_main_content' );
                ?>
            </ol>

            <?php // echo do_shortcode( '[followUs]' ); ?>
        </div>
    </div>

    <?php while ( have_posts() ) : the_post(); ?>

        <?php wc_get_template_part( 'content', 'single-product' ); ?>

    <?php endwhile; // end of the loop. ?>

    <?php
    /**
     * woocommerce_after_main_content hook
     *
     * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
     */
    do_action( 'woocommerce_after_main_content' );

    /**
     * woocommerce_sidebar hook
     *
     * @hooked woocommerce_get_sidebar - 10
     */
    //do_action( 'woocommerce_sidebar' );
    ?>
</div>

<?php get_footer( 'shop' ); ?>

<script>
    jQuery(document).ready(function() {
        jQuery(".woocommerce-review-link").click(function() {
            jQuery('#collapsereviews').removeClass("collapse");
            jQuery('#collapsereviews').css("height", "auto");
            jQuery('#collapsereviews').slideDown(500);
            jQuery('a[href="#collapsereviews"]').removeClass("collapsed");
            event.preventDefault();
        });

        jQuery('a[href="#collapsereviews"]').click(function() {
            jQuery(this).addClass("collapsed");
        });
    });
</script>
