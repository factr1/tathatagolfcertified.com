<?php
/**
 * Email Header
 *
 * @author  WooThemes
 * @package WooCommerce/Templates/Emails
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

// Load colours
$bg        = get_option( 'woocommerce_email_background_color' );
$body      = get_option( 'woocommerce_email_body_background_color' );
$base      = get_option( 'woocommerce_email_base_color' );
$base_text = wc_light_or_dark( $base, '#202020', '#ffffff' );
$text      = get_option( 'woocommerce_email_text_color' );

$bg_darker_10    = wc_hex_darker( $bg, 10 );
$base_lighter_20 = wc_hex_lighter( $base, 20 );
$text_lighter_20 = wc_hex_lighter( $text, 20 );

// For gmail compatibility, including CSS styles in head/body are stripped out therefore styles need to be inline. These variables contain rules which are added to the template inline. !important; is a gmail hack to prevent styles being stripped if it doesn't like something.
$wrapper            = "
    background-color: " . esc_attr( $bg ) . ";
    width:100%;
    -webkit-text-size-adjust:none !important;
    margin:0;
    padding: 70px 0 70px 0;
";
$template_container = "
    box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;
    background-color: " . esc_attr( $body ) . ";
    border: 1px solid $bg_darker_10;
    border-top: 0 !important;
    max-width: 980px;
";
$template_header    = "
    background-color: " . esc_attr( $base ) . ";
    color: $base_text;
    border-bottom: 0;
    font-family:Arial;
    font-weight:bold;
    line-height:100%;
    vertical-align:middle;
    border-radius: 0 !important;
";
$body_content       = "
    background-color: " . esc_attr( $body ) . ";
    border-radius:6px !important;
";
$body_content_inner = "
    color: $text_lighter_20;
    font-family:Arial;
    font-size:14px;
    line-height:150%;
    text-align:left;
";
$header_content_h1  = "
    color: black;
    margin:0;
    text-shadow: 0 1px 0 $base_lighter_20;
    display:block;
    font-family:Arial;
    font-size:30px;
    font-weight:bold;
    text-align:left;
    line-height: 150%;
    padding: 0 !important;
";

$header_image = "
    background-color: black;
    padding: 35px 15px;
";

$header_title = "
    background-color: #fcfcfc;
    padding: 30px 15px;
    padding-bottom: 0;

";

$width_wrapper = "
    max-width: 600px;
     margin: 0 auto;
     overflow: hidden;
";

?>
<!DOCTYPE html>
<html dir="<?php echo is_rtl() ? 'rtl' : 'ltr' ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
</head>
<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<div id="wrapper" dir="<?php echo is_rtl() ? 'rtl' : 'ltr' ?>">
    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr>
            <td align="center" valign="top">

                <table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_container" style="<?php echo $template_container; ?>">
                    <tr>
                        <td align="center" valign="top">
                            <!-- Header -->
                            <table border="0 !important" cellpadding="0" cellspacing="0" width="100%" id="template_header" style="<?php echo $template_header; ?>" bgcolor="<?php echo $base; ?>">
                                <tr>
                                    <td>

                                        <div id="template_header_image" style="<?php echo $header_image; ?>">
                                            <div class="width-wrapper" style="<?php echo $width_wrapper; ?>">
                                                <?php
                                                if ( $img = get_option( 'woocommerce_email_header_image' ) ) {
                                                    echo '<a href="' . get_bloginfo( 'url' ) . '"><img src="' . esc_url( $img ) . '" alt="' . get_bloginfo( 'name' ) . '" /></a>';
                                                }
                                                ?>
                                            </div>
                                        </div>

                                    </td>
                                </tr>
                                <tr>
                                    <td style="<?php echo $header_title; ?>">
                                        <div class="width-wrapper" style="<?php echo $width_wrapper; ?>">
                                            <h1 style="<?php echo $header_content_h1; ?>"><?php echo $email_heading; ?></h1>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <!-- End Header -->
                        </td>
                    </tr>

                    <tr>
                        <td align="center" valign="top" style="padding: 30px 15px;">
                            <!-- Body -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" id="template_body">
                                <tr>
                                    <td valign="top" id="body_content" style="<?php echo $body_content; ?>">
                                        <!-- Content -->
                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                            <tr>
                                                <td valign="top" style="padding: 0 !important;">
                                                    <div class="width-wrapper" style="<?php echo $width_wrapper; ?>">
                                                        <div id="body_content_inner" style="<?php echo $body_content_inner; ?>">
