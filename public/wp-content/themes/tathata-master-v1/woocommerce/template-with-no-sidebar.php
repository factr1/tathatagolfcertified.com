<?php
/**
 * Template Name: Template with no sidebar
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 */

get_header();
?>

<?php get_template_part( 'parts/page-title' ); ?>
    <div class="gear-light-grayBg">

        <div id="blackbar"></div>

        <?php if ( ( is_bbpress() ) && ( ! bbp_is_single_user() ) ) { ?>
            <div class="headerwidgets">
                <div class="forehead-wrapper">
                    <?php dynamic_sidebar( 'sidebar-forehead' ); ?>
                </div>
                <div class="clear"></div>
            </div>
        <?php } ?>

        <?php setBreadcrumbs( get_the_title() ); ?>

        <section class="pageInnerContentWrap">
            <div class="container">
                <div class="row">
                    <article class="col-xs-12">
                        <?php
                        if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
                            // Include the featured content template.
                            get_template_part( 'featured-content' );
                        }
                        ?>
                        <div id="primary" class="content-area">
                            <div id="content" class="site-content" role="main">

                                <?php
                                // Start the Loop.
                                while ( have_posts() ) : the_post();

                                    // Include the page content template.
                                    get_template_part( 'content', 'page' );

                                    // If comments are open or we have at least one comment, load up the comment template.
                                    if ( comments_open() || get_comments_number() ) {
                                        comments_template();
                                    }
                                endwhile;
                                ?>

                            </div><!-- #content -->
                        </div><!-- #primary -->
                    </article>

                </div>
            </div><!-- #main-content -->
        </section>
    </div>

<?php
get_footer();
