<?php
/**
 * Show messages
 *
 * @author        WooThemes
 * @package       WooCommerce/Templates
 * @version       1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
} // Exit if accessed directly

if ( ! $messages ) {
    return;
}
?>

<?php foreach ( $messages as $message ) : ?>
    <?php
    if ( 'Coupon code applied successfully.' == $message ) :
        $message = 'Promo code applied successfully.';
    elseif ( 'Coupon has been removed.' == $message ) :
        $message = 'Promo code has been removed.';
    endif;
    ?>
    <div class="woocommerce-message tt-alert tt-success"><?php echo wp_kses_post( $message ); ?></div>
<?php endforeach; ?>
