<?php
/**
 * Single Product title
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
?>

<hgroup>
    <h2 itemprop="name" class="cont-title product_title entry-title prod-name">
        <?php the_title(); ?>
    </h2>
</hgroup>
