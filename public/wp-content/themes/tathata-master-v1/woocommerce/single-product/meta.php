<?php
/**
 * Single Product Meta
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

global $post, $product;

$cat_count = sizeof( get_the_terms( $post->ID, 'product_cat' ) );
$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );
?>

<hgroup class="prod-record">
    <?php do_action( 'woocommerce_product_meta_start' ); ?>

    <?php if ( wc_product_sku_enabled() && ( $product->get_sku() || $product->is_type( 'variable' ) ) ) : ?>

        <h4><?php _e( 'SKU:', 'woocommerce' ); ?>
            <span itemprop="sku"><?php echo ( $sku = $product->get_sku() ) ? $sku : __( 'N/A', 'woocommerce' ); ?></span>.
        </h4>

    <?php endif; ?>

    <?php echo $product->get_categories( ', ', '<h4>' . _n( 'Category:<span>', 'Categories:<span>', $cat_count, 'woocommerce' ) . ' ', '.</span></h4>' ); ?>

    <?php echo $product->get_tags( ', ', '<h4>' . _n( 'Tag:<span>', 'Tags:<span>', $tag_count, 'woocommerce' ) . ' ', '.</span></h4>' ); ?>

    <?php do_action( 'woocommerce_product_meta_end' ); ?>
</hgroup>
