<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

get_header();
?>

    <div class="gear-light-grayBg">

        <article class="captionWrap">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7">
                        <?php
                        $shop_page_id = woocommerce_get_page_id( 'shop' );
                        echo get_post_field( 'post_content', $shop_page_id );
                        ?>
                    </div>

                    <div class="col-xs-12 col-sm-5 col-md-5">
                        <!-- NOTE: modified to only show the Men's Black Hat -->
                        <div class="redBoxContent ss">
                            <?php
                            $args = array(
                                'post_type'      => 'product',
                                'posts_per_page' => 1,
                                'p'              => 2277
                            );
                            $loop = new WP_Query( $args );

                            while ( $loop->have_posts() ) : $loop->the_post();
                                global $product;
                                ?>
                                <div class="woocommerce">
                                    <p class="wootitle"><?php the_title(); ?></p>
                                    <!-- <p><?php the_excerpt(); ?></p> -->
                                    <p>Protect your face from the sun on and off the golf course with this low-profile, feather-light hat made by Imperial...</p>
                                    <p class="woobottom">
                                        <a href="<?php echo do_shortcode( '[add_to_cart_url id="2277"]' ); ?>" title="<?php echo esc_attr( $loop->post->post_title ? $loop->post->post_title : $loop->post->ID ); ?>" class="greyLinkBtn">Add to Cart</a>

                                    <div class="star-rating" title="Rated <?php echo( $product->get_average_rating() ? $product->get_average_rating() : '0' ); ?> out of 5">
                                        <span style="width: <?php echo $product->get_average_rating() * 20; ?>%;">
                                            <strong itemprop="ratingValue" class="rating"><?php echo( $product->get_average_rating() ? $product->get_average_rating() : '0' ); ?></strong> out of 5
                                        </span>
                                    </div>

                                    <div class="count">(<?php echo $product->get_rating_count(); ?>)</div>
                                    <span class="price"><?php echo $product->get_price_html(); ?></span></p>
                                </div>
                            <?php endwhile; ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </article>

        <?php setBreadcrumbs( 'Shop' ); ?>

        <div class="container">
            <div class="row">

                <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
                    <h1 class="page-title"><?php // woocommerce_page_title(); ?></h1>
                <?php endif; ?>

                <?php // do_action( 'woocommerce_archive_description' ); ?>

                <section class="pageInnerContentWrap profilePage">
                    <div class="" id="storegrid">
                        <div class="row">
                            <?php
                            /**
                             * woocommerce_before_shop_loop hook
                             *
                             * @hooked woocommerce_result_count - 20
                             * @hooked woocommerce_catalog_ordering - 30
                             */
                            do_action( 'woocommerce_before_shop_loop' );
                            ?>
                        </div>

                        <div class="row">
                            <article class="col-xs-12 col-sm-9 col-md-9 gear-part-left">
                                <?php if ( have_posts() ) : ?>
                                    <?php woocommerce_product_loop_start(); ?>
                                    <?php woocommerce_product_subcategories(); ?>

                                    <?php while ( have_posts() ) : the_post(); ?>
                                        <?php wc_get_template_part( 'content', 'product' ); ?>
                                    <?php endwhile; // end of the loop. ?>

                                    <?php woocommerce_product_loop_end(); ?>

                                    <?php
                                    /**
                                     * woocommerce_after_shop_loop hook
                                     *
                                     * @hooked woocommerce_pagination - 10
                                     */
                                    do_action( 'woocommerce_after_shop_loop' );
                                    ?>

                                <?php elseif ( ! woocommerce_product_subcategories( array(
                                    'before' => woocommerce_product_loop_start( false ),
                                    'after'  => woocommerce_product_loop_end( false )
                                ) )
                                ) : ?>

                                    <?php wc_get_template( 'loop/no-products-found.php' ); ?>

                                <?php endif; ?>
                            </article><!-- END gear-part-left -->

                            <article class="col-xs-12 col-sm-3 col-md-3 rightPanel t_right">
                                <?php
                                /**
                                 * woocommerce_sidebar hook
                                 *
                                 * @hooked woocommerce_get_sidebar - 10
                                 */
                                do_action( 'woocommerce_sidebar' );
                                ?>
                                <?php
                                /**
                                 * woocommerce_after_main_content hook
                                 *
                                 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the
                                 *         content)
                                 */
                                do_action( 'woocommerce_after_main_content' );
                                ?>
                            </article><!-- End rightPanel t_right div -->
                        </div><!-- End row div -->
                    </div><!-- End #storegrid -->
                </section><!-- END pageInnerContentWrap -->
            </div><!-- End row div -->
        </div>
    </div>

<?php
get_footer();
