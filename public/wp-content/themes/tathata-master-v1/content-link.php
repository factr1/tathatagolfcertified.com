<?php
/**
 * The template for displaying posts in the Link post format
 *
 * @package    WordPress
 * @subpackage Twenty_Fourteen
 * @since      Twenty Fourteen 1.0
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php twentyfourteen_post_thumbnail(); ?>

    <div class="blogHeader">
        <div class="post-sizes">
            <?php
            if ( get_field( 'image' ) ):
                $image = get_field( 'image' );
                ?>
                <div class="image"><img src="<?php echo $image; ?>" alt="" /></div>
                <?php
            endif; ?>

            <?php if ( get_field( 'videourl' ) ):
                $videourl = get_field( 'videourl' );
                ?>
                <div class="video">
                    <iframe src="//www.youtube.com/embed/<?php echo $videourl; ?>" frameborder="0" allowfullscreen></iframe>
                </div>
                <?php
            endif; ?>
            <?php
            if ( get_field( 'galleryimage1' ) || get_field( 'gallery_image2' ) || get_field( 'gallery_image3' ) ):

                $galleryimg1 = get_field( 'galleryimage1' );
                $galleryimg2 = get_field( 'gallery_image2' );
                $galleryimg3 = get_field( 'gallery_image3' );
                ?>
                <div id="owl-demo" class="owl-carousel owl-theme">
                    <div class="item">
                        <img src="<?php echo $galleryimg1; ?>" alt="Image 1">
                    </div>
                    <div class="item">
                        <img src="<?php echo $galleryimg2; ?>" alt="Image 2">
                    </div>
                    <div class="item">
                        <img src="<?php echo $galleryimg3; ?>" alt="Image 3">
                    </div>
                </div>
                <?php
            endif;
            ?>
        </div>

        <?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && twentyfourteen_categorized_blog() ) : ?>
            <div class="entry-meta">
                <span class="cat-links"><?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfourteen' ) ); ?></span>
            </div><!-- .entry-meta -->
            <?php
        endif;

        if ( is_single() ) :
            the_title( '<h1 class="entry-title"><span class="dateOfPost">' . get_the_time( 'd M' ) . '</span><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
        else :
            the_title( '<h1 class="entry-title"><span class="dateOfPost">' . get_the_time( 'd M' ) . '</span><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
        endif;
        ?>

        <div class="entry-meta">
            <?php
            if ( 'post' == get_post_type() ) {
                printf( '<span class="entry-date">POSTED AT <time class="entry-date" datetime="%1$s">%2$s</time></span> <span class="inline"> IN <span class="category">%3$s</span></span> <span class="byline"> BY <span class="author vcard"><a class="url fn n" href="%4$s" rel="author">%5$s</a></span></span>', esc_attr( get_the_date( 'c' ) ), esc_html( get_the_date() ), get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfourteen' ) ), esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ), get_the_author() );
            }

            if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
                ?>
                <span class="comments-link"><?php comments_popup_link( __( 'Leave a comment', 'twentyfourteen' ), __( '1 Comment', 'twentyfourteen' ), __( '% Comments', 'twentyfourteen' ) ); ?></span>
            <?php endif; ?>

            <?php edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' ); ?>
        </div><!-- .entry-meta -->
    </div><!-- .entry-header -->

    <div class="entry-content">
		<?php
			the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyfourteen' ) );
			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfourteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .entry-content -->

</article><!-- #post-## -->
