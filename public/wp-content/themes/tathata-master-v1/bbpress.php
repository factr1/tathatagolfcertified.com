<?php
/**
 * The template for displaying bbpress
 *
 */
get_header();
?>

<?php get_template_part( 'parts/page-title' ); ?>

<?php if ( ! bbp_is_single_user() ) { ?>
    <div class="headerwidgets">
        <div class="forehead-wrapper">
            <style>
                .bbp-register-link {
                    display: none;
                }
            </style>
            <?php dynamic_sidebar( 'sidebar-forehead' ); ?>
        </div>
        <div class="clear"></div>
    </div>
<?php } ?>

    <article class="course-lessons-pageTitleWrap-three">
        <div class="container course-lessons-pageTitleWrap-container">
            <div class="row">
                <a href="<?php global $current_user;
                get_currentuserinfo();
                echo "/forums/members/" . $current_user->user_login . "/"; ?> "><img src="/wp-content/uploads/sites/5/2015/05/profile-icon.png" class="course-lessons-head-icon"></a>
                <a href="/dashboard/#"><img src="/wp-content/uploads/sites/5/2015/05/60-day-member-icon.png" class="course-lessons-head-icon"></a>
                <a href="/movement-specialists/"><img src="/wp-content/uploads/sites/5/2015/05/movement-specialist-icon.png" class="course-lessons-head-icon"></a>
            </div>
        </div>
    </article>

    <!-- Breadcrumb Area -->
    <section id="breadcrumbs">
        <div class="hidden-xs">
            <div class="container">
                <div class="row">

                    <div class="col-sm-8">
                        <div class="flex -flex-start">
                            <?php bbp_breadcrumb(); ?>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="flex -flex-end">
                            <?php echo do_shortcode( '[followUs]' ); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="visible-xs">
            <div class="container">
                <div class="flex">
                    <div>
                        <?php bbp_breadcrumb(); ?>
                    </div>
                    <div>
                        <?php echo do_shortcode( '[followUs]' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="pageInnerContentWrap">
        <div class="container bbpress-container">
            <div class="row bbpress-forum-container">
                <article class="col-sm-9 left-forum">
                    <div id="primary" class="content-area">
                        <div id="content" class="site-content" role="main">

                            <?php
                            // Start the Loop.
                            while ( have_posts() ) : the_post();

                                // Include the page content template.
                                get_template_part( 'content', 'page' );

                                // If comments are open or we have at least one comment, load up the comment template.
                                if ( comments_open() || get_comments_number() ) {
                                    comments_template();
                                }
                            endwhile;
                            ?>

                        </div><!-- #content -->
                    </div><!-- #primary -->

                </article>

                <article class="col-sm-3 rightPanel right-forum">
                    <?php
                    get_sidebar();
                    get_sidebar( 'content' );
                    ?>
                </article>

            </div>
        </div><!-- #main-content -->
    </section>

<?php
get_footer();
