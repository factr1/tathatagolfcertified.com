<?php
/**
 * The Template for displaying all single posts
 */

$term    = $wp_query->queried_object;
$taxname = $term->post_title;
$terms   = get_the_terms( get_the_ID(), 'oa-chapter' );

get_header();
setPageBanner( 'Certified Program Video Library' );
?>

    <div id="blackbar"></div>

<?php // Custom Breadcrumbs ?>
    <section id="breadcrumbs">
        <div class="hidden-xs">
            <div class="container">
                <div class="row">

                    <div class="col-sm-8">
                        <div class="flex -flex-start">
                            <ul class="breadcrumb">
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/course/certified-movement-specialist-program/' ) ); ?>">Home</a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/certified-program-video-library/' ) ); ?>">Certified Program Video Library</a>
                                </li>
                                <li>
                                    <a href="<?php echo esc_url( home_url( '/oa-chapter/' . $terms[0]->slug ) ); ?>"><?php echo $terms[0]->name; ?></a>
                                </li>

                                <?php if ( ! empty( $post->post_parent ) ) {
                                    $parentTitle = get_the_title( $post->post_parent ); ?>
                                    <li>
                                        <a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo $parentTitle; ?></a>
                                    </li>
                                <?php } ?>

                                <li><?php echo $taxname; ?></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="flex -flex-end">
                            <?php echo do_shortcode( '[followUs]' ); ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="visible-xs">
            <div class="container">
                <div class="flex">
                    <div>
                        <ul class="breadcrumb">
                            <li>
                                <a href="<?php echo esc_url( home_url( '/course/certified-movement-specialist-program/' ) ); ?>">Home</a>
                            </li>
                            <li>
                                <a href="<?php echo esc_url( home_url( '/certified-program-video-library/' ) ); ?>">Certified Program Video Library</a>
                            </li>
                            <li>
                                <a href="<?php echo esc_url( home_url( '/oa-chapter/' . $terms[0]->slug ) ); ?>"><?php echo $terms[0]->name; ?></a>
                            </li>

                            <?php if ( ! empty( $post->post_parent ) ) {
                                $parentTitle = get_the_title( $post->post_parent ); ?>
                                <li>
                                    <a href="<?php echo get_permalink( $post->post_parent ); ?>"><?php echo $parentTitle; ?></a>
                                </li>
                            <?php } ?>

                            <li><?php echo $taxname; ?></li>
                        </ul>
                    </div>

                    <div>
                        <?php echo do_shortcode( '[followUs]' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="single-oa-video" class="single-oa-video">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        <h1><?php the_title(); ?></h1>

                        <div class="embed-responsive embed-responsive-16by9">
                            <?php the_content(); ?>
                        </div>

                        <a class="single-oa-video-back" href="<?php echo esc_url( home_url( '/oa-chapter/' . $terms[0]->slug ) ); ?>">Return to the <?php echo $terms[0]->name; ?> video library</a>
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </section>

<?php
get_footer();
