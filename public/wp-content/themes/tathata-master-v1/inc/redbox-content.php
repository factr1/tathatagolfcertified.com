<div class="redBoxContent">
    <?php
    $args = array( 'post_type'      => 'product',
                   'posts_per_page' => 1,
                   'product_cat'    => 'streaming-and-memberships',
                   'orderby'        => 'rand'
    );
    $loop = new WP_Query( $args );

    while ( $loop->have_posts() ) : $loop->the_post();
        global $product;
        ?>
        <div class="woocommerce">
            <p class="wootitle"><?php the_title(); ?></p>

            <p><?php the_content(); ?></p>

            <p class="woobottom">
                <a href="<?php echo do_shortcode( '[add_to_cart_url id="4388"]' ); ?>" title="<?php echo esc_attr( $loop->post->post_title ? $loop->post->post_title : $loop->post->ID ); ?>" class="greyLinkBtn">Start Training Today</a>

                <div class="star-rating" title="Rated <?php echo( $product->get_average_rating() ? $product->get_average_rating() : '0' ); ?> out of 5">
    				<span style="width: <?php echo $product->get_average_rating() * 20; ?>%;">
    					<strong itemprop="ratingValue" class="rating"><?php echo( $product->get_average_rating() ? $product->get_average_rating() : '0' ); ?></strong> out of 5
    				</span>
                </div>

                <div class="count">(<?php echo $product->get_rating_count(); ?>)</div>
                <span class="price"><?php echo $product->get_price_html(); ?></span>
            </p>
        </div>
    <?php endwhile; ?>
    <?php wp_reset_query(); ?>
</div>
