<?php
// Grab the selected Page Hero Type
function getPageHero() {
    switch ( get_field( 'hero_type' ) ) :
        case 'none':
            return;
        case 'hero':
            return get_template_part( 'parts/hero' );
        case 'slider':
            return get_template_part( 'parts/slider' );
        case 'banner':
            return get_template_part( 'parts/banner' );
    endswitch;
}

// Grab the featured image for a post/page
function getFeaturedImage( $id = null ) {
    if ( has_post_thumbnail() ) {
        $featuredImageID  = get_post_thumbnail_id( $id );
        $featuredImageURL = wp_get_attachment_image_src( $featuredImageID, true );

        return $featuredImageURL[0];
    } else {
        return;
    }
}

// Create breadcrumbs for a category or archive page
function setBreadcrumbs( $title ) {
    $title = $title;
    include( locate_template( 'parts/breadcrumbs.php' ) );
}

// Create a banner for a category or archive page
function setPageBanner( $title ) {
    $customPageTitle = $title;
    include( locate_template( 'parts/page-title.php' ) );
}

/**
 * Simple wrapper for print_r()
 *
 * @param $code The variable to be checked
 */
function debug( $code ) {
    printf( '<pre>%s</pre>', print_r( $code, true ) );
}

/**
 * Custom sort the bbPress topics by modified_date ( aka "Freshness"), in descending order
 */
function custom_bbpress_topic_sorting() {
    $args['orderby'] = 'meta_value';
    $args['order']   = 'DESC';

    return $args;
}

add_filter( 'bbp_before_has_topics_parse_args', 'custom_bbpress_topic_sorting' );
