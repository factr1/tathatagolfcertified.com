<?php

// Add an ACF theme options panel
if ( function_exists( 'acf_add_options_page' ) ) {

    acf_add_options_page( array(
        'page_title' => 'Tathata Settings',
        'menu_title' => 'Tathata Settings',
        'menu_slug'  => 'tathata-settings',
        'capability' => 'edit_posts',
        'redirect'   => false
    ) );
}
