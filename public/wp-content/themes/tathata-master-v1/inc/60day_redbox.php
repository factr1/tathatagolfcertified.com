<div class="redBoxContent">
    <?php
    $args = array( 'post_type'      => 'product',
                   'posts_per_page' => 1,
                   'product_cat'    => 'streaming-and-memberships',
                   'orderby'        => 'rand'
    );
    $loop = new WP_Query( $args );

    while ( $loop->have_posts() ) : $loop->the_post();
        global $product;
        ?>
        <div class="woocommerce">
            <p class="wootitle"><?php the_title(); ?></p>

            <p class="woobottom">
                <a href="<?php echo do_shortcode( '[add_to_cart_url id="4388"]' ); ?>" title="<?php echo esc_attr( $loop->post->post_title ? $loop->post->post_title : $loop->post->ID ); ?>" class="greyLinkBtn"> Start Training Today </a>
                <span class="price"><?php echo $product->get_price_html(); ?></span>
            </p>

            <p style="clear:both">
                <a href="/?add_to_cart=8579" style="color:#fff; text-decoration:underline;">Add on the 30-Disc DVD set for offline viewing</a>
            </p>
        </div>
    <?php endwhile; ?>
    <?php wp_reset_query(); ?>
</div>
