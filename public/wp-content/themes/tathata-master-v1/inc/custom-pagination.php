<?php
/**
 * Custom pagination as per http://callmenick.com/post/custom-wordpress-loop-with-pagination
 * This function is a modified version of the default WordPress numbered pagination.
 *
 * @param  string $numpages  Set the maximun number of pages to paginate
 * @param  string $pagerange Set the number of pages to appear on either side of the current page
 * @param  string $paged     Set the current page number
 *
 * @return function          A modified version of the default WordPress numbered pagination
 */
function custom_pagination( $numpages = '', $pagerange = '', $paged = '' ) {
    if ( empty( $pagerange ) ) {
        $pagerange = 2;
    }
    /**
     * This first part of our function is a fallback
     * for custom pagination inside a regular loop that
     * uses the global $paged and global $wp_query variables.
     * It's good because we can now override default pagination
     * in our theme, and use this function in default quries
     * and custom queries.
     */
    global $paged;
    if ( empty( $paged ) ) {
        $paged = 1;
    }

    if ( $numpages == '' ) {
        global $wp_query;
        $numpages = $wp_query->max_num_pages;
        if ( ! $numpages ) {
            $numpages = 1;
        }
    }

    /**
     * We construct the pagination arguments to enter into our paginate_links
     * function.
     */
    $pagination_args = array(
        // 'base'            => get_pagenum_link(1) . '%_%',
        // 'format'          => 'page/%#%',
        'total'        => $numpages,
        'current'      => $paged,
        'show_all'     => false,
        'end_size'     => 1,
        'mid_size'     => $pagerange,
        'prev_next'    => true,
        'prev_text'    => __( '&laquo;' ),
        'next_text'    => __( '&raquo;' ),
        'type'         => 'plain',
        'add_args'     => false,
        'add_fragment' => ''
    );

    $paginate_links = paginate_links( $pagination_args );

    if ( $paginate_links ) {
        echo '<nav class="custom-pagination">';
      echo $paginate_links;
      //echo "<span class='page-numbers page-num'>Page " . $paged . " of " . $numpages . "</span> ";
      echo "</nav>";
    }
}
