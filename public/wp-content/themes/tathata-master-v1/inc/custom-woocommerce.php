<?php
// Add some fine print to international orders
function internationalComment() {
    printf('<p class="fine-print">%s</p>', 'International shipments may be subject to import duties and taxes, which are levied once the package reaches your country. These charges are the buyer\'s responsibility.');
}
add_action('woocommerce_after_cart_totals', 'internationalComment', 10,1);
