<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <link rel="profile" href="//gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

    <meta name="theme-color" content="#000">
    <meta name="msapplication-TileColor" content="#000">

    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri(); ?>/images/icons/Tathata-57x57-White.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/images/icons/Tathata-72x72-White.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/images/icons/Tathata-114x114-White.png" />
    <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/images/icons/Tathata-144x144-White.png" />

    <!--[if lt IE 9]>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?> role="document">

    <!-- Header -->
    <header id="primary-site-header" role="banner">
        <section class="header-top">
            <div class="container">
                <div class="row">

                    <!-- Logo -->
                    <div class="col-md-4">
                        <h1 class="header-logo text-hide">
                            <a href="<?php echo esc_url( home_url( '/course/certified-movement-specialist-program/' ) ); ?>">Tathata Golf Certified Movement Specialist Program</a>
                        </h1>
                    </div>

                    <!-- Login/Logout -->
                    <div class="col-md-8">
                        <div class="user-status">
                            <?php if ( is_user_logged_in() ) :
                                $current_user = wp_get_current_user();
                                echo '<a href="' . esc_url( home_url() ) . '/dashboard/" class="user-welcome"><span>Welcome, </span>' . $current_user->user_login . '</a>';
                                ?>
                                <a href="<?php echo wp_logout_url(); ?>" class="user-logout">Logout</a>
                            <?php else : ?>
                                <a href="<?php echo esc_url( home_url( '/login/' ) ); ?>">Login</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Primary Navigation -->
        <section class="header-bottom">
            <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
                <div class="container">
                    <span class="menu-text">Menu</span>

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#mobile-nav" aria-expanded="false">
                        <span class="sr-only">View Menu</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Mobile Menu -->
                    <div id="mobile-nav" class="collapse navbar-collapse">
                        <?php
                        $user_id = get_current_user_id();
                        list( $user_id, $tathusermenu, $oavisibility, $learnervisibility, $cmsvisibility, $advisibility, $gradvisibility, $user_group, $group_array ) = tath_view_role_params( $user_id );
                        wp_nav_menu( array( 'menu' => $tathusermenu ) );
                        ?>
                    </div>

                    <!-- Desktop Menu -->
                    <div id="desktop-nav" class="collapse navbar-collapse">
                        <?php
                        $user_id = get_current_user_id();
                        list( $user_id, $tathusermenu, $oavisibility, $learnervisibility, $cmsvisibility, $advisibility, $gradvisibility, $user_group, $group_array ) = tath_view_role_params( $user_id );
                        wp_nav_menu( array( 'menu' => $tathusermenu ) );
                        ?>
                    </div>
                </div>
            </nav>
        </section>
    </header>

    <!-- Main Content -->
    <main id="primary-site-content" class="contentWrapper" role="main">
