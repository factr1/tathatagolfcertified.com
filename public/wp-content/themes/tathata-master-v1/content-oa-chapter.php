<?php
// Content type for the Chapter Support Videos
?>

<a class="chapter-support-video" href="<?php the_permalink(); ?>">
    <div class="chapter-support-video-item">
        <div class="title">
            <h1><?php the_title(); ?></h1>
        </div>

        <div class="video" style="background: url('<?php echo getFeaturedImage(); ?>') center center/cover no-repeat">
            <i class="fa fa-play" aria-hidden="true"></i>
        </div>
    </div>
</a>
