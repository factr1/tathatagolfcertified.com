<?php

   /*
   Plugin Name: Tathata OA Videos
   Plugin URI: http://tathata.com
   Description: Custom user controls and profiles for the Online Academy Videos
   Version: 1.0
   Author: Chris Jenkins
   Author URI: http://lucidindustries.com
   License: GPL3
   */

// Register Custom Post Type
function oa_video() {

    $labels = array(
        'name'                => _x( 'OA Videos', 'Post Type General Name', 'tathata' ),
        'singular_name'       => _x( 'OA Video', 'Post Type Singular Name', 'tathata' ),
        'menu_name'           => __( 'OA Videos', 'tathata' ),
        'parent_item_colon'   => __( 'Parent Video:', 'tathata' ),
        'all_items'           => __( 'All OA Videos', 'tathata' ),
        'view_item'           => __( 'View Video', 'tathata' ),
        'add_new_item'        => __( 'Add New Video', 'tathata' ),
        'add_new'             => __( 'Add New', 'tathata' ),
        'edit_item'           => __( 'Edit Video', 'tathata' ),
        'update_item'         => __( 'Update Video', 'tathata' ),
        'search_items'        => __( 'Search Videos', 'tathata' ),
        'not_found'           => __( 'Not found', 'tathata' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'tathata' ),
    );
    $args = array(
        'label'               => __( 'oa-video', 'tathata' ),
        'description'         => __( 'OA Videos', 'tathata' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'revisions', 'custom-fields', ),
        'taxonomies'          => array( 'post_tag' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-format-video',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
    register_post_type( 'oa-video', $args );

}

// Hook into the 'init' action
add_action( 'init', 'oa_video', 0 );

// Add focus as a taxonomy

function oa_focus_taxonomy() {

    $labels = array(
        'name'                       => _x( 'OA Focus', 'Taxonomy General Name', 'tathata' ),
        'singular_name'              => _x( 'OA Focus', 'Taxonomy Singular Name', 'tathata' ),
        'menu_name'                  => __( 'Focus', 'tathata' ),
        'all_items'                  => __( 'All Focuses', 'tathata' ),
        'parent_item'                => __( 'Parent Focus', 'tathata' ),
        'parent_item_colon'          => __( 'Parent Focus:', 'tathata' ),
        'new_item_name'              => __( 'New Focus', 'tathata' ),
        'add_new_item'               => __( 'Add New Focus', 'tathata' ),
        'edit_item'                  => __( 'Edit Focus', 'tathata' ),
        'update_item'                => __( 'Update Focus', 'tathata' ),
        'separate_items_with_commas' => __( 'Separate Focuses with commas', 'tathata' ),
        'search_items'               => __( 'Search Focuses', 'tathata' ),
        'add_or_remove_items'        => __( 'Add or remove Focus', 'tathata' ),
        'choose_from_most_used'      => __( 'Choose from the most used Focuses', 'tathata' ),
        'not_found'                  => __( 'Not Found', 'tathata' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'oa-focus', array( 'oa-video' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'oa_focus_taxonomy', 0 );

// Add chapter as a taxonomy

function oa_chapter_taxonomy() {

    $labels = array(
        'name'                       => _x( 'OA Chapter', 'Taxonomy General Name', 'tathata' ),
        'singular_name'              => _x( 'OA Chapter', 'Taxonomy Singular Name', 'tathata' ),
        'menu_name'                  => __( 'Chapter', 'tathata' ),
        'all_items'                  => __( 'All Chapters', 'tathata' ),
        'parent_item'                => __( 'Parent Chapter', 'tathata' ),
        'parent_item_colon'          => __( 'Parent Chapter:', 'tathata' ),
        'new_item_name'              => __( 'New Chapter', 'tathata' ),
        'add_new_item'               => __( 'Add New Chapter', 'tathata' ),
        'edit_item'                  => __( 'Edit Chapter', 'tathata' ),
        'update_item'                => __( 'Update Chapter', 'tathata' ),
        'separate_items_with_commas' => __( 'Separate Chapters with commas', 'tathata' ),
        'search_items'               => __( 'Search Chapters', 'tathata' ),
        'add_or_remove_items'        => __( 'Add or remove Chapter', 'tathata' ),
        'choose_from_most_used'      => __( 'Choose from the most used Chapters', 'tathata' ),
        'not_found'                  => __( 'Not Found', 'tathata' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'oa-chapter', array( 'oa-video' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'oa_chapter_taxonomy', 0 );

// Add chapter support thumbnail size

function oaimagesize_setup()
{
    add_image_size('oa-thumbnail', 240,140,true);
}
add_action( 'after_setup_theme', 'oaimagesize_setup' );

add_image_size('oa-thumbnail', 240,140,true);

add_shortcode('chapter-videos',function($atts) {

    $atts = shortcode_atts([
        'term' => '',
    ],$atts,'bartag');

    if(!$atts['term']) {
        // Get current module
        $uri_array = explode( '/', $_SERVER["REQUEST_URI"] );
        $atts['term'] = $uri_array[2];
    }

    // WP_Query arguments
    $args = [
        'post_type' => 'oa-video',
        'post_status' => 'published',
        'taxonomy' => 'oa-chapter',
        'term' => $atts['term'],
        'pagination' => false,
        'order' => 'ASC',
        'orderby' => 'title',
        'posts_per_page' => -1,
    ];

    // The Query
    $query = new WP_Query( $args );

    // The Loop
    $output = '';
    if ( $query->have_posts() ) {
        while ( $query->have_posts() ) {
            $query->the_post();
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $query->ID ), 'oa-thumbnail' );
            $output .= '<div class="support-video"">'.
                                    '<p class="oa-video-thumb"><a href="'. get_permalink() .'" target="_blank"><img src="'. $image[0] .'"></a></p>'.
                                    '<p class="oa-video-title"><a href="'. get_permalink() .'" target="_blank">'.get_the_title().'</a></p>'.
                                    '<p class="oa-video-excerpt">'. get_the_excerpt() .'</p>'.
                                '</div>';
        }
    } else {
        // no posts found
        echo 'No posts found.';
    }

    // Restore original Post Data
    wp_reset_postdata();

    return $output;
});

function add_post_content_to_video_thumbnail_markup( $markup, $post_id ) {
    $post_array = get_post( $post_id );
    $post_content = $post_array->post_content;
    $post_content = apply_filters( 'the_content', $post_content );
    return $post_content . ' ' . $markup;
}

add_filter( 'video_thumbnail_markup', 'add_post_content_to_video_thumbnail_markup', 10, 2 );

?>