<?php

   /*
   Plugin Name: Tathata Users
   Plugin URI: http://tathata.com
   Description: Custom user controls and profiles for Tathata related sites.
   Version: 1.5
   Author: Chris Jenkins
   Author URI: http://lucidindustries.com
   License: GPL3
   */


/* General Setup Stuff */

// Load our css

add_action( 'wp_enqueue_scripts', 'tath_user_styles' );

function tath_user_styles() {
    wp_enqueue_style( 'tath-user-style', plugins_url( 'users.css', __FILE__ ) );
}

// Enable dynamic shortcode menu support

function shortcode_menu( $item_output, $item ) {

    if ( !empty($item->description)) {
         $output = do_shortcode($item->description);

         if ( $output != $item->description )
               $item_output = $output;

        }

    return $item_output;

}

add_filter("walker_nav_menu_start_el", "shortcode_menu" , 10 , 2);

// Setup an options group
add_action( 'admin_init', 'tathata_options_init' );

function tathata_options_init(){
    register_setting( 'tathata_user_options', 'tathata_user_options_group', 'tathata_options_validate' );
}

// Add our admin screen
add_action('admin_menu', 'add_tathata_menus');
    function add_tathata_menus() {
        add_menu_page( 'Tathata Profiles', 'Tathata Profiles', 'manage_options', 'tathata-users', 'tathata_admin','', '3');
    }

    function tathata_admin() {
        if( current_user_can( 'administrator' ) ) {

            tathata_admin_screen();

        } else {
            echo 'You do not have the permissions to view this screen.';
        }
    }

    function tathata_admin_screen() {
                    if ( ! isset( $_REQUEST['settings-updated'] ) )
        $_REQUEST['settings-updated'] = false;

    ?>
    <div class="wrap">
        <?php screen_icon(); echo "<h2>" . __( 'Tathata Profile Settings', 'tathata-users' ) . "</h2>"; ?>

        <?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
        <div class="updated fade"><p><strong><?php _e( 'Options saved', 'tathata-users' ); ?></strong></p></div>
        <?php endif; ?>
        <br>
        <div style=" border: 1px solid #ccc; padding: 10px 20px; font-size: 14px; color: #333; display: block; width: 75%;">
            <p><strong>The following functions are controlled from within the source to this plugin:</strong>
                <ul>
                    <li>Custom fields and contact information in public profiles.</li>
                    <li>User permissions and custom views.<li>
                    <li>The Member Videos custom post type.</li>
                </ul>
            </p>
            <p>The following options affect the public profiles of Tathata users.</p>
        </div>
        <p>&nbsp;</p>
        <form tathata="post" action="options.php">
            <?php settings_fields( 'tathata_user_options' ); ?>
            <?php $options = get_option( 'tathata_user_options_group' ); ?>

            <table class="form-table">
                <tr valign="top" style=""><th scope="row"><?php _e( '<h4 style="font-size: 1.2em;">Settings:</h4>', 'tathata-users' ); ?></th>
                    <td>
                        <table>
                            <tr><th scope="row"><?php _e( '<strong>Option 1:</strong>', 'tathata-users' ); ?></th>
                                <td>
                                    <input id="tathata_user_options_group[opt1]" class="regular-text" type="text" name="tathata_user_options_group[opt1]" value="<?php esc_attr_e( $options['opt1'] ); ?>" />
                                    <label class="description" for="tathata_user_options_group[opt1]"><?php _e( ' <em style="margin-left: 10px;">Option text.</em>', 'tathata-users' ); ?></label>
                                </td>
                            </tr>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            <p class="submit">
                <input type="submit" class="button-primary" value="<?php _e( 'Save Options', 'tathata-users' ); ?>" />
            </p>
        </form>
    </div>
    <?php
}

/**
 * Sanitize and validate input. Accepts an array, return a sanitized array.
 */
function tathata_options_validate( $input ) {
    // Setup select and radio options in case we need them later
    global $select_options, $radio_options;


    // Setup an area to do validation if we get asked to. Usage: $input['platter'] = wp_filter_post_kses($input['platter']) or similar. For further examples: http://codex.wordpress.org/Data_Validation;
    $input['opt1'] = $input['opt1'];



    return $input;
}

// Add additional info to the user profile

add_filter('user_contactmethods', 'tath_user_contactmethods');

function tath_user_contactmethods($fields)
{
    return array_merge($fields,[
        'phone' => 'Phone',
        'twitter' => 'Twitter',
        'instagram' => 'Instagram',
        'facebook' => 'Facebook',
    ]);

    return $fields;
}



    // Create a shortcode to generate a user profile edit link
    add_shortcode('usereditlink','tath_user_edit');

    function tath_user_edit() {
        /*
        ob_start();
        bbp_user_profile_url( bbp_get_current_user_id() );
        $tathedit = ob_get_contents();
        ob_get_clean();
        */

        $url = '#';

        $posts = get_posts([
            'post_type' => 'page',
            'post_status' => 'publish',
            'name' => 'edit-profile',
            'limit' => 1,
        ]);

        $url = $posts ? get_permalink($posts[0]) : $url;

        return '<a href="'.esc_attr($url).'">Edit Profile</a>';
    }

    // ...and another for a dynamic profile link

    add_shortcode('userprofilelink','tath_user_profile');

    function tath_user_profile() {
        /*
        ob_start();
        bbp_user_profile_url( bbp_get_current_user_id() );
        $tathedit = ob_get_contents();
        ob_get_clean();
        $tatheditlink = '<a href="'. $tathedit .'" style="text-decoration:underline;">View Profile</a>';

        return $tatheditlink;
        */
        $url = '#';

        $posts = get_posts([
            'post_type' => 'page',
            'post_status' => 'publish',
            'name' => 'edit-profile',
            'limit' => 1,
        ]);

        $url = $posts ? get_permalink($posts[0]) : $url;

        return '<a href="'.esc_attr($url).'">View Profile</a>';
    }

    // One more for linked usernames
    add_shortcode('userlinkedname','tath_linked_user');

    function tath_linked_user() {
        global $current_user;
        get_currentuserinfo();
        $tathusername = $current_user->display_name;
        ob_start();
        bbp_user_profile_url( bbp_get_current_user_id() );
        $tathedit = ob_get_contents();
        ob_get_clean();
        $tatheditlink = '<a href="'. $tathedit .'" class="welcomeUser">Welcome, ' . $tathusername . '</a>';

        return $tatheditlink;
    }


    /* Begin setting up the custom specialist videos */

    // Create custom post member video post types

        function tathata_member_videos() {

        $labels = array(
            'name'                => _x( 'Member Videos', 'Post Type General Name', 'tathata' ),
            'singular_name'       => _x( 'Member Video', 'Post Type Singular Name', 'tathata' ),
            'menu_name'           => __( 'Member Videos', 'tathata' ),
            'parent_item_colon'   => __( 'Parent Video:', 'tathata' ),
            'all_items'           => __( 'All Videos', 'tathata' ),
            'view_item'           => __( 'View Video', 'tathata' ),
            'add_new_item'        => __( 'Add New Video', 'tathata' ),
            'add_new'             => __( 'Add New', 'tathata' ),
            'edit_item'           => __( 'Edit Video', 'tathata' ),
            'update_item'         => __( 'Update Video', 'tathata' ),
            'search_items'        => __( 'Search Videos', 'tathata' ),
            'not_found'           => __( 'Not found', 'tathata' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'tathata' ),
        );
        $rewrite = array(
            'slug'                => 'videos',
            'with_front'          => true,
            'pages'               => true,
            'feeds'               => true,
        );
        $capabilities = array(
            'edit_post'           => 'specialistview',
            'read_post'           => 'read_post',
            'delete_post'         => 'specialistview',
            'edit_posts'          => 'specialistview',
            'edit_others_posts'   => 'edit_others_posts',
            'publish_posts'       => 'specialistview',
            'read_private_posts'  => 'read_private_posts',
        );
        $args = array(
            'label'               => __( 'member_video', 'tathata' ),
            'description'         => __( 'Tathata Member Videos', 'tathata' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'comments', 'custom-fields', 'post-formats', ),
            'taxonomies'          => array( 'post_tag' ),
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 10,
            'menu_icon'           => 'dashicons-video-alt3',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'rewrite'             => $rewrite,
            'capabilities'        => $capabilities,
        );
        register_post_type( 'member_video', $args );

    }

// Hook into the 'init' action
add_action( 'init', 'tathata_member_videos', 0 );

// Create custom video taxonomy

function video_init() {
    // create a new taxonomy
    register_taxonomy(
        'video-category',
        'member_video',
        array(
            'label' => __( 'Video Category' ),
            'rewrite' => array( 'slug' => 'video-categories' ),
            'hierarchical' => true
        )
    );
}

// Hook into the 'init' action
add_action( 'init', 'video_init' );

// Create a custom post type we'll use to display and search for movement specialists
function tathata_cms() {

    $labels = array(
        'name'                => _x( 'Movement Specialists', 'Post Type General Name', 'tathata' ),
        'singular_name'       => _x( 'Movement Specialist', 'Post Type Singular Name', 'tathata' ),
        'menu_name'           => __( 'Movement Specialists', 'tathata' ),
        'parent_item_colon'   => __( 'Parent Item:', 'tathata' ),
        'all_items'           => __( 'All Movement Specialists', 'tathata' ),
        'view_item'           => __( 'View Specialist', 'tathata' ),
        'add_new_item'        => __( 'Add New Specialist', 'tathata' ),
        'add_new'             => __( 'Add New', 'tathata' ),
        'edit_item'           => __( 'Edit Specialist', 'tathata' ),
        'update_item'         => __( 'Update Specialist', 'tathata' ),
        'search_items'        => __( 'Search Specialists', 'tathata' ),
        'not_found'           => __( 'Not found', 'tathata' ),
        'not_found_in_trash'  => __( 'Not found in Trash', 'tathata' ),
    );
    $args = array(
        'label'               => __( 'cms', 'tathata' ),
        'description'         => __( 'Tathata Movement Specialists', 'tathata' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
        'taxonomies'          => array( 'post_tag' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-universal-access-alt',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
    register_post_type( 'cms', $args );

}

// Hook into the 'init' action
add_action( 'init', 'tathata_cms', 0 );


// Create custom cms taxonomy

function cms_tax_init() {
    // create a new taxonomy
    register_taxonomy(
        'cms-state',
        'cms',
        array(
            'label' => __( 'CMS States' ),
            'rewrite' => array( 'slug' => 'cms-states' ),
            'hierarchical' => true
        )
    );
}

// Hook into the 'init' action
add_action( 'init', 'cms_tax_init' );

function get_cms_videos() {
                global $authordata, $post;


                $authors_posts = get_posts( array( 'author' => $authordata->ID, 'post_type' => 'member_video','post_status' => 'publish',));

                    $output = '<ul>';
                foreach ( $authors_posts as $authors_post ) {
                    $output .= '<li><a href="' . get_permalink( $authors_post->ID ) . '">' . get_the_post_thumbnail( $authors_post->ID, 'thumbnail' ).'</a><p><a href="' . get_permalink( $authors_post->ID ) . '">' . apply_filters( 'the_title', $authors_post->post_title, $authors_post->ID ) . '</a></p></li>';
                }
                    $output .= '</ul>';

                return $output;
    }

// Add a custom image size for the CMS bios

add_action( 'after_setup_theme', 'cms_bio_img' );
function cms_bio_img() {
  add_image_size( 'cms-bio', 230, 230 );
}

// Set user views based on roles

function tath_view_role_params($user_id) {

        // Setup our variables
        global $wpdb;
    $blog_id = get_current_blog_id();
    $result = false;
        $user_group = '';
        $user_id = get_current_user_id();

        // Check our user for groups membership
    $user_group_table = 'tg_groups_user_group';
    //$user_group_table = 'wp_'. $blog_id .'_groups_user_group';
    $user_group = $wpdb->get_row( $wpdb->prepare(
        "SELECT * FROM $user_group_table WHERE user_id = %d AND group_id != %d ORDER BY group_id DESC",
        $user_id, '1')
    );

    // Setting the grad visibility completed up here because we're going to switch it on for admins

    $gradvisibility = 'grad-invisible';

    if (    is_super_admin($user_id) ) {

            $tathusermenu = '219';
            $oavisibility = 'oa-visible';
            $learnervisibility = 'learner-visible';
            $cmsvisibility = 'cms-visible';
            $advisibility = 'ad-invisible';
            $gradvisibility = 'grad-visible';

    } else if ($user_group->group_id == '7') { // if the user_id belongs to Course Grads

            $gradvisibility = 'grad-visible';
            $tathusermenu = '65';
            $oavisibility = 'oa-visible';
            $learnervisibility = 'learner-visible';
            $cmsvisibility = 'cms-invisible';
            $advisibility = 'ad-invisible';

  /*  } else if ($user_group->group_id == '5') { // if the user_id belongs to Dev

            $tathusermenu = '69';
            $oavisibility = 'oa-visible';
            $learnervisibility = 'learner-visible';
            $cmsvisibility = 'cms-visible';
            $advisibility = 'ad-invisible';


    } else if ($user_group->group_id == '4') { // if the user_id belongs to Movement Specialists

            $tathusermenu = '68';
            $oavisibility = 'oa-visible';
            $learnervisibility = 'learner-visible';
            $cmsvisibility = 'cms-visible';
            $advisibility = 'ad-invisible';
*/

    } else if ($user_group->group_id == '3') { // if the user_id belongs to Course Members

            $tathusermenu = '220';
            $oavisibility = 'oa-visible';
            $learnervisibility = 'learner-visible';
            $cmsvisibility = 'cms-invisible';
            $advisibility = 'ad-invisible';

    } else {

            $tathusermenu = '221';
            $oavisibility = 'oa-invisible';
            $learnervisibility = 'learner-invisible';
            $cmsvisibility = 'cms-invisible';
            $advisibility = 'ad-visible';
    }


    return array($user_id,$tathusermenu, $oavisibility, $learnervisibility, $cmsvisibility, $advisibility, $gradvisibility,$user_group, $group_array);
}

function check_visibility() {

            ob_start(); ?>
            <ul>
            <li><?php echo $tathusermenu; ?></li>
            <li><?php echo $oavisibility; ?></li>
            <li><?php echo $learnervisibility; ?></li>
            <li><?php echo $cmsvisibility; ?></li>
            <li><?php echo $advisibility; ?></li>
            </ul>
            <?php
            return ob_get_clean();

}

add_shortcode('check-visibility','check_visibility');

function tath_role_class( $group )
{
$user_id = get_current_user_id();
  list($user_id,$tathusermenu, $oavisibility, $learnervisibility, $cmsvisibility, $advisibility, $gradvisibility) = tath_view_role_params($user_id);
switch( $group ) {
case 'oavisibility':
return $oavisibility;
break;
case 'learnervisibility':
return $learnervisibility;
break;
case 'cmsvisibility':
return $cmsvisibility;
break;
case 'advisibility':
return $advisibility;
break;
case 'gradvisibility':
return $gradvisibility;
break;
default:
return '';
}
}

// Add more time to process completing whole course for user in admin

add_action('sensei_user_lesson_start',function($user_id,$course_id) {

    if(!isset($_POST['add_complete_course']) || 'yes' != $_POST['add_complete_course']) {
        return;
    }

    set_time_limit(120);

},1,2);

add_action('sensei_user_course_end',function($user_id,$course_id) {

    if(!isset($_POST['add_complete_course']) || 'yes' != $_POST['add_complete_course']) {
        return;
    }

    set_time_limit(240);

},1,2);

add_filter('wpseo_enable_xml_sitemap_transient_caching',function() { return false; });

// Add custom form fields to complete course form

define('LEARNER_AJAX_TOKEN_NAME','tathata_learner_ajax');
define('LEARNER_AJAX_NONCE_TOKEN','`^7ENSzPR<O[g8TQGlz(qS?$<S3)|J-3fFTGBMEK ;-c&dFF]9f_sT(&=u:JJ+tJ'); // Generated by WP salt generator

function tathata_learner_ajax_nonce() {
    return wp_create_nonce(LEARNER_AJAX_NONCE_TOKEN);
}

function tathata_learner_ajax_abort($status = 404,$msg = null) {
    header('Status: '.$status);
    if($msg) {
        echo htmlentities($msg);
    }
    exit;
};

add_action('init',function() {
    add_action('wp_ajax_admin_complete_user_lesson',function() {
        check_ajax_referer(LEARNER_AJAX_NONCE_TOKEN,LEARNER_AJAX_TOKEN_NAME);

        // Allow only for logged in users in admin console
        if(!is_user_logged_in() || !is_admin()) {
            tathata_learner_ajax_abort();
        }

        $course_id = (isset($_POST['course_id'])&&preg_match('#^[0-9]+$#ismu',$_POST['course_id'])) ? (int) $_POST['course_id'] : 0;
        $lesson_id = (isset($_POST['lesson_id'])&&preg_match('#^[0-9]+$#ismu',$_POST['lesson_id'])) ? (int) $_POST['lesson_id'] : 0;
        $user_id = (isset($_POST['user_id'])&&preg_match('#^[0-9]+$#ismu',$_POST['user_id'])) ? (int) $_POST['user_id'] : 0;
        $last_lesson = isset($_POST['last_lesson']) ? intval($_POST['last_lesson']) : 0;

        // Missing lesson or user, bail
        if(!$lesson_id || !$user_id) {
            tathata_learner_ajax_abort();
        }

        // Complete the lesson
        ini_set( 'memory_limit', '400M' );
        $result = WooThemes_Sensei_Utils::sensei_start_lesson( $lesson_id, $user_id, true );

        // Update the course meta if last lesson in course
        if($result && $last_lesson) {
            WooThemes_Sensei_Utils::user_complete_course( $course_id, $user_id );
        }

        // Send response
        echo json_encode([
            'result' => $result,
            'message' => ($result) ? 'Completed' : 'Failed',
        ]);
        do_action( 'shutdown' );
        exit;
    });
});

add_action("admin_enqueue_scripts",function() {
    wp_enqueue_script(
        'tathata_course_completion',
        plugins_url('course-completion.js',__FILE__),
        ['jquery-core']
    );
});

add_action('sensei_learners_add_learner_form',function() {

    $course_id = (isset($_GET['course_id']) && preg_match('#^[0-9]+$#ismu',$_GET['course_id'])) ? intval($_GET['course_id']) : 0;

    // No course, bail
    if(!$course_id) {
        return;
    }

    global $woothemes_sensei;

    // Grab all of this Courses' lessons, looping through each...
    $lessons = $woothemes_sensei->post_types->course->course_lessons( $course_id );

    $data = [];
    foreach($lessons as $lesson) {
        $data[] = [
            'id' => $lesson->ID,
            'name' => $lesson->post_title,
        ];
    }

    usort($data,function($a,$b) {
        $ad = (float) preg_replace('#[^0-9\.]#ismu','',$a['name']);
        $bd = (float) preg_replace('#[^0-9\.]#ismu','',$b['name']);
        if($ad === $bd) { return 0; }
        return ($ad<$bd) ? -1 : 1;
    });

    echo '<div class="lesson-data" data-lessons="'.esc_attr(json_encode($data)).'" data-ajax-url="'.esc_attr(admin_url('admin-ajax.php')).'" '.
                'data-nonce="'.tathata_learner_ajax_nonce().'" data-token="'.LEARNER_AJAX_TOKEN_NAME.'"></div>';
});

// Add grads to completed group

add_action( 'sensei_user_course_end','tath_add_to_grad',10, 2);

// Add grads to completed group

function tath_add_to_grad($user_id ,$course_id) {

    $user_group = '7';

    if($course_id == '224') {
        Groups_User_Group::create( array( 'user_id' => $user_id, 'group_id' => $user_group ) );

        // Add course completion date to user meta
        update_user_meta($user_id,'course_completion_stamp',time());
    }

}

function certificate_url_60day( $course_id, $user_id ) {

        $certificate_url = '';

        $args = array(
            'post_type' => 'certificate',
            'author' => $user_id,
            'meta_key' => 'course_id',
            'meta_value' => $course_id
        );

        $query = new WP_Query( $args );
        if ( $query->have_posts() ) {

            $count = 0;
            while ($query->have_posts()) {

                $query->the_post();
                $certificate_url = get_permalink();

            } // End While Loop

        } // End If Statement

        wp_reset_postdata();

        return $certificate_url;

    } // End get_certificate_url()


    function cert_link_60day( $message ) {
        global $current_user, $course, $woothemes_sensei, $wp_query, $post;

        if( isset( $course->ID ) ) {
            $course_id = $course->ID;
        } else {
            $course_id = '224';
        }

        $certificate_template_id = get_post_meta( $course_id, '_course_certificate_template', true );

        if( ! $certificate_template_id ) return $message;

        $my_account_page_id = intval( $woothemes_sensei->settings->settings[ 'my_course_page' ] );
        $view_link_courses = $woothemes_sensei->settings->settings[ 'certificates_view_courses' ];
        $view_link_profile = $woothemes_sensei->settings->settings[ 'certificates_view_profile' ];
        $is_viewable = false;

        if ( ( is_page( '7036' ) || is_singular( 'course' ) || isset( $wp_query->query_vars['course_results'] ) ) && $view_link_courses ) {

            $is_viewable = true;

        } elseif( isset( $wp_query->query_vars['learner_profile'] ) && $view_link_profile ) {

            $is_viewable = true;

        } // End If Statement

        if ( $is_viewable ) {

            // Get User Meta
            get_currentuserinfo();


            if ( is_singular( 'course' ) ) {

                $certificate_url = certificate_url_60day( $post->ID, $current_user->ID );

            } else {

                $certificate_url = certificate_url_60day( '224', $current_user->ID );

            } // End If Statement

            if ( '' != $certificate_url ) {

                $classes = '';

                if ( is_page( $my_account_page_id ) || isset( $wp_query->query_vars['learner_profile'] ) ) {

                    $classes = 'button ';

                } // End If Statement

                $message = $message . '<a href="' . $certificate_url . '" class="final-congrats-a" title="' . esc_attr( __( 'View Certificate', 'sensei-certificates' ) ) . '" target="_blank">'. __( 'View Certificate', 'sensei-certificates' ) . '</a>';

            } // End If Statement

        } // End If Statement

        return $message;

    } // End certificate_link()



function get_cert_link() {
    global $woothemes_sensei, $post, $current_user, $course;
    $certlink = cert_link_60day();
    return $certlink;
}

add_shortcode('get-cert-link','get_cert_link');

// TODO: get this the heck out of here and into a custom WooCommerce plugin along with the other woo stuff spread all over the place.

// change the date format for the file name from YYYY_MM_DD_HH_SS to YYYY-MM-DD

function wc_xml_export_suite_edit_file_name( $post_replace_file_name, $pre_replace_file_name ) {

    // define your variables here - they can be entered in the WooCommerce > XML Export Suite > Settings tab
    $variables = array( '%%timestamp%%' );

    // define the replacement for each of the variables in the same order
    $replacement = array( date( 'Y_m_d' ) );

    // return the filename with the variables replaced
    return str_replace( $variables, $replacement, $pre_replace_file_name );
}
add_filter( 'wc_customer_order_xml_export_suite_export_file_name', 'wc_xml_export_suite_edit_file_name', 10, 2 );