<?php namespace TathataGolf\SharpSpring;
/*
Plugin Name: Tathata Sharpspring API Connector
Plugin URI: http://www.tathatagolfcertified.com/
Description: Provides functions for updating lead fields in sharpspring
Version: 0.0.2
Author: Factor1, James R. Latham
Author URI: http://www.factor1studios.com
*/

if(!function_exists('add_action')) {
    exit;
}

require_once('constants.php');
require_once('vendor/autoload.php');

spl_autoload_register([__NAMESPACE__.'\BaseController','__autoload']);

require_once('functions.php');

add_action('init',function() {
    $plugin = new PluginController();
});