<?php namespace TathataGolf\SharpSpring;

use \RecursiveDirectoryIterator;
use \RecursiveIteratorIterator;

abstract class BaseController {

    const PLUGIN_NAME = "tathata-sharpspring-connector";
    const PLUGIN_HANDLE_SLUG = "tgssapi_plugin";
    const CAPABILITY = "manage_options";

    /**
    *   Return input from get, post, or cookie
    *
    *   @return string|null
    */
    public function _input($k = null)
    {
        $input = stripslashes_deep(array_merge($_GET,$_POST,$_COOKIE));
        if(is_null($k)) {
            return $input;
        }
        if(array_key_exists($k,$input)) {
            return $input[$k];
        }
        return null;
    }

    /**
    *   Outputs content to browser
    *
    *   @return void
    */
    public function _output($s,$content_type ="text/html")
    {
        ob_end_clean();
        header("Connection: close\r\n");
        header("Content-Type: ".$content_type."; charset=UTF-8;\r\n");
        ob_start();
        echo $s;
        header("Content-Length: ".ob_get_length());
        ob_end_flush();
        flush();
        ob_end_clean();
    }

    /**
    *   Outputs json view to browser
    *
    *   @return string
    */
    public function json(array $a)
    {
        $this->_output(json_encode($a),"application/json");
    }

    /**
    *   Loads a view and returns the rendered content
    *
    *   @return string
    */
    public function _view($view,array $data = [])
    {
        $f = app_path().'/views/'.$view.'.php';
        if(file_exists($f)) {
            ob_start();
            extract($data);
            include($f);
            $view_output = ob_get_contents();
            ob_end_clean();
            return $view_output;
        }
        return '';
    }

    /**
    *   Aborts the current script
    *
    *   @return void
    */
    public function abort($status = 404,$msg = null)
    {
        header('Status: '.$status);
        if($msg) {
            echo htmlentities($msg);
        }
        if(env('APP_ENV')!=='testing') {
            exit;
        }
    }

    /**
    *   Returns the requested uri
    *
    *   @return string
    */
    public function uri($return_query = false)
    {
        $uri = (isset($_SERVER['HTTP_X_REWRITE_URL'])) ? $_SERVER['HTTP_X_REWRITE_URL'] : $_SERVER['REQUEST_URI'];
        return ($return_query!==true&&isset($_GET)&&$_GET) ? preg_replace("/\?(.*)$/is","",$uri) : $uri;
    }

    /**
    *   Returns a handle to use when registering stylesheets
    *
    *   @return string
    */
    protected function getCssHandle($path)
    {
        if(preg_match("#^(http(s)?:)?//#ismu",$path)) {
            $path = "ext_".basename($path);
        }
        return preg_replace("#_css$#ismu","",static::PLUGIN_HANDLE_SLUG."_".preg_replace("#[\-/\.]#ismu","_",$path));
    }

    /**
    *   Registers a stylesheet
    *
    *   @return void
    */
    protected function addCss($path,$admin = false,array $dependencies = [],$plugin_page_only = false)
    {
        $load = function($page) use($path,$admin,$dependencies,$plugin_page_only) {
            if($admin===true&&$plugin_page_only===true) {
                if(!preg_match("#_".preg_quote(static::PLUGIN_HANDLE_SLUG)."#ismu",$page)) {
                    return;
                }
            }
            $url = (preg_match("#^http(s)?://#ismu",$path)) ? $path : plugin_url($path);
            wp_enqueue_style(
                $this->getCssHandle($path),
                $url,
                $dependencies
            );
        };
        if($admin===true) {
            add_action("admin_enqueue_scripts",$load);
        } else {
            $load('');
        }
    }

    /**
    *   Return a handle to use when registering scripts
    *
    *   @return string
    */
    protected function getScriptHandle($path)
    {
        if(preg_match("#^(http(s)?:)?//#ismu",$path)) {
            $path = "ext_".basename($path);
        }
        return preg_replace("#_js$#ismu","",static::PLUGIN_HANDLE_SLUG."_".preg_replace("#[\-/\.]#ismu","_",$path));
    }

    /**
    *   Retgister a script
    *
    *   @return void
    */
    protected function addScript($path,$admin = false,array $dependencies = [],$plugin_page_only = false)
    {
        $load = function($page) use($path,$admin,$dependencies,$plugin_page_only) {
            if($admin===true&&$plugin_page_only===true) {
                if(!preg_match("#_".preg_quote(static::PLUGIN_HANDLE_SLUG)."#ismu",$page)) {
                    return;
                }
            }
            $url = (preg_match("#^http(s)?://#ismu",$path)) ? $path : plugin_url($path);
            wp_enqueue_script(
                $this->getScriptHandle($path),
                $url,
                $dependencies
            );
        };
        if($admin===true) {
            add_action("admin_enqueue_scripts",$load);
        } else {
            $load('');
        }
    }

    /**
    *   Autoloads app classes
    *
    *   @return void
    */
    public static function __autoload($cls)
    {
        if(!preg_match('#^'.preg_quote(__NAMESPACE__).'#ismu',$cls)) {
            return;
        }

        if(class_exists($cls,false)) { return; }
        $segments = explode("\\",$cls);
        $classname = array_pop($segments);

        $dir_iterator = new RecursiveDirectoryIterator(app_path());
        $iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);

        foreach($iterator as $path) {
            if(!$path->isFile()) {
                continue;
            }
            $pathname = $path->getPathName();
            $filename = basename($pathname);
            if(!preg_match("#^".preg_quote($classname)."\.php$#",$filename)) {
                continue;
            }
            $pathname = str_replace("\\","/",$pathname);
            require_once($pathname);

            if(class_exists($cls,false)) { return; }
        }
    }

}