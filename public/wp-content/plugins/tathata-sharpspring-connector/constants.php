<?php namespace TathataGolf\SharpSpring;

if(!defined('TGSSAPI_VERSION')) {
    define('TGSSAPI_VERSION','0.0.2');
}
if(!defined('TGSSAPI_PLUGIN_FILE')) {
    define("TGSSAPI_PLUGIN_FILE",str_replace("\\","/",realpath(__FILE__)));
}
if(!defined('TGSSAPI_PLUGIN_DIR')) {
    define("TGSSAPI_PLUGIN_DIR",str_replace("\\","/",realpath(dirname(TGSSAPI_PLUGIN_FILE))));
}
if(!defined('TGSSAPI_PLUGIN_PREFIX')) {
    define('TGSSAPI_PLUGIN_PREFIX','TGSSAPI_');
}