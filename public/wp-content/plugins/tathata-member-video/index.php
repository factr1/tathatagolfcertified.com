<?php 

/*
Plugin Name: Tathata Member Video Submission
Plugin URI: http://tathata.com
Description: A fork of the N-Media front end plugin to handle front end member video submissions.
Version: 1.0
Author: Chris Jenkins
Author URI: http://www.tathata.com/
Text Domain: nm-postfront
*/


/*
 * Lets start from here
*/

/*
 * loading plugin config file
 */
$_config = dirname(__FILE__).'/config.php';
if( file_exists($_config))
	include_once($_config);
else
	die('Error, file not found '.$_config);


/* ======= the plugin main class =========== */
$_plugin = dirname(__FILE__).'/classes/plugin.class.php';
if( file_exists($_plugin))
	include_once($_plugin);
else
	die('Error, file not found '.$_plugin);

/*
 * [1]
 * TODO: just replace class name with your plugin
 */
$postfront = new NM_PLUGIN_PostFront();


if( is_admin() ){

	$_admin = dirname(__FILE__).'/classes/admin.class.php';
	if( file_exists($_admin))
		include_once($_admin );
	else
		die('Error, file not found '.$_admin);

	$postfront_admin = new NM_PLUGIN_PostFront_Admin();
}

/*
 * activation/install the plugin data
*/
register_activation_hook( __FILE__, array('NM_PLUGIN_PostFront', 'activate_plugin'));
register_deactivation_hook( __FILE__, array('NM_PLUGIN_PostFront', 'deactivate_plugin'));


