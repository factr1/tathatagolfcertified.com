<?php
/*
 * sample cotact form template file
 */

	global $postfront;
	
	$upload_runtime = $postfront -> get_runtime();
	$wp_editor_settings = array(
		'wpautop' => false,
		'textarea_rows' => 1,
		'tinymce' => false,
		'quicktags' => false,
	);

/*
 * Validate Media Buttons
 */

	if($postfront -> get_option("_media_upload") == 'yes'){
		$wp_editor_settings['media_buttons'] = true;
	}else{
		$wp_editor_settings['media_buttons'] = false;
	}

/*
 * Validate Richtext Editor
 */

	if($postfront -> get_option("_rich_editor") == 'yes'){
		$wp_editor_settings['tinymce'] = true;
	}else{
		$wp_editor_settings['tinymce'] = false;
	}



$post_content = '';

/*
 * Validate Icons
 */

	if($postfront -> get_option("_show_icons") == 'yes'){
		echo '<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">';
	}
?>

<style>
<?php
/** loading styles 
 *  post-frontend-styles.css
 */
	$postfront -> load_template('post-frontend-style.css');

?>
	#select-button {
		font-size: <?php echo $postfront -> get_option("_feature_btn_fontsize"); ?>px;
		color: <?php echo $postfront -> get_option("_feature_btn_color"); ?>;
		background-color: <?php echo $postfront -> get_option("_feature_btn_bgcolor"); ?>;
	}
	#publish-button {
		font-size: <?php echo $postfront -> get_option("_publish_btn_fontsize"); ?>px !important;
		color: <?php echo $postfront -> get_option("_publish_btn_color"); ?> !important;
		background-color: <?php echo $postfront -> get_option("_publish_btn_bgcolor"); ?> !important;	
	}
	#form-main-area {
		background-color: <?php echo $postfront -> get_option("_form_background_color"); ?>;
	}

/*
 * Custom Styles
 */

	<?php echo $postfront -> get_option("_custom_styles"); ?>
 </style>
 

<div id="form-buttons-bar" class="clearfix">
</div>
		
<!-- the html meat -->
 <form id="nm-post-front-form">
 <input type="hidden" name="featured_image_uploaded" id="featured_image_uploaded" />

	<div id="form-main-area" class="container">
		
		
		<label class="heading">Title</label>
		<div class="clearfix"></div>

		<input maxlength="95" name="post_title" id="post_title" type="text" value="">
		<div class="clearfix"></div>

		<label class="heading">Video URL</label>
		<div class="clearfix"></div>
		<p><em>Paste the URL provided in the share tab of your YouTube video. Example: http://youtu.be/QK8mJJJvaes</em></p>
		<div class="clearfix"></div>
		<?php wp_editor($post_content, 'post_description', $wp_editor_settings );
			//wp_editor('','post_description');
			?>
		<div class="clearfix"></div>

<?php if($postfront -> get_option("_show_taxonomy") == 'yes'){ 
$exclude_taxonomy = explode(',', $postfront -> get_option('_exclude_taxonomy'));
	?>
		
		
		

		<?php
			$posttype = $postfront -> get_option('_post_type');
			$taxonomy_names = get_object_taxonomies( $posttype );
   			foreach($taxonomy_names as $taxonomy){

   				if(!in_array($taxonomy, $exclude_taxonomy)){

   					echo '<label class="heading">Select '.$taxonomy.'</label>';
					echo '<div class="clearfix"></div>';

					$terms_args = array( 'hide_empty'        => false, 
						'parent'      => 0,
						'exclude'	=> $postfront -> get_option('_exclude_term') );
					$terms = get_terms($taxonomy, $terms_args);
					if($terms){

						echo '<ul class="tax-styles">';
						foreach ($terms as $term) {
							
							render_term_tree($term->term_id, $taxonomy);					
						}
						echo "</ul>";
					}	
   				}
				

   			}		
   		?>
		
<?php } ?>

<?php if($postfront -> get_option("_featured_image") == 'yes'){ ?>
		
		<label class="heading">Featured Image</label>
		<div class="clearfix"></div>
		<a href="javascript:;" id="select-button" class="form-button"><i class="fa fa-upload"></i>
			<?php echo ($postfront -> get_option("_feature_btn_label")) ? $postfront -> get_option("_feature_btn_label") : 'Upload Image' ; ?>
		</a>
		<div id="filelist-featuredimage" class="filelist"></div>
		
<?php } ?>
		
		<div id="form-buttons-bar" class="clearfix">
			<button type="submit" id="publish-button" class="form-button form-button-publish">
				<i class="fa fa-floppy-o"></i>
				<?php echo ($postfront -> get_option("_publish_btn_label")) ? $postfront -> get_option("_publish_btn_label") : 'Publish' ; ?>
			</button>
			<p style="text-align:center" id="saving-response"></p> 
		</div>
		
	</div>
	</form>
	<!-- Main Container ENDS -->


<script>
	<!--
	var file_count_featuredimage = 1;
	var uploader_featuredimage;
	
	jQuery(document).ready(function($) {

    // file uploader script
    var $filelist_DIV = $('#filelist-featuredimage');
    uploader_featuredimage = new plupload.Uploader({
		runtimes 			: '<?php echo $upload_runtime?>',
		browse_button 		: 'select-button', // you can pass in id...
		container			: 'form-main-area', // ... or DOM Element itself
		drop_element		: 'form-main-area',
		url 				: '<?php echo network_site_url(get_blog_details( get_current_blog_id() )->domain . 'wp-admin/admin-ajax.php', 'https'); ?>',
		multipart_params 	: {'action' : 'nm_postfront_upload_file'},
		max_file_size 		: '1000mb',
		max_file_count 		: 5,
	    
	    chunk_size: '2mb',
		
	    // Flash settings
		flash_swf_url 		: '<?php echo $postfront -> plugin_meta['url']?>js/plupload-2.1.2/js/Moxie.swf',
		// Silverlight settings
		silverlight_xap_url : '<?php echo $postfront -> plugin_meta['url']?>js/plupload-2.1.2/js/Moxie.xap',
		
		filters : {
			mime_types: [
				{title : "Image files", extensions : "jpg,png,gif"}
			]
		},
		
		init: {
			PostInit: function() {
				$filelist_DIV.html('');

				$('#uploadfiles-featuredimage').bind('click', function() {
					uploader_featuredimage.start();
					return false;
				});
			},

			FilesAdded: function(up, files) {

				var files_added = up.files.length;
				var max_count_error = false;

				
			    plupload.each(files, function (file) {

			    	if(file_count_featuredimage > uploader_featuredimage.settings.max_file_count){
			        
			      		max_count_error = true;
			        }else{
			            // Code to add pending file details, if you want
			            add_thumb_box(file, $filelist_DIV, up);
			        }
			        
			        file_count_featuredimage++;
			    });

			    
			    if(max_count_error)
				    alert(uploader_featuredimage.settings.max_file_count + nm_postfront_vars.mesage_max_files_limit);

			    setTimeout('uploader_featuredimage.start()', 100);    
				
			},
			
			FileUploaded: function(up, file, info){
				
				/* console.log(up);
				console.log(file);*/

				var obj_resp = $.parseJSON(info.response);
				//console.log(obj_resp);
				var file_thumb 	= ''; 

			
				// checking if uploaded file is thumb
				ext = obj_resp.file_name.substring(obj_resp.file_name.lastIndexOf('.') + 1);					
				ext = ext.toLowerCase();
				
				if(ext == 'png' || ext == 'gif' || ext == 'jpg' || ext == 'jpeg'){

					var thumb_count = 0;
					$.each(obj_resp.thumb_meta, function(i, item){

						if(thumb_count === 0){
							
							file_thumb = nm_postfront_vars.file_upload_path_thumb + item.name;
							$filelist_DIV.find('#u_i_c_' + file.id).find('.u_i_c_thumb').append('<img width="100" src="'+file_thumb+ '" id="thumb_'+file.id+'" />');
						}

						thumb_count++;
					});
					//$filelist_DIV.find('#u_i_c_' + file.id).find('.u_i_c_thumb').append('<input type="hidden" name="imagemeta_'+file.id+'" value="'+JSON.stringify(obj_resp.thumb_meta)+'" >');
					
					var file_full 	= nm_postfront_vars.file_upload_path + obj_resp.file_name;
					// thumb thickbox only shown if it is image
					$filelist_DIV.find('#u_i_c_' + file.id).find('.u_i_c_thumb').append('<div style="display:none" id="u_i_c_big' + file.id + '"><img src="'+file_full+ '" /></div>');
					
					//file name	
					$filelist_DIV.find('#u_i_c_' + file.id).find('.progress_bar').html(obj_resp.file_name);
					// zoom effect
					//$filelist_DIV.find('#u_i_c_' + file.id).find('.u_i_c_tools_zoom').append('<a href="#TB_inline?width=500&height=500&inlineId=u_i_c_big'+file.id+'" class="thickbox" title="'+obj_resp.file_name+'"><img width="15" src="'+nm_postfront_vars.plugin_url+'/images/zoom.png" /></a>');
					is_image = true;
				}else{
					file_thumb = nm_postfront_vars.plugin_url+'/images/file.png';
					$filelist_DIV.find('#u_i_c_' + file.id).find('.u_i_c_thumb').html('<img src="'+file_thumb+ '" id="thumb_'+file.id+'" />');
					is_image = false;
				}
				
				// adding checkbox input to Hold uploaded file name as array
				//$filelist_DIV.append('<input style="display:none" checked="checked" type="checkbox" value="'+obj_resp.file_name+'" name="featuredimage['+file.id+']" />');
				$("#featured_image_uploaded").val(obj_resp.file_name);
			},

			UploadProgress: function(up, file) {
				//document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
				//console.log($filelist_DIV.find('#' + file.id).find('.progress_bar_runner'));
				$filelist_DIV.find('#u_i_c_' + file.id).find('.progress_bar_number').html(file.percent + '%');
				$filelist_DIV.find('#u_i_c_' + file.id).find('.progress_bar_runner').css({'display':'block', 'width':file.percent + '%'});
			},

			Error: function(up, err) {
				//document.getElementById('console').innerHTML += "\nError #" + err.code + ": " + err.message;
				alert("\nError #" + err.code + ": " + err.message);
			}
		}
		

	});

    uploader_featuredimage.init();
    

		
});


	
	function add_thumb_box(file, $filelist_DIV){

		var inner_html 	= '<table style="display:noned;width:100%;margin-bottom: 20px;" data-fileid="'+file.id+'">';
		
		inner_html		+= '<tr style="background: #ccc;">';
		// inner_html		+= '<td style="text-align:center;height: 25px;vertical-align: middle;"><input type="radio" name="set_featured" value="'+file.id+'">';
		// inner_html		+= 'Set featured?<br />';
		inner_html		+= '<div class="progress_bar"><span class="progress_bar_runner"></span><span class="progress_bar_number">(' + plupload.formatSize(file.size) + ')<span></div>';
		inner_html		+= '</td></tr>';
		
		inner_html		+= '<tr><td style="vertical-align: top;"><div class="u_i_c_thumb"></div></td>';
		inner_html		+= '</tr>';
		inner_html		+= '</table>';
		  
		jQuery( '<div />', {
			'id'	: 'u_i_c_'+file.id,
			'class'	: 'u_i_c_box',
			'html'	: inner_html,
			
		}).appendTo($filelist_DIV);

		// clearfix
		// 1- removing last clearfix first
		$filelist_DIV.find('.u_i_c_box_clearfix').remove();
		
		jQuery( '<div />', {
			'class'	: 'u_i_c_box_clearfix',				
		}).appendTo($filelist_DIV);
	}
	//-->
</script>