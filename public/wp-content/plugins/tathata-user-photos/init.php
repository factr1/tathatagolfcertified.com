<?php
/**
 * Plugin Name: Tathata User Photos
 * Plugin URI:  http://wordpress.org/extend/tathata-user-photos
 * Description: ** A fork of Basic User Avatars for Tathata. ** Adds an avatar upload field to user profiles. Also provides front-end avatar management via a shortcode and bbPress support. No frills. Fork of Simple Local Avatars 1.3.
 * Version:     1.0.0
 * Author:      Jared Atchison
 * Author URI:  http://jaredatchison.com
 *
 * ----------------------------------------------------------------------------//
 * This plugin is a fork of Simple Local Avatars v1.3.1 by Jake Goldman (10up).
 *
 * Orignal author url:  http://get10up.com
 * Original plugin url: http://wordpress.org/plugins/simple-local-avatars
 *
 * If you want some snazzy ajax and some other nifty features, check out Simple
 * Local Avatars 2.0+
 * ----------------------------------------------------------------------------//
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * @author     Jared Atchison
 * @version    1.0.0
 * @package    JA_BasicLocalAvatars
 * @copyright  Copyright (c) 2013, Jared Atchison
 * @link       http://jaredatchison.com
 * @license    http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 */

class tathata_user_photos {

	private $user_id_being_edited;

	/**
	 * Initialize all the things
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		// Actions
		add_action( 'admin_init',                array( $this, 'admin_init'               )        );
		add_action( 'show_user_profile',         array( $this, 'edit_user_profile'        )        );
		add_action( 'edit_user_profile',         array( $this, 'edit_user_profile'        )        );
		add_action( 'personal_options_update',   array( $this, 'edit_user_profile_update' )        );
		add_action( 'edit_user_profile_update',  array( $this, 'edit_user_profile_update' )        );
		add_action( 'bbp_user_edit_after_name', array( $this, 'bbpress_user_profile'     )        );

		// Shortcode
		add_shortcode( 'tathata-user-photos',     array( $this, 'shortcode'                )        );

		// Filters
		add_filter( 'get_avatar',                array( $this, 'get_avatar'               ), 10, 5 );
		add_filter( 'avatar_defaults',           array( $this, 'avatar_defaults'          )        );

	}

	/**
	 * Start the admin engine.
	 *
	 * @since 1.0.0
	 */
	public function admin_init() {

		// Load the textdomain so we can support other languages
		load_plugin_textdomain( 'tathata-user-photos', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );

		// Register/add the Discussion setting to restrict avatar upload capabilites
		register_setting( 'discussion', 'tathata_user_photos_caps', array( $this, 'sanitize_options' ) );
		add_settings_field( 'tathata-user-photos-caps', __( 'User Photo Permissions', 'tathata-user-photos' ), array( $this, 'avatar_settings_field' ), 'discussion', 'avatars' );
	}

	/**
	 * Discussion settings option
	 *
	 * @since 1.0.0
	 * @param array $args [description]
	 */
	public function avatar_settings_field( $args ) {
		$options = get_option( 'tathata_user_photos_caps' );
		?>
		<label for="tathata_user_photos_caps">
			<input type="checkbox" name="tathata_user_photos_caps" id="tathata_user_photos_caps" value="1" <?php checked( $options['tathata_user_photos_caps'], 1 ); ?>/>
			<?php _e( 'Only allow users with file upload capabilities to upload local avatars (Authors and above)', 'tathata-user-photos' ); ?>
		</label>
		<?php
	}

	/**
	 * Sanitize the Discussion settings option
	 *
	 * @since 1.0.0
	 * @param array $input
	 * @return array
	 */
	public function sanitize_options( $input ) {
		$new_input['tathata_user_photos_caps'] = empty( $input['tathata_user_photos_caps'] ) ? 0 : 1;
		return $new_input;
	}

	/**
	 * Filter the avatar WordPress returns
	 *
	 * @since 1.0.0
	 * @param string $avatar 
	 * @param int/string/object $id_or_email
	 * @param int $size 
	 * @param string $default
	 * @param boolean $alt 
	 * @return string
	 */
	public function get_avatar( $avatar = '', $id_or_email, $size = 230, $default = '', $alt = false ) {

		// Determine if we recive an ID or string
		if ( is_numeric( $id_or_email ) )
			$user_id = (int) $id_or_email;
		elseif ( is_string( $id_or_email ) && ( $user = get_user_by( 'email', $id_or_email ) ) )
			$user_id = $user->ID;
		elseif ( is_object( $id_or_email ) && ! empty( $id_or_email->user_id ) )
			$user_id = (int) $id_or_email->user_id;

		if ( empty( $user_id ) )
			return $avatar;

		$local_avatars = get_user_meta( $user_id, 'tathata_user_photo', true );

		if ( empty( $local_avatars ) || empty( $local_avatars['full'] ) )
			return $avatar;

		$size = (int) $size;

		if ( empty( $alt ) )
			$alt = get_the_author_meta( 'display_name', $user_id );

		// Generate a new size
		if ( empty( $local_avatars[$size] ) ) {

			$upload_path      = wp_upload_dir();
			$avatar_full_path = str_replace( $upload_path['baseurl'], $upload_path['basedir'], $local_avatars['full'] );
			$image            = wp_get_image_editor( $avatar_full_path );

			if ( ! is_wp_error( $image ) ) {
				$image->resize( $size, $size, true );
				$image_sized = $image->save();
			}

			// Deal with original being >= to original image (or lack of sizing ability)
			$local_avatars[$size] = is_wp_error( $image_sized ) ? $local_avatars[$size] = $local_avatars['full'] : str_replace( $upload_path['basedir'], $upload_path['baseurl'], $image_sized['path'] );

			// Save updated avatar sizes
			update_user_meta( $user_id, 'tathata_user_photo', $local_avatars );

		} elseif ( substr( $local_avatars[$size], 0, 4 ) != 'http' ) {
			$local_avatars[$size] = home_url( $local_avatars[$size] );
		}

		$author_class = is_author( $user_id ) ? ' current-author' : '' ;
		$avatar       = "<img alt='" . esc_attr( $alt ) . "' src='" . $local_avatars[$size] . "' class='avatar avatar-{$size}{$author_class} photo' height='{$size}' width='{$size}' />";

		return apply_filters( 'tathata_user_photo', $avatar );
	}

	/**
	 * Form to display on the user profile edit screen
	 *
	 * @since 1.0.0
	 * @param object $profileuser
	 * @return
	 */
	public function edit_user_profile( $profileuser ) {

		// bbPress will try to auto-add this to user profiles - don't let it.
		// Instead we hook our own proper function that displays cleaner.
		if ( function_exists( 'is_bbpress') && is_bbpress() )
			return;
		?>

		<h3><?php _e( 'Avatar', 'tathata-user-photos' ); ?></h3>
		<table class="form-table">
			<tr>
				<th><label for="tathata-user-photo"><?php _e( 'Upload Avatar', 'tathata-user-photos' ); ?></label></th>
				<td style="width: 50px;" valign="top">
					<?php echo get_avatar( $profileuser->ID ); ?>
				</td>
				<td>
				<?php
				$options = get_option( 'tathata_user_photos_caps' );
				if ( empty( $options['tathata_user_photos_caps'] ) || current_user_can( 'upload_files' ) ) {
					// Nonce security ftw
					wp_nonce_field( 'tathata_user_photo_nonce', '_tathata_user_photo_nonce', false );
					
					// File upload input
					echo '<input type="file" name="tathata-user-photo" id="basic-local-avatar" /><br />';

					if ( empty( $profileuser->tathata_user_photo ) ) {
						echo '<span class="description">' . __( 'No profile photo is set. Use the upload field to add a profile photo.', 'tathata-user-photos' ) . '</span>';
					} else {
						echo '<input type="checkbox" name="tathata-user-photo-erase" value="1" /> ' . __( 'Delete current photo', 'tathata-user-photos' ) . '<br />';
						echo '<span class="description">' . __( 'Replace the current profile photo by uploading a new image, or by checking the delete option.', 'tathata-user-photos' ) . '</span>';
					}

				} else {
					if ( empty( $profileuser->tathata_user_photo ) ) {
						echo '<span class="description">' . __( 'No profile photo is set. Use the upload field to add a profile photo.', 'tathata-user-photos' ) . '</span>';
					} else {
						echo '<span class="description">' . __( 'You do not have media management permissions. Please contact the site administrator.', 'tathata-user-photos' ) . '</span>';
					}	
				}
				?>
				</td>
			</tr>
		</table>
		<script type="text/javascript">var form = document.getElementById('your-profile');form.encoding = 'multipart/form-data';form.setAttribute('enctype', 'multipart/form-data');</script>
		<?php
	}

	/**
	 * Update the user's avatar setting
	 *
	 * @since 1.0.0
	 * @param int $user_id
	 */
	public function edit_user_profile_update( $user_id ) {

		// Check for nonce otherwise bail
		if ( ! isset( $_POST['_tathata_user_photo_nonce'] ) || ! wp_verify_nonce( $_POST['_tathata_user_photo_nonce'], 'tathata_user_photo_nonce' ) )
			return;

		if ( ! empty( $_FILES['tathata-user-photo']['name'] ) ) {

			// Allowed file extensions/types
			$mimes = array(
				'jpg|jpeg|jpe' => 'image/jpeg',
				'gif'          => 'image/gif',
				'png'          => 'image/png',
			);

			// Front end support - shortcode, bbPress, etc
			if ( ! function_exists( 'wp_handle_upload' ) )
				require_once ABSPATH . 'wp-admin/includes/file.php';

			// Delete old images if successful
			$this->avatar_delete( $user_id );

			// Need to be more secure since low privelege users can upload
			if ( strstr( $_FILES['tathata-user-photo']['name'], '.php' ) )
				wp_die( 'For security reasons, the extension ".php" cannot be in your file name.' );

			// Make user_id known to unique_filename_callback function
			$this->user_id_being_edited = $user_id; 
			$avatar = wp_handle_upload( $_FILES['tathata-user-photo'], array( 'mimes' => $mimes, 'test_form' => false, 'unique_filename_callback' => array( $this, 'unique_filename_callback' ) ) );

			// Handle failures
			if ( empty( $avatar['file'] ) ) {  
				switch ( $avatar['error'] ) {
				case 'File type does not meet security guidelines. Try another.' :
					add_action( 'user_profile_update_errors', create_function( '$a', '$a->add("avatar_error",__("Please upload a valid image file.","tathata-user-photos"));' ) );
					break;
				default :
					add_action( 'user_profile_update_errors', create_function( '$a', '$a->add("avatar_error","<strong>".__("There was an error uploading the image:","tathata-user-photos")."</strong> ' . esc_attr( $avatar['error'] ) . '");' ) );
				}
				return;
			}

			// Save user information (overwriting previous)
			update_user_meta( $user_id, 'tathata_user_photo', array( 'full' => $avatar['url'] ) );

		} elseif ( ! empty( $_POST['tathata-user-photo-erase'] ) ) {
			// Nuke the current avatar
			$this->avatar_delete( $user_id );
		}
	}

	/**
	 * Enable avatar management on the frontend via this shortocde.
	 *
	 * @since 1.0.0
	 */
	function shortcode() {

		// Don't bother if the user isn't logged in
		if ( ! is_user_logged_in() )
			return;

		$user_id     = get_current_user_id();
		$profileuser = get_userdata( $user_id );

		if ( isset( $_POST['manage_avatar_submit'] ) ){
			$this->edit_user_profile_update( $user_id );
		}

		ob_start();
		?>
		<form id="tathata-user-photo-form" action="<?php the_permalink(); ?>" method="post" enctype="multipart/form-data">
			<?php
			echo get_avatar( $profileuser->ID );

			$options = get_option( 'tathata_user_photos_caps' );
			if ( empty( $options['tathata_user_photos_caps'] ) || current_user_can( 'upload_files' ) ) {
				// Nonce security ftw
				wp_nonce_field( 'tathata_user_photo_nonce', '_tathata_user_photo_nonce', false );
				
				// File upload input
				echo '<p><input type="file" name="tathata-user-photo" id="basic-local-avatar" /></p>';

				if ( empty( $profileuser->tathata_user_photo ) ) {
					echo '<p class="description">' . __( 'No profile photo is set. Use the upload field to add a profile photo.', 'tathata-user-photos' ) . '</p>';
				} else {
					echo '<input type="checkbox" name="tathata-user-photo-erase" value="1" /> ' . __( 'Delete image', 'tathata-user-photos' ) . '<br />';
					echo '<p class="description">' . __( 'Replace the current profile photo by uploading a new image, or by checking the delete option.', 'tathata-user-photos' ) . '</p>';
				}

			} else {
				if ( empty( $profileuser->tathata_user_photo ) ) {
					echo '<p class="description">' . __( 'No profile photo is set. Use the upload field to add a profile photo.', 'tathata-user-photos' ) . '</p>';
				} else {
					echo '<p class="description">' . __( 'You do not have media management permissions. Please contact the site administrator.', 'tathata-user-photos' ) . '</p>';
				}	
			}
			?>
			<input type="submit" name="manage_avatar_submit" value="Update Avatar" />
		</form>
		<?php
		return ob_get_clean();
	}

	/**
	 * Form to display on the bbPress user profile edit screen
	 *
	 * @since 1.0.0
	 */
	public function bbpress_user_profile() {

		if ( !bbp_is_user_home_edit() )
			return;

		$user_id     = get_current_user_id();
		$profileuser = get_userdata( $user_id );

		echo '<div>';
			echo '<label for="basic-local-avatar">' . __( 'Profile photo', 'tathata-user-photos' ) . '</label>';
 			echo '<fieldset class="bbp-form avatar">';

	 			echo get_avatar( $profileuser->ID );
				$options = get_option( 'tathata_user_photos_caps' );
				if ( empty( $options['tathata_user_photos_caps'] ) || current_user_can( 'upload_files' ) ) {
					// Nonce security ftw
					wp_nonce_field( 'tathata_user_photo_nonce', '_tathata_user_photo_nonce', false );
					
					// File upload input
					echo '<br /><input type="file" name="tathata-user-photo" id="basic-local-avatar" /><br />';

					if ( empty( $profileuser->tathata_user_photo ) ) {
						echo '<span class="description" style="margin-left:0;">' . __( 'No profile photo is set. Use the upload field to add a profile photo.', 'tathata-user-photos' ) . '</span>';
					} else {
						echo '<input type="checkbox" name="tathata-user-photo-erase" value="1" style="width:auto" /> ' . __( 'Delete image', 'tathata-user-photos' ) . '<br />';
						echo '<span class="description" style="margin-left:0;">' . __( 'Replace the current profile photo by uploading a new image, or by checking the delete option.', 'tathata-user-photos' ) . '</span>';
					}

				} else {
					if ( empty( $profileuser->tathata_user_photo ) ) {
						echo '<span class="description" style="margin-left:0;">' . __( 'No profile photo is set. Use the upload field to add a profile photo.', 'tathata-user-photos' ) . '</span>';
					} else {
						echo '<span class="description" style="margin-left:0;">' . __( 'You do not have media management permissions. Please contact the site administrator.', 'tathata-user-photos' ) . '</span>';
					}	
				}

			echo '</fieldset>';
		echo '</div>';
		?>
		<script type="text/javascript">var form = document.getElementById('bbp-your-profile');form.encoding = 'multipart/form-data';form.setAttribute('enctype', 'multipart/form-data');</script>
		<?php
	}

	/**
	 * Remove the custom get_avatar hook for the default avatar list output on 
	 * the Discussion Settings page.
	 *
	 * @since 1.0.0
	 * @param array $avatar_defaults
	 * @return array
	 */
	public function avatar_defaults( $avatar_defaults ) {
		remove_action( 'get_avatar', array( $this, 'get_avatar' ) );
		return $avatar_defaults;
	}

	/**
	 * Delete avatars based on user_id
	 *
	 * @since 1.0.0
	 * @param int $user_id
	 */
	public function avatar_delete( $user_id ) {
		$old_avatars = get_user_meta( $user_id, 'tathata_user_photo', true );
		$upload_path = wp_upload_dir();

		if ( is_array( $old_avatars ) ) {
			foreach ( $old_avatars as $old_avatar ) {
				$old_avatar_path = str_replace( $upload_path['baseurl'], $upload_path['basedir'], $old_avatar );
				@unlink( $old_avatar_path );
			}
		}

		delete_user_meta( $user_id, 'tathata_user_photo' );
	}

	/**
	 * File names are magic
	 *
	 * @since 1.0.0
	 * @param string $dir
	 * @param string $name
	 * @param string $ext
	 * @return string
	 */
	public function unique_filename_callback( $dir, $name, $ext ) {
		$user = get_user_by( 'id', (int) $this->user_id_being_edited );
		$name = $base_name = sanitize_file_name( $user->display_name . '_avatar' );
		$number = 1;

		while ( file_exists( $dir . "/$name$ext" ) ) {
			$name = $base_name . '_' . $number;
			$number++;
		}

		return $name . $ext;
	}
}
$tathata_user_photos = new tathata_user_photos;

/**
 * During uninstallation, remove the custom field from the users and delete the local avatars
 *
 * @since 1.0.0
 */
function tathata_user_photos_uninstall() {
	$tathata_user_photos = new tathata_user_photos;
	$users = get_users_of_blog();

	foreach ( $users as $user )
		$tathata_user_photos->avatar_delete( $user->user_id );

	delete_option( 'tathata_user_photos_caps' );
}
register_uninstall_hook( __FILE__, 'tathata_user_photos_uninstall' );