<?php

if(!class_exists('WooThemes_Sensei_Frontend')) {
    return;
}

class WooThemes_Sensei_Frontend_Custom extends WooThemes_Sensei_Frontend {

    public $token;
    public $course;
    public $lesson;
    public $messages;
    public $data;

    /**
     * Constructor.
     * @since  1.0.0
     * @return  void
     */
    public function __construct()
    {
        add_action('sensei_course_archive_course_custom_title', array($this, 'sensei_course_archive_course_custom_title'), 10, 1);
        add_action('sensei_lesson_quiz_meta_custom', array($this, 'sensei_lesson_quiz_meta_custom'), 10, 2);
        add_action('sensei_quiz_questions_custom', array($this, 'quiz_questions_custom'), 10);
        add_action('sensei_quiz_question_type_custom', array($this, 'quiz_question_type_custom'), 10, 1);
    }

    public function quiz_questions_custom($return = false)
    {
        include(ABSPATH . 'wp-content/plugins/sensei-custom/templates/single-quiz/quiz-questions.php');
    }

    public function quiz_question_type_custom($question_type = 'multiple-choice')
    {
        global $woothemes_sensei;
        include(ABSPATH . 'wp-content/plugins/sensei-custom/templates/single-quiz/question_type-' . $question_type . '.php');
        return $content;
    }

    function sensei_course_archive_course_custom_title($post_item)
    {
        if (isset($post_item->ID) && ( 0 < $post_item->ID )) {
            $post_id = absint($post_item->ID);
            $post_title = $post_item->post_title;
        } else {
            $post_id = get_the_ID();
            $post_title = get_the_title();
        }
        ?>
        <header>
            <h2>
                <a href="<?php print site_url(); ?>/course-chapters/<?php echo $post_id; ?>" title="<?php echo esc_attr($post_title); ?>">
                    <?php echo $post_title; ?>
                </a>
            </h2>
        </header>
        <?php
    }

    function sensei_lesson_quiz_meta_custom($post_id = 0, $user_id = 0)
    {
        global $woothemes_sensei, $quiz_id;

        // Get the prerequisite lesson
        $lesson_prerequisite = get_post_meta($post_id, '_lesson_prerequisite', true);
        $lesson_course_id = get_post_meta($post_id, '_lesson_course', true);

        // Lesson Quiz Meta
        $lesson_quizzes = $this->lesson_quizzes($post_id);
        foreach ($lesson_quizzes as $quiz_item) {
            $quiz_id = $quiz_item->ID;
        }
        $has_user_completed_lesson = WooThemes_Sensei_Utils::user_completed_lesson($post_id, $user_id);
        $show_actions = true;

        if (intval($lesson_prerequisite) > 0) {

            // Lesson Quiz Meta
            $prereq_lesson_quizzes = $woothemes_sensei->frontend->lesson->lesson_quizzes($lesson_prerequisite);
/*
            foreach ($prereq_lesson_quizzes as $quiz_item) {
                $prerequisite_quiz_id = $quiz_item->ID;
            }
*/

            // Get quiz pass setting
            $prereq_pass_required = get_post_meta($prerequisite_quiz_id, '_pass_required', true);

            if ($prereq_pass_required) {
                $quiz_grade = intval(WooThemes_Sensei_Utils::sensei_get_activity_value(array('post_id' => $prerequisite_quiz_id, 'user_id' => $user_id, 'type' => 'sensei_quiz_grade', 'field' => 'comment_content')));
                $quiz_passmark = intval(get_post_meta($prerequisite_quiz_id, '_quiz_passmark', true));
                if ($quiz_grade < $quiz_passmark) {
                    $show_actions = false;
                }
            } else {
                $user_lesson_end = WooThemes_Sensei_Utils::sensei_get_activity_value(array('post_id' => $lesson_prerequisite, 'user_id' => $user_id, 'type' => 'sensei_lesson_start', 'field' => 'comment_content'));
                if (!$user_lesson_end || $user_lesson_end == '' || strlen($user_lesson_end) == 0) {
                    $show_actions = false;
                }
            }
        }
    }

    function lesson_quizzes($lesson_id = 0, $post_status = 'publish')
    {
        $posts_array = array();

        $post_args = array('post_type' => 'quiz',
            'numberposts' => -1,
            'orderby' => 'title',
            'order' => 'DESC',
            'meta_key' => '_quiz_lesson',
            'meta_value' => $lesson_id,
            'post_status' => $post_status,
        );
        $posts_array = get_posts($post_args);

        return $posts_array;
    }

}