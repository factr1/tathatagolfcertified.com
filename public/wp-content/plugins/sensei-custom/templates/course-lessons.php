<?php
/**
 * The Template for displaying all single course meta information.
 *
 * Override this template by copying it to yourtheme/sensei/single-course/course-lessons.php
 *
 * @author 		WooThemes
 * @package 	Sensei/Templates
 * @version     1.0.0
 */
if (!defined('ABSPATH'))
    exit;

global $post, $woothemes_sensei, $current_user, $cour_id;
$UserMeta = get_user_meta($current_user->data->ID);
$lessons_completed = 0;
$mid = isset($_GET['mid']) ? $_GET['mid'] : '';
//do_action('sensei_course_single_chapter', $mid);
//global $course_module, $course_module_lessons;
//$course_lessons = $course_module_lessons;
//$ChapterID = @trim(str_ireplace('Chapter', '', strstr($course_module->name, ':', true)));
//$total_lessons = count($course_lessons);
$total_lessons = 1;
// Check if the user is taking the course
$is_user_taking_course = WooThemes_Sensei_Utils::sensei_check_for_activity(array('post_id' => $_GET['course_id'], 'user_id' => $current_user->ID, 'type' => 'sensei_course_start'));

get_currentuserinfo();
if (0 < $total_lessons) {

    $lessons_completed = 0;
    $show_lesson_numbers = false;
    $post_classes = array('course', 'post');
    ?>
    <section class="contentWrapper">
        <article class="pageTitleWrap sixtyDayMemProIntro">
            <div class="container">
                <div class="row">
                    <h2>Profile</h2>
                </div>
            </div>
        </article>
        <div class="container">
            <div class="row">
                <ol class="breadcrumb pull-left">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Training Programs</a></li>
                    <li>Certified Movement Specialist Program</li>
                    <li><a href="#">Chapter-01</a></li>
                </ol>
            </div>
        </div>

        <section class="pageInnerContentWrap profilePage">
            <div class="container">
                <div class="row">
                    <article class="col-xs-12 col-sm-6 col-md-6">
                        <div class="clearfix"></div>




                        <?php
                        foreach ($courses as $course) {
                            $course_modules = $sen_modules->get_course_modules($course->ID);
                            $cour_id = $course->ID;
                            $total_lessons_count = 0;
                            $lessons_completed_count = 0;
                            ?>
                            <h1> <?php echo $course->post_title; ?></h1>
                            <ul class="accordion_example trainingProgressLevel">
                                <?php
                                foreach ($course_modules as $course_module) {
                                    if ($course_module->parent == 0) {
                                        do_action('sensei_course_single_chapter', $course_module->term_id);
                                        global $course_module, $course_module_lessons;
                                        $course_lessons = $course_module_lessons;
                                        $ChapterID = @trim(str_ireplace('Chapter', '', strstr($course_module->name, ':', true)));
                                        $total_lessons = count($course_lessons);
                                        $total_lessons_count += $total_lessons;
                                        ?>
                                        <!-- Section 1 -->
                                        <li class="acc_active accordion_in">
                                            <div class="welcomeToHeading acc_head">
                                                <div class="acc_icon_expand"></div>
                                                <ul>
                                                    <li>Chapter<span><?php printf("%02s", $ChapterID); ?></span></li>
                                                    <li>Days<span>1-<?php printf("%02s", $total_lessons); ?></span></li>
                                                </ul>
                                            </div>
                                            <div>

                                                <ul class="trainingProgressLevel">
                                                    <?php
                                                    $lesson_count = 1;
                                                    foreach ($course_lessons as $lesson_item) {
                                                        $single_lesson_complete = false;
                                                        $user_lesson_end = '';
                                                        if (is_user_logged_in()) {
                                                            // Check if Lesson is complete
                                                            $user_lesson_end = WooThemes_Sensei_Utils::sensei_get_activity_value(array('post_id' => $lesson_item->ID, 'user_id' => $current_user->ID, 'type' => 'sensei_lesson_end', 'field' => 'comment_content'));
                                                            if ('' != $user_lesson_end) {
                                                                //Check for Passed or Completed Setting
                                                                $course_completion = $woothemes_sensei->settings->settings['course_completion'];
                                                                if ('passed' == $course_completion) {
                                                                    // If Setting is Passed -> Check for Quiz Grades
                                                                    $lesson_quizzes = $woothemes_sensei->post_types->lesson->lesson_quizzes($lesson_item->ID);
                                                                    // Get Quiz ID
                                                                    if (is_array($lesson_quizzes) || is_object($lesson_quizzes)) {
                                                                        foreach ($lesson_quizzes as $quiz_item) {
                                                                            $lesson_quiz_id = $quiz_item->ID;
                                                                        } // End For Loop
                                                                        // Quiz Grade
                                                                        $lesson_grade = WooThemes_Sensei_Utils::sensei_get_activity_value(array('post_id' => $lesson_quiz_id, 'user_id' => $current_user->ID, 'type' => 'sensei_quiz_grade', 'field' => 'comment_content')); // Check for wrapper
                                                                        // Check if Grade is bigger than pass percentage
                                                                        $lesson_prerequisite = abs(round(doubleval(get_post_meta($lesson_quiz_id, '_quiz_passmark', true)), 2));
                                                                        if ($lesson_prerequisite <= intval($lesson_grade)) {
                                                                            $lessons_completed++;
                                                                            $single_lesson_complete = true;
                                                                            $post_classes[] = 'lesson-completed';
                                                                        } // End If Statement
                                                                    } // End If Statement
                                                                } else {
                                                                    $lessons_completed++;
                                                                    $single_lesson_complete = true;
                                                                    $post_classes[] = 'lesson-completed';
                                                                } // End If Statement;
                                                            } // End If Statement
                                                        } // End If Statement
                                                        // Get Lesson data
                                                        $complexity_array = $woothemes_sensei->frontend->lesson->lesson_complexities();
                                                        $lesson_length = get_post_meta($lesson_item->ID, '_lesson_length', true);
                                                        $lesson_complexity = get_post_meta($lesson_item->ID, '_lesson_complexity', true);
                                                        if ('' != $lesson_complexity) {
                                                            $lesson_complexity = $complexity_array[$lesson_complexity];
                                                        }
                                                        $user_info = get_userdata(absint($lesson_item->post_author));
                                                        $is_preview = WooThemes_Sensei_Utils::is_preview_lesson($lesson_item->ID);
                                                        $preview_label = '';
                                                        if ($is_preview && !$is_user_taking_course) {
                                                            $preview_label = $woothemes_sensei->frontend->sensei_lesson_preview_title_text($post->ID);
                                                            $preview_label = '<span class="preview-heading">' . $preview_label . '</span>';
                                                            $post_classes[] = 'lesson-preview';
                                                        }
                                                        ?>

                                                        <?php if ('' != $user_lesson_end && $single_lesson_complete) { ?>
                                                            <li class="completed">
                                                                <span class="completedLevel"><?php print apply_filters('sensei_complete_text', __('Complete', 'woothemes-sensei')) . '!'; ?></span>
                                                            <?php } else { ?>
                                                            <li>
                                                                <span class="completedLevel">
                                                                    <?php print apply_filters('sensei_in_progress_text', __('In Progress', 'woothemes-sensei')); ?>
                                                                </span>

                                                            <?php }
                                                            ?>
                                                            <div class="dayCount">Day <span><?php printf("%02s", $lesson_count); ?></span></div>
                                                            <ul class="trainingArea">
                                                                <li><a href="<?php print esc_url(get_permalink($lesson_item->ID)); ?>"><?php print esc_html(sprintf(__('%s', 'woothemes-sensei'), substr($lesson_item->post_title, 0, 41))) . $preview_label; ?></a></li>
                                                            </ul>
                                                            <div class="trainingTimeDetail">
                                                                <span>Total Time (43:06)</span>
                                                                <?php
                                                                if (isset($woothemes_sensei->settings->settings['lesson_author']) && ( $woothemes_sensei->settings->settings['lesson_author'] )) {
                                                                    ?>
                                                                    <span>Author: <?php print esc_html($user_info->display_name); ?></span>
                                                                <?php } ?>
                                                                <span>
                                                                    <?php
                                                                    if ('' != $lesson_complexity) {
                                                                        print apply_filters('sensei_complexity_text', __('Complexity: ', 'woothemes-sensei')) . $lesson_complexity;
                                                                    }
                                                                    ?>

                                                                </span>
                                                            </div>
                                                        </li>
                                                        <?php
                                                        $lesson_count++;
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                }
                                ?></ul>
                            <?php
                        }
                        ?>

                    </article>
                    <article class="col-xs-12 col-sm-6 col-md-6 rightPanel">
                        <div class="courseInProgress">
                            <h5>Course in Process...</h5>
                        </div>
                        <div class="clearfix"></div>
                        <h3>Profile Details</h3>

                        <article class="profileDetailWrap">
                            <div class="leftPanelDetail">
                                <div class="profileImg"><?php echo get_avatar($current_user->ID, 230); ?></div>
                                <h6>BADGES EARNED</h6>

                                <ul class="otherProfileImgs">
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                    <li></li>
                                </ul>
                                <?php
                                if (!empty($UserMeta['user_thumbnail'][0])) {
                                    $user_thumb = get_post_meta($UserMeta['user_thumbnail'][0]);
                                    $profile_img_path = site_url() . '/wp-content/uploads/' . $user_thumb['_wp_attached_file'][0];
                                    ?>
                                    <style>
                                        .profilePage .profileDetailWrap .leftPanelDetail .profileImg {
                                            background: url("<?php print $profile_img_path; ?>") no-repeat scroll 10px 10px transparent;
                                        }
                                    <?php } ?>
                                </style>
                            </div>

                            <div class="rightPanelDetail">
                                <h1><span>USER</span> NAME</h1>
                                <h5><?php print ucfirst($current_user->data->user_nicename); ?></h5>

                                <ul class="memberActivityList">
                                    <li class="profSixtyDayPro">Certified Movement Specialist Program Member<span class="startDate">Start Date:<?php print date('d/m/y', strtotime($current_user->data->user_registered)); ?></span></li>
                                    <li class="profOnlineAccademy">Online Academy Member<span class="startDate">Start Date: <?php print date('d/m/y', strtotime($current_user->data->user_registered)); ?></span></li>
                                </ul>
                                <a href="javascript:void(0)">Update Account Info</a>
                                <div class="clearfix"></div>
                                <a href="javascript:void(0)" class="userId">TG000978122223-02</a>
                                <address>
                                    <span>Address</span>
                                    <?php
                                    if (!empty($UserMeta['useraddress'][0])) {
                                        print $UserMeta['useraddress'][0];
                                    }
                                    ?>
                                    <span>Phone </span>
                                    <?php
                                    if (!empty($UserMeta['phone'][0])) {
                                        print $UserMeta['phone'][0];
                                    }
                                    ?>
                                    <span>eMail </span>
                                    <?php print $current_user->data->user_email; ?>
                                </address>
                            </div>

                        </article>
                        <?php //echo do_shortcode('[usercourses]');  ?>
                        <?php
                        $html = '';

                        if (is_user_logged_in() && $is_user_taking_course) {
                            //  $html .= '<span class="course-completion-rate">' . sprintf( __( 'Currently completed %1$s of %2$s in total', 'woothemes-sensei' ), '######', $total_lessons ) . '</span>';
                            $html .= '<div class="meter+++++"><span style="width: @@@@@%">@@@@@%</span></div>';
                        }
                        if (is_user_logged_in() && $is_user_taking_course) {
                            // Add dynamic data to the output
                            $html = str_replace('######', $lesson_count, $html);
                            $progress_percentage = abs(round(( doubleval($lesson_count) * 100 ) / ( $total_lessons_count ), 0));
                            /* if ( 0 == $progress_percentage ) { $progress_percentage = 5; } */
                            $html = str_replace('@@@@@', $progress_percentage, $html);
                            if (50 < $progress_percentage) {
                                $class = ' green';
                            } elseif (25 <= $progress_percentage && 50 >= $progress_percentage) {
                                $class = ' orange';
                            } else {
                                $class = ' red';
                            }
                            $html = str_replace('+++++', $class, $html);
                            $html = '';
                           $html = '<span class="course-completion-rate">PROGRESS OF CHAPTER ' . $chapter_num . "</span>";
                        } // End If Statement
                        $html .= "<div id = 'bar4' class = 'barfiller'>
                    <div class = 'tipWrap'>
                    <span class = 'tip'></span>
                    </div>
                    <span class = 'fill' data-percentage = '$progress_percentage'></span>
                    </div><div style='display:inline;'>$progress_percentage%</div>";
                        echo $html;
//              print $html;
                        ?>
                        <!--<div class="progressBarWrap">
                                                    <span class="course-completion-rate">PROGRESS OF CHAPTER 01</span><div class="barfiller" id="bar4">
                                                        <div class="tipWrap" style="display: inline;">
                                                            <span class="tip" style="left: 160.64px; transition: left 1s ease-in-out 0s;">38%</span>
                                                        </div>
                                                        <span data-percentage="38" class="fill" style="background: none repeat scroll 0% 0% rgb(153, 0, 0); width: 181.64px; transition: width 1s ease-in-out 0s;"></span>
                                                    </div><div style="display:inline;">38%</div>                
                                                </div>-->
                        <!--  <div class="progressBarWrap">
                            <img src="img/progressBar.png" alt="">
                          </div>-->
                        <div class="continueTrainingWrap">
                            <a href="javascript:void(0)" class="redLinkBtn redLinkLeftBtn">
                                <img src="<?php print get_bloginfo('template_url'); ?>/img/60DayWhiteIcon.png" alt=""/>
                            </a>
                            <a href="javascript:void(0)" class="redLinkBtn">Continue Training</a>
                        </div>

                    </article>
                    <div class="clearfix"></div>
                </div>
            </div>
        </section>
        <div class="clearfix"></div>
    </section>
<?php } ?>
