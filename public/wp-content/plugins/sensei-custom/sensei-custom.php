<?php
/*
*   Plugin Name: Sensei Custom
*   Description: Custom Sensei Modules and Front-end
*   Version: 0.2
*   Author: Tathata Golf, Last Updated by James R. Latham
*   Author URI: /
 */

if(!defined('ABSPATH')) {
    exit;
}

function shortcode_all_courses_custom($atts, $content = null)
{
    global $woothemes_sensei, $shortcode_override;
    extract(shortcode_atts(array('amount' => 0), $atts));

    $shortcode_override = 'allcourses_custom';
    $current_user = wp_get_current_user();

    ob_start();
    include(get_template_directory().'/sensei/loop-course.php');
    $content = ob_get_clean();

    return $content;
}

function shortcode_custom_course_lession()
{
    global $woothemes_sensei, $current_user;
    $meta_values = get_post_meta(938);

    $sen_modules = new Sensei_Core_Modules($var_d = null);

    $course_ids = WooThemes_Sensei_Utils::sensei_activity_ids([
        'user_id' => $current_user->ID,
        'type__in' => ['sensei_course_status'],
    ]);

    $args = array(
        'post_type' => 'course',
        'post_status' => 'publish',
        'posts_per_page' => -1,
        'orderby' => 'menu_order date',
        'order' => 'ASC',
        'suppress_filters' => false,
    );
    $courses = get_posts($args);
    ob_start();
    include(get_template_directory().'/sensei/single-course/course-lessons.php');
    $content = ob_get_clean();

    return $content;
}

function tath_subscription_string_restructure( $subscription_string, $product, $include )
{
    if ( ! is_object( $product ) ) {
        $product = WC_Subscriptions::get_product( $product );
    }

    $base_price = woocommerce_price( WC_Subscriptions_Product::get_price( $product ) );

    $fee = WC_Subscriptions_Product::get_sign_up_fee( $product );
    if ( $fee > 0 ) {
        $subscription_string = sprintf('%s and %s per month', woocommerce_price( $fee ), $base_price );
    } else {
        $subscription_string = sprintf('%s per month', $base_price );
    }
    return $subscription_string;
}

function register_custom_lesson_taxonomy()
{
    $labels = array(
        'name'                       => _x( 'Focuses', 'Taxonomy General Name', 'tathata' ),
        'singular_name'              => _x( 'Lesson Focus', 'Taxonomy Singular Name', 'tathata' ),
        'menu_name'                  => __( 'Focus', 'tathata' ),
        'all_items'                  => __( 'All Focuses', 'tathata' ),
        'parent_item'                => __( 'Parent Focus', 'tathata' ),
        'parent_item_colon'          => __( 'Parent Focus:', 'tathata' ),
        'new_item_name'              => __( 'New Focus', 'tathata' ),
        'add_new_item'               => __( 'Add New Focus', 'tathata' ),
        'edit_item'                  => __( 'Edit Focus', 'tathata' ),
        'update_item'                => __( 'Update Focus', 'tathata' ),
        'view_item'                  => __( 'View Focus', 'tathata' ),
        'separate_items_with_commas' => __( 'Separate focuses with commas', 'tathata' ),
        'add_or_remove_items'        => __( 'Add or remove focuses', 'tathata' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'tathata' ),
        'popular_items'              => __( 'Popular Focuses', 'tathata' ),
        'search_items'               => __( 'Search Focuses', 'tathata' ),
        'not_found'                  => __( 'Not Found', 'tathata' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'lesson-focus', array( 'lesson' ), $args );
}

add_action('admin_init',function() {
    $contributor = get_role('60_day');
    $contributor->add_cap('upload_files');
});

add_action('init',function() {

    if(!function_exists('is_sensei_active')||!is_sensei_active()) {
        return;
    }

    require_once('classes/Sensei_Custom.php');
    require_once('classes/WooThemes_Sensei_Frontend_Custom.php');

    add_shortcode('allcourses_custom', 'shortcode_all_courses_custom');
    add_shortcode('custom_course_lession', 'shortcode_custom_course_lession');

    add_shortcode('next_lesson_permalink', 'shortcode_next_lesson_permalink');

    global $sensei_custom_modules;
    $sensei_custom_modules = new Sensei_Custom(__FILE__);

    if(!is_admin()) {
        $sensei_custom_modules = new WooThemes_Sensei_Frontend_Custom();
    }

    add_filter('woocommerce_subscriptions_product_price_string', 'tath_subscription_string_restructure', 10, 3);
    add_image_size('lesson_featured_preview', 55, 55, true);

    register_custom_lesson_taxonomy();
},0);

if(!function_exists('get_next_lesson'))
{
    /**
    *   Returns the first uncompleted lesson ID from a course
    *
    *   @param integer $course_id
    *
    *   @return integer
    */
    function get_next_lesson($course_id)
    {
        global $woothemes_sensei;;

        // No user, bail early
        if(!is_user_logged_in()) {
            return;
        }

        $course_modules = $woothemes_sensei->modules ->get_course_modules(intval($course_id));

        $next_lesson = null;
        foreach($course_modules as $course_module) {

            $lessons = Sensei_Custom::getLessons($course_id,$course_module->term_id);

            foreach($lessons as $lesson) {

                $user_lesson_end = WooThemes_Sensei_Utils::user_completed_lesson(
                    WooThemes_Sensei_Utils::user_lesson_status(
                        $lesson->ID,
                        get_current_user_id()
                    )
                );

                $course_completion = $woothemes_sensei->settings->settings['course_completion'];

                if(!$user_lesson_end || $course_completion != 'passed') {
                    $next_lesson = $lesson->ID;
                    break 2;
                }

            }

        }

        return $next_lesson;
    }
}

function shortcode_next_lesson_permalink($atts)
{
    $atts = shortcode_atts([
        'course' => 0
    ],$atts);

    $course_id = (int) $atts['course'];

    if(!$course_id) {
        return;
    }

    return esc_attr(get_permalink(get_next_lesson($course_id)));
}

function custom_sensei_has_user_completed_prerequisite_lesson( $lesson_id, $user_id ) {

    $lesson_prerequisite =  WooThemes_Sensei_Lesson::get_lesson_prerequisite_id( $lesson_id );
    $lesson_has_pre_requisite = $lesson_prerequisite > 0;

    return $lesson_has_pre_requisite ? WooThemes_Sensei_Lesson::is_prerequisite_complete(  $lesson_id, $user_id ) : true;

} // End sensei_has_user_completed_prerequisite_lesson()