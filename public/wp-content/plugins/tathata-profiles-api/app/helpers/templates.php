<?php namespace TGCAPI;

if(!defined('TGCAPI_VERSION')) { return; }

function view($view,array $data = [])
{
    return PluginController::view($view,$data);
}