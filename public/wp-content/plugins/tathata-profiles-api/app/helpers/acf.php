<?php namespace TGCAPI;

if(!function_exists("get_repeater_subfields"))
{
    function get_repeater_subfields($field_name,$subfields = [],$post_id = false)
    {
        if(!function_exists("get_field")||!$subfields) {
            return [];
        }
        if(is_string($subfields)) {
            $subfields = explode(" ",$subfields);
        }
        $repeater = get_field($field_name,$post_id);
        if(!$repeater || !is_array($repeater)) {
            return [];
        }
        $rows = [];
        foreach($repeater as $k => $field) {
            if(count($subfields)==1) {
                $rows[] = $field[$subfields[0]];
                continue;
            }
            $row = [];
            foreach($subfields as $s) {
                $row[$s] = $field[$s];
            }
            $rows[] = $row;
        }
        return $rows;
    }
}