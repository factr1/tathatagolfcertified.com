<?php namespace TGCAPI;

if(!defined('TGCAPI_VERSION')) { return; }

function env($key = null)
{
    if(is_null($key)) {
        return $_ENV;
    }
    return (isset($_ENV[$key])) ? (string) $_ENV[$key] : '';
}

function app_path()
{
    return TGCAPI_PLUGIN_DIR.'/app';
}

function plugin_path($path = '')
{
    return TGCAPI_PLUGIN_DIR.(($path)?'/'.$path:'');
}

function plugin_url($path = '')
{
    return \plugins_url($path,TGCAPI_PLUGIN_FILE);
}

function plugin_folder()
{
    return basename(plugin_url());
}

function ajax_url()
{
    return \admin_url('admin-ajax.php');
}

function snake_case($s)
{
    return preg_replace("#\s+#ismu","",ucwords(preg_replace("#[^a-z]+#ismu"," ",$s)));
}

function ssl_path()
{
    return plugin_path('app/ssl');
}