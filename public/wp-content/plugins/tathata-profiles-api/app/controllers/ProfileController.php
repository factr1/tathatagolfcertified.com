<?php namespace TGCAPI;

class ProfileController extends BaseController {

    use StaticCalls;

    /**
    *   Create a new instance
    *
    */
    public function __construct()
    {
        if(!Auth::valid()) {
            $this->abort(403,'Invalid Authorization Header');
        }
    }

    /**
    *   Return json view of all profiles
    *
    *   @return string
    */
    public function _getProfiles()
    {
        static::__protect($this,__FUNCTION__);
        $this->json(Profiles::all());
        exit;
    }

    /**
    *   Return json view of featured profiles
    *
    *   @return string
    */
    public function _getFeaturedProfiles()
    {
        static::__protect($this,__FUNCTION__);
        $this->json(Profiles::featured());
        exit;
    }

    /**
    *   Return json view of completed profiles
    *
    *   @return string
    */
    public function _getCompletedProfiles($page = 1)
    {
        static::__protect($this,__FUNCTION__);
        $this->json(Profiles::completed(intval($page)));
        exit;
    }

    /**
    *   Return json view of completed profiles by search query
    *
    *   @param string $q
    *   @param integer $page
    *
    *   @return string
    */
    public function _getProfilesByQuery($q = '',$page = 1)
    {
        static::__protect($this,__FUNCTION__);

        if(!is_string($q)) {
            return;
        }

        $q = urldecode($q);

        $this->json(Profiles::search($q,intval($page)));
        exit;
    }

    /**
    *   Returns json view of profile by user id
    *
    *   @param integer $id
    *
    *   @return string
    */
    public function _getProfileById($id)
    {
        static::__protect($this,__FUNCTION__);

        $data = ($user=Profiles::user($id)) ? $user : [];

        $this->json($data);
        exit;
    }

    /**
    *   Returns json view of profile by user slug
    *
    *   @param string $slug
    *
    *   @return string
    */
    public function _getProfileBySlug($slug)
    {
        static::__protect($this,__FUNCTION__);

        $data = ($user=Profiles::userBySlug($slug)) ? $user : [];

        $this->json($data);
        exit;
    }

}