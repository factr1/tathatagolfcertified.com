<?php namespace TGCAPI;

class RouteController extends BaseController {

    use StaticCalls;

    public static $routes = [];

    public function _init()
    {
        static::add_routes();
        static::check_routes();
    }

    public function _add_routes()
    {
        self::__protect($this,__FUNCTION__);

        $endpoint = plugin_folder();

        static::$routes = [

            '^/'.$endpoint.'/profiles/?$' => [
                'controller' => __NAMESPACE__.'\ProfileController',
                'action' => 'getProfiles',
                'params' => ''
            ],

            '^/'.$endpoint.'/profiles/featured/?$' => [
                'controller' => __NAMESPACE__.'\ProfileController',
                'action' => 'getFeaturedProfiles',
                'params' => ''
            ],

            '^/'.$endpoint.'/profiles/completed/?$' => [
                'controller' => __NAMESPACE__.'\ProfileController',
                'action' => 'getCompletedProfiles',
                'params' => ''
            ],

            '^/'.$endpoint.'/profiles/completed/?\?page=([0-9]+)$' => [
                'controller' => __NAMESPACE__.'\ProfileController',
                'action' => 'getCompletedProfiles',
                'params' => '1'
            ],

            '^/'.$endpoint.'/profiles/search/?\?q=([^&]*)?$' => [
                'controller' => __NAMESPACE__.'\ProfileController',
                'action' => 'getProfilesByQuery',
                'params' => '1',
                'with_query' => true
            ],

            '^/'.$endpoint.'/profiles/search/?\?q=([^&]*)?&page=([0-9]+)$' => [
                'controller' => __NAMESPACE__.'\ProfileController',
                'action' => 'getProfilesByQuery',
                'params' => '1,2',
                'with_query' => true
            ],

            '^/'.$endpoint.'/profiles/([0-9]+)/?$' => [
                'controller' => __NAMESPACE__.'\ProfileController',
                'action' => 'getProfileById',
                'params' => '1'
            ],

            '^/'.$endpoint.'/profiles/slug/([^/]+)/?$' => [
                'controller' => __NAMESPACE__.'\ProfileController',
                'action' => 'getProfileBySlug',
                'params' => '1'
            ],

        ];
    }

    protected function _params(array $route,array $match)
    {
        $params = [];
        if(isset($route['params'])) {
            foreach(explode(",",$route['params']) as $n) {
                $params[] = (isset($match[$n])&&$match[$n]) ? $match[$n] : null;
            }
        }
        return $params;
    }

    protected function _route_controller(array $route,array $params = [])
    {
        if(!isset($route['controller'])||!isset($route['action'])) {
            return;
        }
        $c = $route['controller'];
        $a = $route['action'];
        $cb = [$c,$a];

        if(!is_callable($cb)) {
            return;
        }

        $response = call_user_func_array($cb,$params);

        if(!is_string($response)) {
            return;
        }

        echo $response;
        exit;
    }

    protected function _route_redirect(array $route,array $params = [])
    {
        if(!isset($route['redirect'])) {
            return;
        }
        foreach($params as $k => $v) {
            $route['redirect'] = str_replace("\$".($k+1),$v,$route['redirect']);
        }
        $status = (isset($route['status'])) ? (int) $route['status'] : 302;
        wp_redirect($route['redirect'],$status);
        exit;
    }

    public function _check_routes()
    {
        self::__protect($this,__FUNCTION__);
        foreach(static::$routes as $preg => $route) {
            $q = (isset($route['with_query'])&&$route['with_query']===true) ? true : false;
            if(preg_match("#".$preg."#ismu",$this->uri($q),$match)) {
                if(!is_array($route)) { return; }
                $params = $this->_params($route,$match);
                $this->_route_controller($route,$params);
                $this->_route_redirect($route,$params);
            }
        }
    }

}