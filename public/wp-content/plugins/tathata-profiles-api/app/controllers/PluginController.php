<?php namespace TGCAPI;

class PluginController extends BaseController {

    use StaticCalls;

    protected static $instance;

    /**
    *   Create a new plugin instance and initialize controllers
    *
    *   @return string
    */
    public function __construct()
    {
        if(!function_exists('acf_add_local_field_group')) {
            return;
        }

        if(static::$instance) {
            return;
        }

        ACF::init();
        RouteController::init();

        static::$instance = $this;
    }

}