<?php namespace TGCAPI;

trait StaticCalls {

    public static function __callStatic($method,$args = [])
    {
        $method = "_".$method;
        $cls = get_called_class();
        if(method_exists($cls,$method)) {
            $c = new $cls();
            return ($args) ? call_user_func_array([&$c,$method],$args) : $c->$method();
        } else {
        throw new \Exception("Unable to access ".$method." for class \"".get_called_class()."\" in static context");
        }
    }

    public static function __protect($obj,$func)
    {
        if(is_null($obj)) {
            throw new \Exception("Unable to access \"".$func."\" for class \"".get_called_class()."\" in static context");
        }
    }

}