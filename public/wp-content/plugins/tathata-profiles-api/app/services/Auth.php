<?php namespace TGCAPI;

use \HttpResponse as Http;

class Auth {

    /**
    *   Verifies a valid authorization header exists in request
    *   matching a users api key. Returns user if found
    *
    *   @return WP_User|false
    */
    public static function valid()
    {
        if(!isset($_SERVER['HTTP_APIAUTH'])) {
            return false;
        }

        $auth = base64_decode((string)$_SERVER['HTTP_APIAUTH']);

        if(!$auth||!preg_match('#^TGCAPI ([^\s:]+):([^\s:]+)$#ismu',$auth,$creds)) {
            return false;
        }

        $user = get_user_by('email',$creds[1]);
        $key = $creds[2];

        if(!$user || !$key || get_field('certified_api_key','user_'.$user->ID)!==$key) {
            return false;
        }

        return $user;
    }

}