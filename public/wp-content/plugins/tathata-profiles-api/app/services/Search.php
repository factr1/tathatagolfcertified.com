<?php namespace TGCAPI;

class Search {

    /**
    *   Returns array of fields from object to be matched against
    *   based on the properties passed.
    *
    *   @param object $obj
    *   @param array $vars
    *
    *   @return string
    */
    public static function fields(\stdClass $obj,array $vars)
    {
        return array_filter(array_map(function($e) use($obj) {
            return (property_exists($obj,$e)) ? (string) $obj->$e : null;
        },$vars),function($e) {
            return !is_null($e);
        });
    }

    /**
    *   Returns the relevant items from a search match
    *
    *   @param array $items
    *   @param integer $tolerance
    *
    *   @return array
    */
    protected static function relevant(array $items,$tolerance)
    {
        // Filter out non matching or above tolerance items
        $items = array_filter($items,function($item) use($tolerance) {
            return ($item->relevance>0 && $item->relevance<=$tolerance);
        });

        // Sort based on relevance
        usort($items,function($a,$b) {
            if($a->relevance===$b->relevance) { return 0; }
            return ($a->relevance<$b->relevance) ? -1 : 1;
        });

        return $items;
    }

    /**
    *   Returns matching items from an array of objects based on the properties
    *   to match against and the query.
    *
    *   @param array $items
    *   @param string $query
    *   @param array|callback $vars (callback should take an object and return array of field values)
    *   @param integer $tolerance  (1 uses exact match, 5 uses loose word match)
    *
    *   @return array
    */
    public static function matches(array $items, $query = '', $vars, $tolerance = 100)
    {
        // Nothing to match, bail early
        if(!is_string($query)||!$query||!$items) {
            return $items;
        }

        // Parse query into words
        $query = explode(" ",$query);

        // Calculate the metaphone for the query
        $querymeta = implode(" ",array_map(function($e) {
            return trim(metaphone($e));
        },$query));

        // No query meta, likely just a number queried, return no matches
        if(!$querymeta) {
            return [];
        }

        // Loop through the items
        foreach($items as $k => $item) {

            // Load the searchable fields
            $fields = is_callable($vars) ? call_user_func_array($vars,[$item]) : static::fields($item,$vars);

            // No searchable fields found, continue
            if(!$fields) {
                continue;
            }

            // Calclate the metaphone for the field words
            $fieldmeta = implode(" ",array_map(function($e) {
                $words = explode(" ",$e);
                return implode(" ",array_map(function($word) {
                    return trim(metaphone($word));
                },$words));
            },$fields));

            // Calculate the character difference as a percentage of the query length
            $querydiff = levenshtein($querymeta,$fieldmeta) / strlen($querymeta);

            $item->relevance = 0;

            // Exact matches
            if($tolerance>=1&&$querymeta===$fieldmeta) {
                $item->relevance = 1;
                continue;
            }

            // Partial match but very close match
            if($tolerance>=2&&$querydiff>=0&&$querydiff<.25) {
                $item->relevance = 2;
                continue;
            }

            // Partial match but somewhat close match
            if($tolerance>=3&&$querydiff>=0&&$querydiff<.5) {
                $item->relevance = 3;
                continue;
            }

            // Entire query found in fields
            if($tolerance>=4 && (in_array($querymeta,explode(" ",$fieldmeta))!==false || strpos($fieldmeta,$querymeta)!==false) ) {
                $item->relevance = 4;
                continue;
            }

            // Exact word matches found
            if($tolerance>=4) {

                // Count of words found
                $word_count = array_sum(array_map(function($word) use($fields) {
                    return in_array($word,$fields) ? 1 : 0;
                },$query));

                // Percentage of words found
                if($word_count) {
                    $pct = ($word_count / count($query)) * 100;
                    $item->relevance = (int) (104 - $pct);

                    if($item->relevance<=$tolerance) {
                        continue;
                    }
                }
            }

            // Loose meta matches found
            if($tolerance>=100) {

                $wordmeta = explode(" ",$querymeta);

                $word_count = array_sum(array_map(function($w) use($fieldmeta) {
                    return in_array($w,explode(" ",$fieldmeta)) ? 1 : 0;
                },$wordmeta));

                if($word_count) {
                    $pct = ($word_count / count($wordmeta)) * 100;
                    $item->relevance = (int) (105 - $pct);

                    if($item->relevance<=$tolerance) {
                        continue;
                    }
                }

            }

            unset($items[$k]);
        }

        return static::relevant($items,$tolerance);
    }

    /**
    *   Returns matching items from an array of objects based on the
    *   distance from a location
    *
    *   @param array $items
    *   @param object $center {lat,lng}
    *   @param callback $cb (callback should take an item object and return array of location objects)
    *   @param float $tolerance (Distance in Miles)
    *   @return array
    */
    public static function matchesByRadius(array $items, \stdClass $center, $cb, $tolerance = 100)
    {
        // No items, bail early
        if(!$items) {
            return $items;
        }

        // Check for valid center
        if(!property_exists($center,'lat')||!property_exists($center,'lng')) {
            return [];
        }

        // Loop through the items
        foreach($items as $k => $item) {

           // Load the searchable locations
            $locations = is_callable($cb) ? call_user_func_array($cb,[$item]) : null;

            // No searchable fields found, remove from search and continue
            if(!is_array($locations)||!$locations) {
                unset($items[$k]);
                continue;
            }

            // Check locations for match
            foreach($locations as $location) {

                // Force objects
                if(is_array($location)) {
                    $location = (object) $location;
                }

                // Invalid location data, skip
                if(!is_object($location)||!property_exists($location,'lat')||!property_exists($location,'lng')) {
                    continue;
                }

                // Obtain distance between points
                $distance = Geocode::distance(
                    $center->lat,
                    $center->lng,
                    $location->lat,
                    $location->lng
                );

                // Distance in radius, check next item
                if($distance<=$tolerance) {
                    continue 2;
                }
            }

            // Not in radius, remove
            unset($items[$k]);
        }

        return $items;
    }

    /**
    *   Returns array of items in excluding items from another set
    *
    *   @param array items
    *   @param array exclude
    *
    *   @return array
    */
    public static function excludeItems(array $items,array $exclude)
    {
        if(!$exclude) {
            return $items;
        }

        $exclude = array_map(function($item) {
            return $item->ID;
        },$exclude);

        return array_filter($items,function($item) use($exclude) {
            return !in_array($item->ID,$exclude);
        });
    }

    /**
    *   Returns subset for page from an array
    *
    *   @param array $items
    *   @param integer $limit
    *   @param integer $page
    *
    *   @return array
    */
    public static function paginatedResults($items,$limit,$page)
    {
        if(!is_array($items)||!is_int($limit)||!is_int($page)) {
            return [];
        }

        $count = count($items);
        $pages = ceil($count/$limit);

        if($page<1) {
            return [
                'items' => $items,
                'count' => $count,
                'pages' => $pages,
                'page' => 1
            ];
        }

        $offset = ($limit * $page) - $limit;

        if($offset>=$count) {
            return [
                'items' => [],
                'count' => $count,
                'pages' => $pages,
                'page' => $page
            ];
        }

        return [
            'items' => array_slice($items,$offset,$limit),
            'count' => $count,
            'pages' => $pages,
            'page' => $page
        ];
    }

}