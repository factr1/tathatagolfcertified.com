<?php namespace TGCAPI;

class Geocode {

    const ENDPOINT = "https://maps.googleapis.com/maps/api/geocode/json?";

    protected static $instance;

    protected $params;

    /**
    *   Create a new instance
    *
    *   @return void
    */
    public function __construct(array $params = [])
    {
        $this->params = array_merge([
            'apiKey' => ''
        ],$params);
    }

    /**
    *   Fetch a Given URL via cURL
    *
    *   @param string $url
    *
    *   @return string
    */
    protected function fetchUrl($url)
    {
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_HTTPGET,true);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch,CURLOPT_HEADER,false);
        curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
        curl_setopt($ch,CURLOPT_MAXREDIRS,10);
        curl_setopt($ch,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_1);
        curl_setopt($ch,CURLOPT_REFERER,"http://".$_SERVER['HTTP_HOST']."/");
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,2);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,true);
        curl_setopt($ch,CURLOPT_CAINFO,ssl_path()."/Mozilla_Bundle.pem");
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT,10);
        curl_setopt($ch,CURLOPT_TIMEOUT,60);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    /**
    *   Returns the json decoded response from a url endpoint
    *
    *   @param string $url
    *
    *   @return object
    */
    protected function fetchResponse($url)
    {
        if(!($response=$this->fetchUrl($url))) {
            return false;
        }

        if(!($data=json_decode($response))) {
            return false;
        }

        // Check for results array
        if(!is_object($data)||!property_exists($data,'results')||!is_array($data->results)||!$data->results) {
            return false;
        }

        // Load coordinate data
        $location = $data->results[0]->geometry->location;

        $data = (object) [
            'lat' => $location->lat,
            'lng' => $location->lng,
        ];

        // Return coordinates
        return $data;
    }

    /**
    *   Returns base endpoint query url
    *
    *   @param string $address
    *
    *   @return string
    */
    protected function query($address)
    {
        return static::ENDPOINT.'address='.urlencode($address).'&sensor=false'.
            (($this->params['apiKey'])?'&key='.urlencode($this->params['apiKey']):'');
    }

    /**
    *   Returns response for a given address
    *
    *   @param string $address
    */
    public function get($address)
    {
        return $this->fetchResponse($this->query($address));
    }

    /**
    *   Calculates the distance between locations
    *
    *   @param float $lat1
    *   @param float $lng1
    *   @param float $lat2
    *   @param float $lng2
    *   @param string $unit
    *
    *   @return float
    */
    public static function distance($lat1, $lng1, $lat2, $lng2, $unit = 'M')
    {
        $theta = $lng1 - $lng2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        // Return Kilometers
        if($unit === 'K') {
            return ($miles * 1.609344);
        }

        return $miles;
    }

    /**
    *   Returns new API instance loading any defined email/key
    *
    *   @param array $params
    *
    *   @return \TGCAPI\Geocode
    */
    public static function getInstance(array $params = [])
    {
        if(is_null(static::$instance)) {
            static::$instance = new static(array_merge([
                'apiKey' => ((defined("GOOGLE_GEOCODE_API_KEY"))?GOOGLE_GEOCODE_API_KEY:'')
            ],$params));
        }
        return static::$instance;
    }

}