<?php namespace TGCAPI;

use \WP_User;
use \Groups_Group;

class Profiles {

    const GROUP_COMPLETED = 7;
    const PAGE_LIMIT = 100;

    /**
    *   Returns a user profile object formatted for the API
    *
    *   @param $user WP_User
    *
    *   @return object
    */
    public static function get(WP_User $user)
    {
        $user_key = 'user_'.$user->ID;

        $user = (object) [
                'ID' => $user->ID,
                'first_name' => get_field('first_name',$user_key),
                'last_name' => get_field('last_name',$user_key),
                'display_name' => $user->display_name,
                'email' => $user->user_email,
                'website' => get_field('url',$user_key),
                'phone' => get_field('phone',$user_key),
                'twitter' => get_field('twitter',$user_key),
                'facebook' => get_field('facebook',$user_key),
                'instagram' => get_field('instagram',$user_key),
                'googleplus' => get_field('googleplus',$user_key),
                'youtube' => get_field('youtube',$user_key),
                'birth_date' => get_field('birth_date',$user_key),
                'mailing_address' => get_field('mailing_address',$user_key),
                'bio' => get_field('description',$user_key),
                'locations' => get_repeater_subfields('locations',[
                    'name',
                    'months_of_year',
                    'address',
                    'phone',
                    'description'
                ],$user_key),
                'student_rating' => get_field('student_rating',$user_key),
                'group_training_offerings' => get_field('group_training_offerings',$user_key),
                'live_training' => get_field('live_training',$user_key),
                'active' => (bool) get_field('profile_active',$user_key),
                'featured' => (bool) get_field('profile_featured',$user_key),
                'course_completed' => (int) get_user_meta($user->ID,'course_completion_stamp',true),
                'avatar' => get_avatar($user->ID,200),
                'profile_header_image' => get_field('profile_header_image',$user_key),
                'profile_slug' => get_field('profile_slug',$user_key),
            ];

        // Cleanup address field
        foreach($user->locations as $k => $location) {
            if(!isset($location['address']['address'])) {
                continue;
            }
            $user->locations[$k]['address']['address'] = preg_replace('#, United States$#ismu','',$location['address']['address']);
        }

        // Pull src only from avatar
        if(preg_match('#^.*src=("|\')([^"\']*)("|\').*$#imsu',(string)$user->avatar,$matches)) {
            $user->avatar = $matches[2];
        }

        return $user;
    }

    /**
    *   Returns all user profiles
    *
    *   @return array
    */
    public static function all()
    {
        $profiles = [];

        foreach(get_users() as $user) {
            $profiles[] = static::get($user);
        }

        return $profiles;
    }

    /**
    *   Returns all user profiles marked as featured specialists
    *
    *   @return array
    */
    public static function featured()
    {
        $profiles = [];

        $users = get_users([
            'meta_query' => [
                'relation' => 'AND',
                [
                    'key' => 'profile_active',
                    'value' => 1,
                ],
                [
                    'key' => 'profile_featured',
                    'value' => 1,
                ],
            ],
            'number' => 4, // Limit returned to 4 profiles
        ]);

        foreach($users as $user) {
            $profiles[] = static::get($user);
        }

        return $profiles;
    }

    /**
    *   Returns all user profiles with course completed
    *
    *   @param integer $page
    *
    *   @return array
    */
    public static function completed($page = 1)
    {
        $group = new Groups_Group(static::GROUP_COMPLETED);

        // Load all profile detail
        $profiles = [];
        foreach($group->users as $user) {
            $profiles[] = static::get($user->user);
        }

        // Filter inactive profiles
        $profiles = array_filter($profiles,function($profile) {
            return $profile->active;
        });

        // Sort by star rating descending
        usort($profiles,function($a,$b) {
            $ar = intval($a->student_rating);
            $br = intval($b->student_rating);

            if($ar>$br) { return -1; }
            if($ar<$br) { return 1; }

            return strcmp($a->first_name,$b->first_name);
        });

        return Search::paginatedResults($profiles,static::PAGE_LIMIT,$page);
    }

    /**
    *   Returns a user profile by id
    *
    *   @param integer $id
    *
    *   @return array
    */
    public static function user($id)
    {
        $user = get_user_by('ID',intval($id));

        return ($user) ? (array) static::get($user) : null;
    }

    /**
    *   Returns a user profile by slug
    *
    *   @param string $slug
    *
    *   @return array
    */
    public static function userBySlug($slug)
    {
        global $wpdb;

        $results = $wpdb->get_results("
            select user_id from ".$wpdb->prefix."usermeta
                where meta_key='profile_slug' and meta_value='".$wpdb->_real_escape($slug)."' limit 1
        ");

        return ($results) ? static::user((int)$results[0]->user_id) : null;
    }

    /**
    *   Returns location names from a profile
    *
    *   @param object $user
    *
    *   @return array
    */
    protected static function locations(\stdClass $profile)
    {
        if(!property_exists($profile,'locations')||!$profile->locations) {
            return [];
        }
        return array_filter(array_map(function($location) {
            if(is_array($location)) {
                $location = (object) $location;
            }
            if(!property_exists($location,'name')||!$location->name) {
                return null;
            }
            return $location->name;
        },$profile->locations),function($location) {
            return !is_null($location);
        });
    }

    /**
    *   Returns addresses from a profile
    *
    *   @param object $profile
    *
    *   @return array
    */
    protected static function addresses(\stdClass $profile)
    {
        if(!property_exists($profile,'locations')||!$profile->locations) {
            return [];
        }
        return array_filter(array_map(function($location) {
            if(is_array($location)) {
                $location = (object) $location;
            }
            if(!property_exists($location,'address')||!$location->address) {
                return null;
            }
            if(is_array($location->address)) {
                $location->address = (object) $location->address;
            }
            if(!property_exists($location->address,'address')||!$location->address->address) {
                return null;
            }
            return $location->address->address;
        },$profile->locations),function($address) {
            return !is_null($address);
        });
    }

    /**
    *   Returns location coordinates from a profile
    *
    *   @param object $profile
    *
    *   @return array
    */
    protected static function coordinates(\stdClass $profile)
    {
        if(!property_exists($profile,'locations')||!$profile->locations) {
            return [];
        }
        return array_filter(array_map(function($location) {
            if(is_array($location)) {
                $location = (object) $location;
            }
            if(!property_exists($location,'address')||!$location->address) {
                return null;
            }
            if(is_array($location->address)) {
                $location->address = (object) $location->address;
            }
            if(!property_exists($location->address,'address')||!$location->address->address) {
                return null;
            }
            return (object) [
                'lat' => $location->address->lat,
                'lng' => $location->address->lng,
            ];
        },$profile->locations),function($location) {
            return !is_null($location);
        });
    }

    /**
    *   Returns user profiles with course completed by search query
    *
    *   @param string $q
    *   @param integer $page
    *
    *   @return array
    */
    public static function search($q = '',$page = 1)
    {
        if(!is_string($q)||!$q) {
            return $completed;
        }

        // Check for Location
        $geo = Geocode::getInstance();
        $location = $geo->get($q);

        if($location) {
            return static::searchByLocation($location,$q,$page);
        }

        // Load all completed profiles
        $completed = static::completed(0);
        $profiles = $completed['items'];

        // Name Matches
        $firstNameMatches = Search::matches($profiles,$q,['first_name'],2);
        $profiles = Search::excludeItems($profiles,$firstNameMatches);

        $lastNameMatches = Search::matches($profiles,$q,['last_name'],2);
        $profiles = Search::excludeItems($profiles,$lastNameMatches);

        $displayNameMatches = Search::matches($profiles,$q,['display_name'],3);
        $profiles = Search::excludeItems($profiles,$displayNameMatches);

        $fullNameMatches = Search::matches($profiles,$q,['first_name','last_name'],3);
        $profiles = Search::excludeItems($profiles,$fullNameMatches);

        // Location Matches
        $locationMatches = Search::matches($profiles,$q,function($profile) {
            return static::locations($profile);
        },3);
        $profiles = Search::excludeItems($profiles,$locationMatches);

        // Address Matches
        $addressMatches = Search::matches($profiles,$q,function($profile) {
            return static::addresses($profile);
        },3);
        $profiles = Search::excludeItems($profiles,$addressMatches);

        // Combine Matches
        $profiles = array_merge(
            $firstNameMatches,
            $lastNameMatches,
            $displayNameMatches,
            $fullNameMatches,
            $locationMatches,
            $addressMatches
        );

        unset($instructorMatches);
        unset($displaynameMatches);
        unset($locationMatches);
        unset($addressMatches);

        // Sort by star rating descending, relevance ascending
        usort($profiles,function($a,$b) {
            $ar = intval($a->student_rating);
            $br = intval($b->student_rating);

            if($ar>$br) { return -1; }
            if($ar<$br) { return 1; }

            if($a->relevance===$b->relevance) { return 0; }
            return ($a->relevance<$b->relevance) ? -1 : 1;
        });

        return Search::paginatedResults($profiles,static::PAGE_LIMIT,$page);
    }

    /**
    *   Returns user profiles with course completed by search query based on location
    *
    *   @param string $q
    *   @param integer $page
    *
    *   @return array
    */
    protected static function searchByLocation(\stdClass $location, $q = '',$page = 1)
    {
        // Load all completed profiles
        $completed = static::completed(0);
        $profiles = $completed['items'];

        // Name Matches
        $firstNameMatches = Search::matches($profiles,$q,['first_name'],2);
        $profiles = Search::excludeItems($profiles,$firstNameMatches);

        $lastNameMatches = Search::matches($profiles,$q,['last_name'],2);
        $profiles = Search::excludeItems($profiles,$lastNameMatches);

        $displayNameMatches = Search::matches($profiles,$q,['display_name'],3);
        $profiles = Search::excludeItems($profiles,$displayNameMatches);

        $fullNameMatches = Search::matches($profiles,$q,['first_name','last_name'],3);
        $profiles = Search::excludeItems($profiles,$fullNameMatches);

        // Location name close matches
        $locationNameMatches = Search::matches($profiles,$q,function($profile) {
            return static::locations($profile);
        },3);
        $profiles = Search::excludeItems($profiles,$locationNameMatches);

        // Location radius matches
        $locationMatches = Search::matchesByRadius($profiles,$location,function($profile) {
            return static::coordinates($profile);
        });
        $profiles = Search::excludeItems($profiles,$locationMatches);

        // Combine Matches
        $profiles = array_merge(
            $firstNameMatches,
            $lastNameMatches,
            $displayNameMatches,
            $fullNameMatches,
            $locationNameMatches,
            $locationMatches
        );

        unset($instructorMatches);
        unset($displaynameMatches);
        unset($locationNameMatches);
        unset($locationMatches);

        // Sort by star rating descending
        usort($profiles,function($a,$b) {
            $ar = intval($a->student_rating);
            $br = intval($b->student_rating);

            if($ar>$br) { return -1; }
            if($ar<$br) { return 1; }

            return 0;
        });

        return Search::paginatedResults($profiles,static::PAGE_LIMIT,$page);
    }

}