<?php namespace TGCAPI;

class ACF {

    public static function init()
    {
        static::registerFields();
        static::registerFilters();
    }

    /**
    *   Registers all ACF Fields for Template Post Type
    *
    */
    public static function registerFields()
    {
        // Fields registered in local acf-json
    }

    /**
    *   Registers all custom ACF Filters
    *
    */
    public static function registerFilters()
    {
        // Loading Profile Slug
        add_filter('acf/load_value/name=profile_slug',function($value,$post_id,$field) {
            if(!$value) {
                $first_name = get_field('first_name',$post_id);
                $last_name = get_field('last_name',$post_id);
                $value = sanitize_title(trim($first_name.' '.$last_name));
            }
            return $value;
        },10,3);

        // Validate Profile Slug

        add_filter('acf/validate_value/name=profile_slug',function($valid,$value,$field,$input) {

            $user_id = (isset($_POST['user_id']) && preg_match('#^[0-9]+$#ismu',$_POST['user_id'])) ? (int) $_POST['user_id'] : 0;

            if(!$valid||!$user_id) {
                return $valid;
            }

            global $wpdb;

            $value = sanitize_title(trim($value));

            $results = $wpdb->get_results("
                select user_id from ".$wpdb->prefix."usermeta
                    where meta_key='profile_slug' and meta_value='".$wpdb->_real_escape($value)."'
                    and user_id!=".$user_id."
            ");

            return (!$results) ? $valid : 'Profile slug "'.$value.'" is already in use. Please select another handle.';
        },10,4);

        // Saving Profile Slug
         add_filter('acf/update_value/name=profile_slug',function($value,$post_id,$field) {

            if(!preg_match('#^user_([0-9]+)$#ismu',$post_id,$matches)) {
                return $value;
            }

            if(!$value) {
                $first_name = get_field('first_name',$post_id);
                $last_name = get_field('last_name',$post_id);
                $value = trim($first_name.' '.$last_name);
            }

            $user_id = (int) $matches[1];
            $value = sanitize_title(trim($value));

            global $wpdb;

            $results = $wpdb->get_results("
                select user_id from ".$wpdb->prefix."usermeta
                    where meta_key='profile_slug' and meta_value='".$wpdb->_real_escape($value)."'
                    and user_id!=".$user_id."
            ");

            return (!$results) ? $value : '';
        },10,3);

    }

}