<?php namespace TGCAPI;
/*
Plugin Name: Tathata Golf Certified API
Plugin URI: http://www.tathatagolfcertified.com/
Description: API for Certified Instructor Profiles
Version: 0.0.1
Author: Factor1, James R. Latham
Author URI: http://www.factor1studios.com
*/

if(!function_exists('add_action')) {
    exit;
}

require_once('constants.php');
require_once('vendor/autoload.php');

spl_autoload_register(['TGCAPI\BaseController','__autoload']);

add_action('init',function() {
    $plugin = new PluginController();
});