# Tathata Golf Certified Instructor API

The purpose of this plugin is to provide a custom API for querying and fetching certified 
instructor profiles.

## Available Endpoints

* /tathata-profiles-api/profiles
* /tathata-profiles-api/profiles/featured
* /tathata-profiles-api/profiles/completed
* /tathata-profiles-api/profiles/completed?page={page_number}
* /tathata-profiles-api/profiles/search?q={query}
* /tathata-profiles-api/profiles/search?q={query}&page={page_number}
* /tathata-profiles-api/profiles/{user_id}
* /tathata-profiles-api/profiles/{user_slug}

## Search Results

When searching for instructors the plugin will:

1) Query Google to determine if the query is a location (zip, address, state, etc)

2) If query is a location, a search by location is returned

3) if query is not a location, a search by name is returned


### Searches by Location

Searches by location will check for matches within a 100 mile radius, in addition it will also 
return results that are a close match on either the instructor name or a location name with 
the provided query. The results are then sorted by the instructors rating in descending order 
and then by relevance to the query.


### Searches by Name

Searches by name will search for close matches on the instructors name, location names, and 
location addresses. Any matches returned are then sorted by the instructors rating in descending order 
and then by relevance based on how closely it is matched to the original search query.

## API Authorization

Calls to the API are protected with an authorization header. A calls should include an authorization 
header "APIAUTH" that includes the accessing user's email and their API key separated by a colon 
with "TGCAPI " prepended and encoded in base64.

Here's an example of setting the header via cURL in PHP:

```

curl_setopt($ch,CURLOPT_HTTPHEADER,[
    'ApiAuth: '.base64_encode('TGCAPI users_email@certified.site:api_key')
]);

```