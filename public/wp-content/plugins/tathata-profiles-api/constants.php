<?php namespace TGCAPI;

if(!defined('TGCAPI_VERSION')) {
    define('TGCAPI_VERSION','0.0.2');
}
if(!defined('TGCAPI_PLUGIN_FILE')) {
    define("TGCAPI_PLUGIN_FILE",str_replace("\\","/",realpath(__FILE__)));
}
if(!defined('TGCAPI_PLUGIN_DIR')) {
    define("TGCAPI_PLUGIN_DIR",str_replace("\\","/",realpath(dirname(TGCAPI_PLUGIN_FILE))));
}
if(!defined('TGCAPI_PLUGIN_PREFIX')) {
    define('TGCAPI_PLUGIN_PREFIX','TGCAPI_');
}