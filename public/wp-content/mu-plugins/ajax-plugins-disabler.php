<?php

/*
Plugin Name: Tathata Ajax Plugin Disabler
Plugin URI: http://www.tathatagolf.com/
Description: Custom mu plugin to disable select plugins during ajax calls
Version: 0.1
Author: Factor1, James R. Latham
Author URI: http://www.tathatagolf.com/
License: MIT
*/

add_action('muplugins_loaded',function() {

    // Not ajax call, bypass
    if(!defined('DOING_AJAX') || !DOING_AJAX) {
        return;
    }

    // Disable plugins on complete course action
    add_filter('option_active_plugins',function($plugins) {

        // Target course completion ajax only
        if(!isset($_POST['action']) || $_POST['action'] !== 'admin_complete_user_lesson') {
            return $plugins;
        }

        $unload = [
            'add-new-default-avatar/kl_addnewdefaultavatar.php',
            'addthis/addthis_social_widget.php',
            'affiliate-wp/affiliate-wp.php',
            'affiliatewp-affiliate-area-shortcodes/affiliatewp-affiliate-area-shortcodes.php',
            'affiliatewp-allowed-products/affiliatewp-allowed-products.php',
            'affiliate-wp-affiliate-groups/affiliate-wp-affiliate-groups.php',
            'factr1-affiliate-group-rates-plugin/plugin.php',
            'anything-order-by-terms/anything-order.php',
            'appointment-booking/main.php',
            'bbpress wp4 fix/bbpress wp4 fix.php',
            'bbpress-notify-nospam/bbpress-notify-nospam.php',
            'bbpress-report-content/bbpress-report-content.php',
            'bbpress-shortcodes/bbpress-shortcodes.php',
            'bbpress/bbpress.php',
            'bootstrap-3-shortcodes/bootstrap-shortcodes.php',
            'bootstrap-shortcodes/bootstrap-shortcodes.php',
            'category-checklist-tree/category-checklist-tree.php',
            'cimy-user-extra-fields/cimy_user_extra_fields.php',
            'custom-forgot-mail/index.php',
            'display-widgets/display-widgets.php',
            'duplicate-post/duplicate-post.php',
            'duracelltomi-google-tag-manager/duracelltomi-google-tag-manager-for-wordpress.php',
            'easy-custom-auto-excerpt/easy-custom-auto-excerpt.php',
            'embed-any-document/awsm-embed.php',
            'google-analytics-dashboard-for-wp/gadwp.php',
            'gravity-forms-custom-post-types/gfcptaddon.php',
            'gravityforms/gravityforms.php',
            'gravityformsauthorizenet/authorizenet.php',
            'gravityformsmailchimp/mailchimp.php',
            'gravityformspaypal/paypal.php',
            'gravityformssignature/signature.php',
            'ignitewoo-gift-certificates/woocommerce-gift-certificates.php',
            'intuitive-custom-post-order/intuitive-custom-post-order.php',
            'mailgun/mailgun.php',
            'meteor-slides/meteor-slides-plugin.php',
            'oiopub-direct/wp.php',
            'p3-profiler/p3-profiler.php',
            'peekaboo/peekaboo.php',
            'photo-gallery/photo-gallery.php',
            'postcode-shipping/postcode_shipping.php',
            'responsive-lightbox/responsive-lightbox.php',
            'revslider/revslider.php',
            'safe-report-comments/safe-report-comments.php',
            'smart-send-pickup-shipping/woocommerce-shipping-local-pickup.php',
            'social-share-button/social-share-button.php',
            'tathata-greatest-players/greatest-players.php',
            'tathata-member-video/index.php',
            'tathata-oa-videos/oa-videos.php',
            'tathata-certified-connector/plugin.php',
            'testimonial-slider/testimonial-slider.php',
            'theme-my-login/theme-my-login.php',
            'toggle-wpautop/toggle-wpautop.php',
            'user-role-editor-pro/user-role-editor-pro.php',
            'video-thumbnails/video-thumbnails.php',
            'wck-custom-fields-and-custom-post-types-creator/wck.php',
            'what-the-file/what-the-file.php',
            'widgets-on-pages/widgets_on_pages.php',
            'woocommerce-customer-order-xml-export-suite/woocommerce-customer-order-xml-export-suite.php',
            'woocommerce-follow-up-emails/woocommerce-follow-up-emails.php',
            'woothemes-updater/woothemes-updater.php',
            'wooview/wooview.php',
            'wordpress-importer/wordpress-importer.php',
            'wordpress-seo/wp-seo.php',
            'wp-all-export-pro/wp-all-export-pro.php',
            'wp-better-emails/wpbe.php',
            'wp-maintenance-mode/wp-maintenance-mode.php',
            'wp-menu-cart-pro/wp-menu-cart-pro.php',
            'wp-migrate-db-pro/wp-migrate-db-pro.php',
            'wp-optimize/wp-optimize.php',
            'wp-rocket/wp-rocket.php',
            'wp-sitemap-page/wp-sitemap-page.php',
            'wunderground/wunderground.php',
        ];

        return array_diff($plugins,$unload);

    },9999,1);
});