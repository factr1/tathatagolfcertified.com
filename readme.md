# To setup your local environment: #

Create a `wp-config.settings` file in the project root like this and change the hostnames to your local environment host names.

## Settings File ##
```
<?php

$config = [
    'environments' => [
        'local'=>[
            'error_reporting' =>E_ALL,
            'http_hosts' => [
                'www.tathatagolfcertified.dev',
                'tathatagolfcertified.dev'
            ],
            'public_paths'=>[
                '',
            ]
        ]
    ]
];
```

## Local Config File ##
Second, create an `wp-config.local` in the root which will contain your actual normal `wp-config.php` contents.

### Working with Tathatagolf.com ###
This website was created from a fork of Tathatagolf.com. There has not been any direct link between the two sites, and any improvements to the main site needed to be manually copied and pasted into the Certified site. There is now a `certified-merger` branch that bridges this gap. Any changes made on the main site that also need to be ported over to the Certified site and (vice-versa) will also be pushed to this branch. Then you just have to switch to the other repo and pull the `certified-merger` branch down from the origin. Note that not all files are exactly the same. MOST Sass/CSS, images, template parts, core template files, etc. are the same but take care when editing WooCommerce and Sensei files. The logic varies slightly so be sure to test, and once completed merge into `master` or `develop` as needed.