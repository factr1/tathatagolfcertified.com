#!/bin/bash

# Requires quick_sync wrapper for rsync from SPLIT Concepts

export CURRENT_DIR=$(dirname $0)

source $CURRENT_DIR/.config.staging

export PROD_BASE_DIR=$BASE_SYSTEM_DIR

export LOCAL_BASE_DIR=..

export SNAPSHOTS_PATH=/scripts/snapshots

quick_sync 22 $LOCAL_BASE_DIR$SNAPSHOTS_PATH/ tathatagolfcerti@tathatagolfcertified.com:$PROD_BASE_DIR$SNAPSHOTS_PATH

